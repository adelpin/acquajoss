﻿using Core;
using Core.Helpers;
using Core.Properties;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace AcquaJoss
{
    public class Global : HttpApplication
    {

        private static string _url;

        public static string url
        {
            get
            {
                string url = GlobalSettings.UrlSite;
                return GlobalSettings.EnProduccion || string.IsNullOrEmpty(url) ? _url : url;
            }
        }

       

        public static bool IsMobile
        {
            get
            {
                return HttpContext.Current.Request.Browser.IsMobileDevice;
            }
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //starting        
            Impostazioni.Conexion = Settings.Default.DT_BackOffice;

            //Impostazioni.PrezzoNavetta = (double)Repository.ConvertiPrezzo(Repository.GetNavetta());
            
            Impostazioni.DynamicIds = Repository.GetDynamicIds();

            Impostazioni.BasePrice = Repository.GetBasePrice();
            Impostazioni.BaseReducePrice = Repository.GetBaseReducePrice();
            Impostazioni.BaseHotelPrice = Repository.GetBaseHotelPrice();

            Impostazioni.Steps = Repository.GetStepInfo();

            //company info  for invoice
            //1 zoomarine site
            //2 zoomarine Travel
            Repository.GetInvoiceInfo(1);

            //Tmaster
            string TmTest = "";
            if ( ConfigurationManager.AppSettings["TmTest"] == "on")
            {
                TmTest = "Test";
            }

            Impostazioni.TmServer = ConfigurationManager.AppSettings["TmServer" + TmTest];
            Impostazioni.TmEndPoint = Impostazioni.TmServer + Impostazioni.TmPercorso;

            Impostazioni.DynamicCalendarDb = ConfigurationManager.AppSettings["DynamicCalendarDb"];


            ////////////////////////
            //unicredit bank
            string UniTest = "";
            if (ConfigurationManager.AppSettings["UniTest"] == "on")
            {
                UniTest = "Test";
            }

            Impostazioni.UniURL = ConfigurationManager.AppSettings["UniURL" + UniTest];
            Impostazioni.UnierrorURL = Impostazioni.UniURL + ConfigurationManager.AppSettings["UnierrorURL"];
            Impostazioni.UninotifyURL = Impostazioni.UniURL + ConfigurationManager.AppSettings["UninotifyURL"];


            Impostazioni.UniserverURL = ConfigurationManager.AppSettings["UniserverURL" + UniTest];
            Impostazioni.tid = ConfigurationManager.AppSettings["tid" + UniTest];
            Impostazioni.kSig = ConfigurationManager.AppSettings["kSig" + UniTest];
        

            ////////////////////////
            //banca sella bank
            string BsTest = "";
            if (ConfigurationManager.AppSettings["BsTest"] == "on")
            {
                BsTest = "Test";
                Impostazioni.BsTest = BsTest;
            }

            Impostazioni.BsShopLogin = ConfigurationManager.AppSettings["BsShopLogin" + BsTest];
            Impostazioni.BsUrl = ConfigurationManager.AppSettings["BsUrl" + BsTest];
            Impostazioni.BsEndPoint = ConfigurationManager.AppSettings["BsEndPoint" + BsTest];
            Impostazioni.BsCheckEndPoint = ConfigurationManager.AppSettings["BsCheckEndPoint" + BsTest];

            Impostazioni.BsIframeJs = ConfigurationManager.AppSettings["BsIframeJs" + BsTest];
      
        }
        public void Session_Start(object source, EventArgs e)
        {
            GlobalSettings.CountryCode = CountrySite.GetCountryCode(GlobalSettings.Ip);
        }

        public static string ShippingEmail
        {
            get
            {
                string email = ConfigurationManager.AppSettings["ShippingEmail"];
                return !string.IsNullOrEmpty(email) ? email : "info@zoomarine.it";
            }
        }

        public static string ReceiptEmail
        {
            get
            {
                string email = ConfigurationManager.AppSettings["ReceiptEmail"];
                return !string.IsNullOrEmpty(email) ? email : "info@zoomarine.it";
            }
        }

        Dictionary<string, string> map = new Dictionary<string, string>() {
            {"/AreaRiservata/LoginUser/","/areariservata/login/" },
            // {"/areariservata/","/areariservata/login/" },
            { "/acquisto/", "/acquisto/biglietto/" },
            {"/informazioni/", "/informazioni-generali/" },
            {"/pagina/pioggia/promopioggia/", "/domande-frequenti/" },
            {"/attrazioni/", "/ilparco/" },
            {"/acquisto/altri-biglietti/", "/promozioni/" },
            {"/orari/", "/calendari/" },
            {"/acquisto/stato/", "/areariservata/verifica/" },
            {"/visite-guidate/scuole/", "/scuole/" },
            {"/visite-guidate/gruppi/", "/gruppi/" },
            {"/eduscienza/14/educazione/", "/eduscienza/" },
            {"/visite-guidate/centri-estivi/", "/centro-estivo/" },
            {"/visite-guidate/compleanni/", "/compleanni/" },
            {"/visite-guidate/convenzioni/", "/convenzioni/" },
            {"/attrazioni/dinosauri/", "/ilparco/era-dei-dinosauri/" },
            {"/attrazioni/tuffatori/", "/ilparco/galeone-dei-tuffatori/" },
            {"/attrazioni/malibu/", "/ilparco/malibu/" },
            {"/attrazioni/blueRiver/", "/ilparco/blue-river/" },
            {"/attrazioni/vertigo/", "/ilparco/vertigo/" },
            {"/attrazioni/hawaii/", "/ilparco/hawaii/" },
            {"/attrazioni/harakiri/", "/ilparco/harakiri/" },
            {"/attrazioni/squalotto/", "/ilparco/squalotto/" },
            {"/attrazioni/segway/", "/ilparco/formula-segway/" },
            {"/attrazioni/tartarughe/", "/ilparco/oasi-delle-tartarughe/" },
            {"/attrazioni/polpo/", "/ilparco/polpo/" },
            {"/attrazioni/flowRider/", "/ilparco/flow-rider/" },
            {"/attrazioni/minipolpo/", "/ilparco/mini-polpo/" },
            {"/attrazioni/pinguini/", "/ilparco/spiaggia-dei-pinguin/" },
            {"/attrazioni/amicidizampa/", "/ilparco/amici-di-zampa/" },
            {"/attrazioni/carosello/", "/ilparco/carosello/" },
            {"/attrazioni/galeonemaledetto/", "/ilparco/galeone-maledetto/" },
            {"/attrazioni/pirati/", "/ilparco/laguna-dei-pirati/" },
            {"/attrazioni/nidoDeifenicotteri/", "/ilparco/terra-dei-draghi/" },
            {"/attrazioni/cinema4d/", "/ilparco/cinema-4d/" },
            {"/attrazioni/treninofattoria/", "/ilparco/trenino-fattoria/" },
            {"/attrazioni/giochiamoinsiemeallefoche/", "/ilparco/giochiamo-insieme-alle-foche/" },
            {"/attrazioni/volofalco", "/ilparco/volo-del-falco/" },
            {"/attrazioni/covodeipirati/", "/ilparco/covo-dei-pirati/" },
            {"/attrazioni/avventuranellaforesta/", "/ilparco/avventura-nella-foresta/" },
            {"/attrazioni/california/", "/ilparco/california/" },
            {"/mission/", "/chi-siamo/" },
            {"/partner-scientifici/", "/chi-siamo/" },
            {"/zoomarine-spa/", "/chi-siamo/" },
            {"/blog/", "/" },

        };

        List<string> excludeURL = new List<string>() {
          { "areariservata" },
           { "acquisto" },
           { "shoppingcart"},
           {"confirmation" },
           {"conferma" },
           {"checkout" },
           {"home" },
           {"landingpage" },
            {"/landingpage/" },
           {"error" }
       };

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            string rutacompleta = context.Request.Path.ToLower();

            if (!rutacompleta.Contains("error/") && !context.Request.RawUrl.Contains("?"))
            {
                var myKey = map.FirstOrDefault(x => x.Key == rutacompleta).Value;
                bool exist = excludeURL.Where(urls => rutacompleta.Contains(urls)).ToList().Count > 0;
                if (myKey != null)
                {
                    context.Response.Redirect(myKey.Contains("{0}") ? string.Format(myKey, "promozioni") : myKey, true);
                }
                else if (!rutacompleta.Contains("content") && !rutacompleta.Contains("bundles") && !rutacompleta.EndsWith("/") && !exist)
                {
                    context.Response.Redirect(rutacompleta + "/", true);
                }

            }
            _url = Request.Url.DnsSafeHost;

        }
    }
}
