﻿using System.Web.Mvc;
using System.Web.Routing;

namespace AcquaJoss
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.LowercaseUrls = true;
            routes.MapMvcAttributeRoutes();


            routes.MapRoute(
             name: "oauthredirect",
             url: "oauthredirect",
             defaults: new { controller = "Home", action = "oauthredirect" }
         );


            routes.MapRoute(
            name: "ComeArrivare",
            url: "come-arrivare",
            defaults: new { controller = "Home", action = "ComeArrivare" }
          );
            routes.MapRoute(
              name: "InformazioniGenerali",
              url: "informazioni-generali",
              defaults: new { controller = "Home", action = "InformazioniGenerali" }
          );

            routes.MapRoute(
             name: "domandeFrequenti",
             url: "domande-frequenti",
             defaults: new { controller = "Home", action = "domandeFrequenti" }
          );
            
            routes.MapRoute(
               name: "Calendari",
               url: "calendari",
               defaults: new { controller = "Home", action = "Calendari" }
           );

            routes.MapRoute(
               name: "Eventi",
               url: "eventi",
               defaults: new { controller = "Home", action = "Eventi" }
           );

            routes.MapRoute(
               name: "EventoInterna",
               url: "eventi/{url}",
               defaults: new { controller = "Home", action = "EventoInterna" }
           );
           
            routes.MapRoute(
               name: "EventiPassati",
               url: "eventi-pasati",
               defaults: new { controller = "Home", action = "EventiPassati" }
           );

            routes.MapRoute(
                name: "CentroEstivo",
                url: "centro-estivo",
                defaults: new { controller = "Home", action = "CentroEstivo" }
            );

            routes.MapRoute(
                name: "InformativaCookie",
                url: "informativa-cookie",
                defaults: new { controller = "Home", action = "InformativaCookie" }
            );

            routes.MapRoute(
                name: "ChiSiamo",
                url: "chi-siamo",
                defaults: new { controller = "Home", action = "ChiSiamo" }
            );

            routes.MapRoute(
               name: "Eduscienza",
               url: "eduscienza",
               defaults: new { controller = "Home", action = "Eduscienza" }
           );

            routes.MapRoute(
               name: "News",
               url: "news",
               defaults: new { controller = "Home", action = "News" }
           );

            routes.MapRoute(
               name: "NewsInterna",
               url: "news/{url}",
               defaults: new { controller = "Home", action = "NewsInterna" }
           );

            routes.MapRoute(
               name: "EventiVip",
               url: "eventi-vip",
               defaults: new { controller = "Home", action = "EventiVip" }
           );

            routes.MapRoute(
              name: "Compleanni",
              url: "compleanni",
              defaults: new { controller = "Home", action = "Compleanni" }
            );

            routes.MapRoute(
              name: "Convenzioni",
              url: "convenzioni",
              defaults: new { controller = "Home", action = "Convenzioni" }
            );

            routes.MapRoute(
                name: "Contatti",
                url: "contatti",
                defaults: new { controller = "Home", action = "Contatti" }
            );
            routes.MapRoute(
                name: "Servizi",
                url: "servizi",
                defaults: new { controller = "Home", action = "Servizi" }
            );
            routes.MapRoute(
                name: "Scuole",
                url: "scuole",
                defaults: new { controller = "Home", action = "Scuole" }
            );

            routes.MapRoute(
                name: "Infanzia",
                url: "scuole/infanzia",
                defaults: new { controller = "Home", action = "Infanzia" }
            );

            routes.MapRoute(
                name: "Primaria",
                url: "scuole/primaria",
                defaults: new { controller = "Home", action = "Primaria" }
            );

            routes.MapRoute(
                name: "SecondariaI",
                url: "scuole/secondaria-uno",
                defaults: new { controller = "Home", action = "SecondariaI" }
            );

            routes.MapRoute(
                name: "SecondariaII",
                url: "scuole/secondaria-due",
                defaults: new { controller = "Home", action = "SecondariaII" }
            );

            routes.MapRoute(
               name: "Gruppi",
               url: "gruppi",
               defaults: new { controller = "Home", action = "Gruppi" }
           );

            routes.MapRoute(
               name: "Lavora",
               url: "lavora-con-noi",
               defaults: new { controller = "Home", action = "Lavora" }
           );

            routes.MapRoute(
               name: "Partner",
               url: "partner",
               defaults: new { controller = "Home", action = "Partner" }
           );

            routes.MapRoute(
                name: "Promozioni",
                url: "promozioni",
                defaults: new { controller = "Home", action = "Promozioni" }
            );

            routes.MapRoute(
               name: "pachettoDiFamiglia",
               url: "pachetto-di-famiglia",
               defaults: new { controller = "Home", action = "pachettoDiFamiglia" }
           );

            routes.MapRoute(
              name: "Ilparco",
              url: "ilparco",
              defaults: new { controller = "Home", action = "Ilparco" }
           );

        

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
