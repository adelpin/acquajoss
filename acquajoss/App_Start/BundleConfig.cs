﻿using System.Web.Optimization;

namespace AcquaJoss
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //BundleTable.EnableOptimizations = true;

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"
                         ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/bootstrap-toolkit.js",
                      "~/Scripts/bootstrap-datepicker.js",
                      "~/Scripts/locales/bootstrap-datepicker.it.min.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                      "~/Scripts/jquery-nivo-slider.min.js",
                      "~/Scripts/hammer.min.js",
                      "~/Scripts/moment-with-locales.min.js",
                      "~/Scripts/bootstrap-datetimepicker.min.js",
                      "~/Scripts/googlemaps-scrollprevent.min.js",                    
                      "~/Scripts/slick.min.js",
                      "~/Scripts/jquery.fancybox.js",
                      "~/Scripts/readmore.js",
                      "~/Scripts/validator.js",
                      "~/Scripts/embed-video.js",
                      "~/Scripts/mixitup.min.js",
                      "~/Scripts/pinterest_grid.js",
                      "~/Scripts/macy.js",
                      "~/Scripts/siema.min.js",
                      "~/Scripts/jquery.shorten.1.0.js",
                      "~/Scripts/jquery-scrolltofixed-min.js",
                      "~/Scripts/jquery.validate-vsdoc.js",
                      "~/Scripts/jquery.serializejson.js",
                      "~/Scripts/newsletter.js",
                      "~/Scripts/main.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/animate.css",
                      "~/Content/bootsnav.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/slick.css",
                      "~/Content/responsive-embed.css",
                      "~/Content/elegant-icons.min.css",
                      "~/Content/slick-theme.css",
                      "~/Content/jquery.fancybox.css",
                      "~/Content/bootstrap-datepicker.css",
                      "~/Content/bootstrap-datetimepicker.css",
                      "~/Content/Site.css"));

        }
    }
}
