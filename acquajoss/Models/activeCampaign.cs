﻿namespace AcquaJoss.Models
{
    //public class activeCampaign
    //{
    //}

    public class ContactSyncParams
    {
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string phone { get; set; }
        public string form { get; set; }
        public string tags { get; set; }
    }

    public class AditionalContactSyncParams
    {
        public dynamic value { get; set; }
    }

    public class ReturnContactSyncParams
    {
        public int subscriber_id { get; set; }
        public int sendlast_should { get; set; }
        public int sendlast_did { get; set; }
        public int result_code { get; set; }
        public string result_message { get; set; }
        public string result_output { get; set; }
    }

    public class ReturncallEventParams
    {
        public int success { get; set; }
        public string message { get; set; }
    }
}

