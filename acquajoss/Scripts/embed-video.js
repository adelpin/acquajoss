var i, c, y, v, s, n;
var im = new Array();
v = document.getElementsByClassName("youtube");

for (n = 0; n < v.length; n++) {
    var child = v[n];
    if (child.hasAttribute("src")) {
        im[n] = child.getAttribute("src");
    }
}
if (v.length > 0) {
    s = document.createElement("style");
    s.type = "text/css";
    s.innerHTML = '.youtube{background-color:#000;max-width:100%;overflow:hidden;position:relative;}.youtube .thumb{bottom:0;display:block;left:0;margin:auto;max-width:100%;position:absolute;right:0;top:0;width:100%;height:auto}.youtube .play{filter:alpha(opacity=80);opacity:.8;left:50%;position:absolute;top:50%;margin-top:-49px;margin-left:-49px;} .youtube .play{font-size: 7em; color: black;} .youtube .play:hover{color:red;cursor:pointer;opacity:1;}';
    document.body.appendChild(s);
}
for (n = 0; n < v.length; n++) {
    y = v[n];
    i = document.createElement("img");
    i.setAttribute("src", im[n]);
    i.setAttribute("class", "thumb hidden-xs");
    c = document.createElement("i");
    c.setAttribute("class", "social_youtube play hidden-xs");


    var t = document.createElement("iframe");
    t.setAttribute("src", "https://www.youtube.com/embed/" + y.id + param(y));
    t.setAttribute("class", "embed-responsive-item");
    t.style.width = y.style.width;
    t.style.height = y.style.height;

    y.appendChild(t);
    y.appendChild(i);
    y.appendChild(c);

    c.onclick = function () {
        t.setAttribute("src", t.getAttribute("src") + "&autoplay=1");
        i.style.display = c.style.display = "none";
    }
};

function param(x) {
    if (x.getAttribute("data-params") !== null) {
        var params = x.getAttribute("data-params");
        params = loop(params);
        return "?" + params;
    }
}

function loop(x) {
    if (x.indexOf("loop") > -1) {
        var loop = /loop=([^&]+)/.exec(x)[1];
        if (loop == "1" || loop == "true") {
            x += "&playlist=" + y.id;
        }
    }
    return x;
}

window.addEventListener("resize", function (event) {
    if (this.innerWidth <= 768) {
    }
});