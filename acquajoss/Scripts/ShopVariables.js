﻿var TotPrice = 0;
var Tickets = [], Animals = [], SubAnimals = [], Activities = [], Menus = [];

var ticket = [], animal = [], AnimalMaxQuantity = [], subanimal = [], SubAnimalMaxQuantity = [], activity = [], menu = [];


var Quantity = [], Titles = [], Descriptions = [], Notes = [], BasePrices = []; Prices = [];
var AniQuantity = [], AniPosition = [], AniTitles = [], AniDescriptions = [], AniNotes = [], AniImages = [], AniBasePrices = [], AniPrices = [];
var SubAniQuantity, SubAniDisp, SubAniTitles, SubAniDescriptions, SubAniNotes, SubAniPrices;


var ActQuantity = [], ActTitles = [], ActDescriptions = [], ActNotes = [], ActImages = [], ActBasePrices = [], ActPrices = [];
var MnuQuantity = [], MnuTitles = [], MnuDescriptions = [], MnuNotes = [], MnuImages = [], MnuBasePrices = [], MnuPrices = [];

var Day = "", OldDay = "";
var phtit, phori, phdes, phnot, phimg, phbaseprc, phprc, phqnt, phsbt;

var phchk;
var azzera;
var SubAniIndex, SubAniQuantLinear, NoCalcSmq;
var ArrFinish;

var Arrloop, Lungloop;

var Purchase;
var ShoppingList;
var OptionList;

var LogUser = 0;
var LogUserId = 0;
var AcquistoId = "";
var Svincolato = "";
var VerificaCliente = "";

var FamilyPack = "";
var FamilyTitle = "";
var FamilyDescription = "";

var Discount = "";

////////////////////////////////////////////
//buyer
var Name, Surname, Email, TelePhone, Zip, Invoice, CompanyName, VatNumber, SSNumber, Address, City, Province, MailingList, CodUni, Pec, Private;
var Buyer = { Name: "", Surname: "", Email: "", TelePhone: "", Zip: "", Invoice: "", CompanyName: "", VatNumber: "", SSNumber: "", Address: "", City: "", MailingList: "", CodUni: "", Pec: "", Private: ""};


////////////////////////////////////////////
//dynamic section
phtit = "Tit_";
phori = "Ori_";
phdes = "Des_";
phnot = "Not_";
phimg = "Img_";
phbaseprc = "BasePrice_";
phprc = "WebPrice_";
phqnt = "Quantity_";
phsbt = "SubTot_";

var elements = [phdes, phnot, phtit, phimg, phbaseprc, phprc, phqnt, phsbt];

var subanid = "SubAnimalId_";


$(".form-steps_item-icon").on('click', function (e) {

    var mappa = $(this).attr('id');

    switch (mappa) {
        case "step1":
            GoToBuy(Prodotto);
            break;

        case "step2":
            GoToMenu(Prodotto);
            break;

        case "step3":
            GoToAnimal(Prodotto);
            break;

        case "step4":
            GoToActivity(Prodotto);
            break;

        case "step5":
            GoToShoppingCart(Prodotto);
            break;

        default:
            e.preventDefault();
    }
});


function ConvertDate(Day)
{
    //convert date string dd/mm/yyyy
    //in javascript date

    var dd = Day.substr(0, 2);
    var mm = Number(Day.substr(3, 2)) - 1;
    var aa = Number(Day.substr(6, 4));

    var jDay = new Date(aa, mm, dd);

    return jDay;
}


function GoToBuy(Prodotto)
{

    Acquisizione();
    GetOptionList();

    if (Prodotto == "ticket" || Prodotto == null)
    {
        GetShoppingList();

        var URL = "/Acquisto/backToBuy";
    }
    
    if (Prodotto == "hotel")
    {
        GetShoppingListHotel();
        var URL = "/Acquisto/backToBuyHotel";
    }

    if (Prodotto == "abbonamento") {
        GetShoppingListSubscription();
        var URL = "/Acquisto/BacktoSubscriptions";
    }


  
    var promise = $.post(URL, ShoppingList);

    promise.done(function (data) {
        window.location.href =  data;
    });

    promise.fail(function (err) { console.log("error promise: " + URL); });
}

function GoToMenu()
{
    Acquisizione();
    GetOptionList();

    if (Prodotto == "ticket" || Prodotto == null) {
        GetShoppingList();
        URL = "/Acquisto/GetShopping";
    }
    else {
        GetShoppingListHotel();
        URL = "/Acquisto/GetShoppingHotel";
    }

    var Info = { Shopping: ShoppingList };

    var promise = $.post(URL, Info);
  
    promise.done(function (data) {
        window.location.href =  "/Acquisto/BuyMenu";
    });

    promise.fail(function (err) { console.log("error promise: " + URL); });
}

function GoToAnimal()
{
    Acquisizione();
    GetOptionList();

    if (Prodotto == "ticket" || Prodotto == null) {
        GetShoppingList();
        URL = "/Acquisto/GetShopping";
    }
    else {
        GetShoppingListHotel();
        URL = "/Acquisto/GetShoppingHotel";
    }

    var Info = { Shopping: ShoppingList };
    var promise = $.post(URL, Info);

    promise.done(function (data) {
        window.location.href = "/Acquisto/Buyanimal";
    });

    promise.fail(function (err) { console.log("error promise: " + URL); });
}


function redirect (url) {
    var ua        = navigator.userAgent.toLowerCase(),
        isIE      = ua.indexOf('msie') !== -1,
        version   = parseInt(ua.substr(4, 2), 10);

    // Internet Explorer 8 and lower
    if (isIE && version < 9) {
        var link = document.createElement('a');
        link.href = url;
        document.body.appendChild(link);
        link.click();
    }

        // All other browsers can use the standard window.location.href (they don't lose HTTP_REFERER like Internet Explorer 8 & lower does)
    else { 
        window.location.href = url; 
    }
}


function GoToActivity()
{
    Acquisizione();
    GetOptionList();

    if (Prodotto == "ticket" || Prodotto == null) {
        GetShoppingList();
        URL = "/Acquisto/GetShopping";
    }
    else {
        GetShoppingListHotel();
        URL = "/Acquisto/GetShoppingHotel";
    }

    var Info = { Shopping: ShoppingList };
    var promise = $.post(URL, Info);

    promise.done(function (data) {
        window.location.href =  "/Acquisto/Buyactivity";
    });

    promise.fail(function (err) { console.log("error promise: " + URL); });
}

function GoToShoppingCart()
{
    Acquisizione();
    GetOptionList();
    
    if (Prodotto == "ticket" || Prodotto == null) {
        GetShoppingList();
        URL = "/Acquisto/ShoppingCart";
    }


    if (Prodotto == "hotel")
    {
        GetShoppingListHotel();
        URL = "/Acquisto/ShoppingCartHotel";
    }

    if (Prodotto == "abbonamento")
    {
        return;
    }

    var Info = { Shopping: ShoppingList };
    var promise = $.post(URL, Info);

    promise.done(function (data) {
        window.location.href =  data;
    });

    promise.fail(function (err) { console.log("error promise: " + URL); });
}

function GetOptionList() {

    OptionList = {
        Animals: Animals,
        SubAnimals: SubAnimals,
        Activities: Activities,
        Menus: Menus
    };
}


function GetShoppingList() {
    ShoppingList = {
        Buyer: Buyer,
        Day: Day,
        DiscountCode: DiscountCode,
        Tickets: Tickets,
        Options: OptionList
    };

}

function GetShoppingListHotel() {

    ShoppingList = {
        Buyer: Buyer,
        DayArrive: DayArrive,
        DayDeparture: DayDeparture,
        AssicCambioData: AssicCambioData,
        ServizioNavetta: ServizioNavetta,
        HotelId: HotelId,
        HotelPrice: HotelPrice,
        HotelPriceId: HotelPriceId,
        Rooms: Rooms,
        Price: TotPrice,
        Options: OptionList
    };

}

function GetShoppingListSubscription() {

    ShoppingList = {
        Buyer: Buyer,
        SubscriptionType: SubscriptionId,
        Quantity: Quantity,
        SubscriberName: Names,
        SubscriberSurname: Surnames
    };

}

function Acquisizione() {
   
    for (var i = 0; i < ticket.length; i++) {
        Tickets[i] = { Id: ticket[i], Title: Titles[i], Description: Descriptions[i], Note: Notes[i], Price: Prices[i], Quantity: Quantity[i] };
    }

    for (var i = 0; i < animal.length; i++) {
        Animals[i] = { Id: animal[i], Title: AniTitles[i], Description: AniDescriptions[i], Note: AniNotes[i], Price: AniPrices[i], Quantity: AniQuantity[i] };
    }

    for (var i = 0; i < activity.length; i++) {
        Activities[i] = { Id: activity[i], Title: ActTitles[i], Description: ActDescriptions[i], Note: ActNotes[i], Price: ActPrices[i], Quantity: ActQuantity[i] };
    }

    for (var i = 0; i < menu.length; i++) {
        Menus[i] = { Id: menu[i], Title: MnuTitles[i], Description: MnuDescriptions[i], Note: MnuNotes[i], Price: MnuPrices[i], Quantity: MnuQuantity[i] };
    }

    var index = 0;
    

    for (var i = 0; i < animal.length; i++) {
        var subl = 0;
        if (subanimal[i] != null) { subl = subanimal[i].length; }

        for (var j = 0; j < subl; j++) {
            SubAnimals[index] = {
                Id: subanimal[i][j].Id,
                IdFather: subanimal[i][j].IdFather,
                Quantity: SubAniQuantity[i][j]
            };
            index++;
        }
    }


}

function Template(himg, htit, hdes, hqnt, hbaseprc, hprc, hnot, subt) {
    var temp = '';

    temp += '<div class="col-sm-12" style="margin-bottom:20px;">';

    //temp += '<div class="row btn-lg" style="background-color: white; box-shadow: 1px 1px 6px -1px rgba(0,0,0,0.75);">';
    //temp += '<h3 style="margin: 0;" id ="OptionTitle"></h3>';
    //temp += '</div>';
    //temp += '<br/>';


    temp += '<div class="row btn-lg" style="background-color: white; box-shadow: 1px 1px 6px -1px rgba(0,0,0,0.75);">';
    temp += '<div class="col-sm-4 btn-lg" style=" border: solid 1px; border-color:grey;">';
    temp += '<img id="' + himg + '" src="" width="100%" height="100%">';
    temp += '</div>';
    temp += '<div class="col-sm-8">';
    temp += '<h4 id="' + htit + '"></h4>';
    temp += '<div class="col-md-6 col-sm-6">';
    temp += '<h5 style="margin-top: 0; margin-bottom: 0; height: 30px;" id="' + hdes + '"></h5>';
    temp += '<h4 style="margin-top: 15px; margin-bottom: 4px;">Quantità</h4>';
    temp += '<div class="rail-select" style="margin-top: 2px;">';
    temp += '<div class="select-side"><i class="glyphicon glyphicon-menu-down blue"></i></div>';
    temp += '<select id="' + hqnt + '" onchange="SetQuantity(value, LineId)" class="form-control">';
    temp += '</select>';
    temp += '</div>';
    temp += '</div>';
    temp += '<div class="col-md-6 col-sm-6 textPromoShop" style="text-align: right;">';
    temp += '<h4 style="margin-top: 2px; margin-bottom: 2px; color: #999999; font-size: 14px; display:inline-block;">PREZZO AL PARCO&nbsp;</h4>';
    temp += '<h4 class="strikediag" id="' + hbaseprc + '" style="margin-top: 2px; margin-bottom: 2px; color: #999999; font-size: 14px; display:inline-block;"></h4>';
    temp += '<h4 style="margin-top: 2px; color: #089bda; display:inline-block;"><b>PROMO ONLINE&nbsp;</b></h4>';
    temp += '<h4 style="font-weight: bold; margin-top: 2px; color: #089bda; display: inline-block;" id="' + hprc + '"></h4>';
    temp += '<h3 style="text-align: center; margin-left: auto; padding: 4px; max-width: 160px; background-color: #ececec; color: darkslategray; font-family: Hiruko Pro;" id="' + subt + '">0€</h3>';
    temp += '</div>';
    temp += '<div class="row btn-lg">';
    temp += '</div>';
    temp += '</div>';
    temp += '<div class="row col-sm-12" style="">';
    temp += '<h4 id="' + hnot + '" style="font-family:Circular Std; margin: 5px; color: #000000"></h4>';
    temp += '</div>';
    temp += '</div>';
    temp += '</div>';

    return temp;
}
          
function Template_old(himg, htit, hdes, hqnt, hbaseprc, hprc, hnot, subt) {
    var temp = '';
    temp += '<div class="row"><div class="col-md-9">';


    temp += '<div class="row btn-lg" style="background-color: white; border: solid 1px; border-color:grey;">';
    temp += '<div class="col-md-3 btn-lg" style="background-color: white; border: solid 1px; border-color:grey;">';
    temp += '<img id="' + himg + '" src="" width="100%" height="100%">';
    temp += '</div>';
    temp += '<div class="col-md-9">';
    temp += '<h4><label id="' + htit + '"></label></h4>';
    temp += '<h4><label id="' + hdes + '"></label></h4>';

    temp += '<div class="row btn-lg">';
    temp += '<div class="col-md-4">';
    temp += '<h5>Quantita</h5>';

    //temp += '<div class="rail-select2">';
    //temp += '<div class="select-side"><i class="glyphicon glyphicon-menu-down"></i></div>';
    //temp += '<select id="' + hqnt + '" onchange="SetQuantity(value, LineId)" class="form-control" >';
    //temp += '</select>';
    //temp += '</div>';

    temp += '<div class="rail-select" style="margin-top: -5px;">';
    temp += '<div class="select-side"><i class="glyphicon glyphicon-menu-down blue"></i></div>';
    temp += '<select id="' + hqnt + '" onchange="SetQuantity(value, LineId)" class="form-control" >';
    temp += '</select>';
    temp += '</div>';


    temp += '</div>';
    temp += '<div class="col-md-4"><h5 style="color: #089bda">Prezzo Normale</h5><span class="strikediag" style="color: #089bda" id="' + hbaseprc + '"></span></div>';
    temp += '<div class="col-md-4"><h5 style="color: #009d3f">PROMO ONLINE</h5><label style="color: #009d3f" id="' + hprc + '"></label></div>';
    temp += '</div></div>';
    temp += '<div class="row" style="margin-left:10px; padding:0;"><h5 style="color: #000000">&nbsp;</h5><label style="font-size: 12px;" id="' + hnot + '">&nbsp;</label></div>';
    temp += '</div>';
    temp += '<div>&nbsp;</div>';

    temp += '</div>';
    temp += '<div id="' + subt + '" class="col-md-3" style="visibility: hidden; font-family: Hiruko Pro; text-align: center;">';
    temp += ' <div class="col-md-12 btn-lg" style="background-color: white; border: solid 1px; border-color:grey;">';
    temp += '<h4 style="color: red;">TOTALE</h4>';
    temp += '<h3 style="color: darkslategray; font-family: Hiruko Pro;" id="SubTotPrice">0€</h3>';
    temp += '</div>';
    //temp += '<div class="col-md-12 btn-lg">';
    //temp += '<input type="button" id="BtnProcedi" class="btn btn-success" style="box-shadow: 0 2px #777;" value="PROCEDI >">';
    //temp += '</div>';

    temp += '</div>';

    return temp;
}

function ReadFieldsBuyer() {
    //proceed to checkout
    Name = $("#Bnome").val();
    Surname = $("#Bcognome").val();
    Email = $("#Bemail").val();
    TelePhone = $("#Btelefono").val();
    Zip = $("#Bcap").val();


    if ($("#RicFatt").prop("checked")) {
        Invoice = "Y";
        CompanyName = $("#Bragsoc").val();


        if ($("input[name='oppriv']:checked").val() === "Y") {
            CompanyName = Name + " " + Surname;
        }

        //if ($("#oppriv").prop("checked"))
        //{
        //    CompanyName = Name + " " + Surname;
        //}
      
        VatNumber = $("#Bpiva").val();
        SSNumber = $("#Bcodfisc").val().toUpperCase(); //important
        Address = $("#Bindirizzo").val();
        City = $("#Bcitta").val();
        Province = $("#Bprovincia").val().toUpperCase(); //important
        CodUni = $("#Bcoduni").val().toUpperCase(); //important
        Pec = $("#Bpec").val();
    }
    else {
        Invoice = "";
        CompanyName = "";
        VatNumber = "";
        SSNumber = "";
        Address = "";
        City = "";
        Province = "";
        CodUni = "";
        Pec = "";      
    }

    if ($("#opmail").prop("checked")) {
        MailingList = "Y";
    }
    else {
        MailingList = "";
    }

    if ($("input[name='oppriv']:checked").val() === "Y") {
        Private = "Y";
    }
    else {
        Private = "";
    }


    SetCookieBuyer();

    Buyer = {
        Name: Name,
        Surname: Surname,
        Email: Email,
        TelePhone: TelePhone,
        Zip: Zip,
        Invoice: Invoice,
        CompanyName: CompanyName,
        VatNumber: VatNumber,
        SSNumber: SSNumber,
        Address: Address,
        City: City,
        Province: Province,
        MailingList: MailingList,
        CodUni: CodUni,
        Pec: Pec,
        Private: Private
    };

}

function FormatMoney(money)
{

    if (money == null)
    {
        return "0,00";
    }
    
    var amount = money;

    amount = amount.toString();
    amount = amount.replace(".", ",");

    var n = amount.search(",");
    if (n != -1) {
        if (amount.substring(n + 1).length == 1) { amount = amount + "0"; }
    }
    else
    {
        amount = amount + ",00";
    }

    return amount;
}

///////////////////////////////////
//buyer management
function LoadBuyer(DataModel) {
    Name = DataModel.Name;
    Surname = DataModel.Surname;
    Email = DataModel.Email;
    TelePhone = DataModel.TelePhone;
    Zip = DataModel.Zip;
    Invoice = DataModel.Invoice;
    CompanyName = DataModel.CompanyName;
    VatNumber = DataModel.VatNumber;
    SSNumber = DataModel.SSNumber;
    Address = DataModel.Address;
    City = DataModel.City;
    Province = DataModel.Province;
    MailingList = DataModel.MailingList;
    CodUni = DataModel.CodUni;
    Pec = DataModel.Pec;

    
    if (CompanyName === Name + " " + Surname) {
        Private = "Y";
        HideCompany();
    }
    else {
        Private = "";
        ShowCompany();
    }
      
}

function GetBuyer() {
    Buyer = {
        Name: Name,
        Surname: Surname,
        Email: Email,
        TelePhone: TelePhone,
        Zip: Zip,
        Invoice: Invoice,
        CompanyName: CompanyName,
        VatNumber: VatNumber,
        SSNumber: SSNumber,
        Address: Address,
        City: City,
        Province: Province,
        MailingList: MailingList,
        CodUni: CodUni,
        Pec: Pec,
        Private: Private
    };
}

function FillBuyer() {
    //buyer info
    $("#Bnome").val(Name);
    $("#Bcognome").val(Surname);
    $("#Btelefono").val(TelePhone);
    $("#Bcap").val(Zip);
    $("#Bemail").val(Email);
    $("#Bconfmail").val(Email);

    if (Invoice === 'Y') {
        //no default 
        //$("#RicFatt").prop("checked", true);
        //$("#DatiFattura").css("visibility", "visible");
        //$("#DatiFattura").slideDown("slow");

        $("#Bragsoc").val(CompanyName);
        $("#Bpiva").val(VatNumber);
        $("#Bcodfisc").val(SSNumber);
        $("#Bindirizzo").val(Address);
        $("#Bcitta").val(City);
        $("#Bprovincia").val(Province);
        $("#Bcoduni").val(CodUni);
        $("#Bpec").val(Pec);
        
    }
    else {
        $("#Bragsoc").val("");
        $("#Bpiva").val("");
        $("#Bcodfisc").val("");
        $("#Bindirizzo").val("");
        $("#Bcitta").val("");
        $("#Bprovincia").val("");
        $("#Bcoduni").val("");
        $("#Bpec").val("");
    }

    if (MailingList === 'Y') {
        $("#opmail").prop("checked", true);
    }

    //no default by cookies for this one:

    if (Private === 'Y') {
        $("input[name='oppriv'][value='Y']").prop('checked', true);
        $("#Fcodfisc").text("Codice Fiscale *");
        HideCompany();
    }
    else
    {
        $("#Fcodfisc").text("Codice Fiscale");
        $("input[name='oppriv'][value='']").prop('checked', true);
    }

}


//electronic invoice
$('input[type=radio][name=oppriv]').change(function () {
    if (this.value === "") {
        //company
        $("#Fcodfisc").text("Codice Fiscale");
        ShowCompany();
    }
    else
    {   
        //private
        $("#Fcodfisc").text("Codice Fiscale *");
        HideCompany();
                    
    }
});



function ShowCompany() {
    $("#divragsoc").css("display", "block");
    $("#diviva").css("display", "block");
    $("#tappo").css("display", "none");
    $("#Bragsoc").val("");
}

function HideCompany() {
    $("#divragsoc").css("display", "none");
    $("#diviva").css("display", "none");
    $("#tappo").css("display", "block");
    $("#Bpiva").val("");
    $("#BragSoc").val("");
}

function ValidateBuyer() {
    var ErrFields = [];

    ErrFields.push("Bnome", "Nome obbligatorio");
    ErrFields.push("Bcognome", "Cognome obbligatorio");
    ErrFields.push("Btelefono", "Telefono obbligatorio");
    ErrFields.push("Bcap", "Cap obbligatorio");
    ErrFields.push("Bemail", "Email obbligatoria");
    ErrFields.push("Bconfmail", "Conferma Email obbligatoria");

    var ErrOptionals = [];



    //ErrOptionals.push("Bragsoc", "Ragione sociale obbligatoria");
    ErrOptionals.push("Bindirizzo", "Indirizzo per fattura obbligatorio");
    ErrOptionals.push("Bcitta", "Città per fattura obbligatorio");
    ErrOptionals.push("Bprovincia", "Sigla provincia per fattura obbligatorio");

    var ret = "";

    //campi obbligatori
    for (var i = 0; i < ErrFields.length; i += 2) {
        ret = Obbligatorio(ErrFields[i], ErrFields[i + 1]);
        if (ret != "") return ret;
    }

    //email non valida
    var em = $("#Bemail").val();

    if (validateEmail(em) == false) {
        return "Email non valida";
    }


    //email uguali
    if ($("#Bemail").val() != $("#Bconfmail").val()) {
        return "Le email non corrispondono";
    }


    //cap
    if ($("#Bcap").val().length < 5) {
        return "CAP non valido";
    }


    ////////////////////////////////////////////////
    //fattura
    if ($("#RicFatt").prop("checked")) {
        //campi obbligatori
        for (var i = 0; i < ErrOptionals.length; i += 2) {
            ret = Obbligatorio(ErrOptionals[i], ErrOptionals[i + 1]);
            if (ret != "") return ret;
        }

        if ($("#Bprovincia").val() != "") {
            var pr = $("#Bprovincia").val();

            if (pr.length === 1) {
                ret = "Sigla Provincia non valida";
                return ret;
            }
        }

        if ($("input[name='oppriv']:checked").val() === "") {
            //company
            if ($("#Bpiva").val() === "" && $("#Bcodfisc").val() === "") {
                ret = "Indicare o la Partita Iva o il Codice Fiscale, per le aziende";
                return ret;
            }
        }
        else {
            //private
            if ( $("#Bcodfisc").val() === "") {
                ret = "Codice Fiscale obbligatorio, per i privati";
                return ret;
            }
        }

       

        //obbligatorio uno dei due:
        //if ($("#Bcodfisc").val() === "")
        //{
            if ($("#Bpiva").val() !== "" && $("#Bpiva").val().length < 11) {
                ret = "Partita Iva non valida";
                return ret;
            }
        //}
       
        //if ($("#Bpiva").val() === "")
        //{
            if ($("#Bcodfisc").val() !== "" && $("#Bcodfisc").val().length < 16) {
                ret = "Codice fiscale non valido";
                return ret;
            }
        //}
       
        if ($("#Bpiva").val() === "") {
            var codfisc = $("#Bcodfisc").val();

            if (validateCodiceFiscale(codfisc) == false) {
                ret = "Codice fiscale non valido";
                return ret;
            }
        }
              
       
        //mandatory one of them, if company:
        if ($("input[name='oppriv']:checked").val() === "") {       


            if ($("#Bragsoc").val() === "") {
                ret = "Ragione sociale obbligatoria";
                return ret;
            }

          
            if ($("#Bpec").val() == "" && $("#Bcoduni").val() == "") {
                ret = "Indicare il Codice Univoco e o la Pec, per le aziende";
                return ret;
            }

            if ($("#Bcoduni").val() != "" && $("#Bcoduni").val().length < 7) {
                ret = "Il codice univoco deve avere 7 caratteri";
                return ret;
            }
        }
        else
        {
            if ($("#Bcoduni").val() != "" && $("#Bcoduni").val().length < 7) {
                ret = "Il codice univoco, se viene messo, deve avere 7 caratteri";
                return ret;
            }

            var Pem = $("#Bpec").val();
            if (Pem.length > 0) {
                if (validateEmail(Pem) == false) {
                    return "Pec non valida";
                }
            }  

        }

        if ($("#Bcoduni").val() == "") {
            var Pem = $("#Bpec").val();
            if (Pem.length > 0)
            {
                if (validateEmail(Pem) == false) {
                    return "Pec non valida";
                }
            }        
        }

    }

    if ($("#oppers").prop("checked") == false) {
        ret = "Accettare il trattamento dei dati personali per fini amministrativi";
    }

    return ret;
}


function CheckAdult() {
    //almeno un adulto
    if (Number(Quantity[1]) > 0 && Number(Quantity[0]) == 0) {
        AlertCenter("Acquistare almeno un biglietto intero");
        return -1;
    }
    else
    {
        AlertCenterClose();
        return 0;
    }
}

function FillFakeForm() {
    $("#Bnome").val("Test");
    $("#Bcognome").val("Test");
    $("#Bemail").val("adelpin@hotmail.com");
    $("#Bconfmail").val("adelpin@hotmail.com");
    $("#Btelefono").val("0654321");
    $("#Bcap").val("00100");
    $("#oppers").prop("checked", true);

}

function Obbligatorio(campo, messaggio) {
    if ($("#" + campo).val().trim() == "") {
        return messaggio;
    }
    else {
        return "";
    }
}

function Letters(event) {
    var charCode = event.keyCode;

    if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 9 || charCode == 32)
        return true;
    else
        return false;
};

function Numbers(event) {
    //only numbers, numpad numbers, backspace,  delete, tab
    var charCode = event.keyCode;
    
    if (event.shiftKey) { return false }


    if ((charCode >= 48 && charCode <= 57) || charCode == 8 || charCode == 46 || charCode == 9 || (charCode >= 96 && charCode <= 105))
        return true;
    else
        return false;
};

function LettersAndNumbers(event) {
    //only letters numbers, numpad numbers, backspace, space, delete, tab
    var charCode = event.keyCode;

    if (event.shiftKey) { return false }

    //arrows
    if (charCode == 37 || charCode == 39) {
        return true;
    }

    if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode >= 48 && charCode <= 57) || charCode == 8 || (charCode >= 96 && charCode <= 105))
        return true;
    else
        return false;
};

function EmailCharacters(event) {
    //only letters numbers, numpad numbers, backspace, space, delete, tab
    //and . - _ @

    var charCode = event.keyCode;

    //characters allowed on email
    if (charCode == 190 || charCode == 189 || charCode == 192) {
        return true;
    }


    if (event.shiftKey) { return false }

    //arrows
    if (charCode == 37 || charCode == 39) {
        return true;
    }

    if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode >= 48 && charCode <= 57) || charCode == 8 || (charCode >= 96 && charCode <= 105))
        return true;
    else
        this.value = "";
    return false;
};

function validateEmail(em) {
    if (em.indexOf("'") != -1 || em.indexOf("+") != -1 || em.indexOf("(") != -1 || em.indexOf(")") != -1) {
        return false;
    }

    //inside a cshtml page @ became @@ 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (em.search(re) == -1) {
        return false;
    }

    return true;
}

//verify also the check digit:
function validateCodiceFiscale(cfins) {
    var cf = cfins.toUpperCase();
    var cfReg = /^[A-Z]{6}\d{2}[A-Z]\d{2}[A-Z]\d{3}[A-Z]$/;
    if (!cfReg.test(cf))
        return false;
    var set1 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var set2 = "ABCDEFGHIJABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var setpari = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var setdisp = "BAKPLCQDREVOSFTGUHMINJWZYX";
    var s = 0;
    for (i = 1; i <= 13; i += 2)
        s += setpari.indexOf(set2.charAt(set1.indexOf(cf.charAt(i))));
    for (i = 0; i <= 14; i += 2)
        s += setdisp.indexOf(set2.charAt(set1.indexOf(cf.charAt(i))));
    if (s % 26 != cf.charCodeAt(15) - 'A'.charCodeAt(0))
        return false;
    return true;
}

/////////////////////////////////
//alerts

//show alert bootstrap
function AlertShow(message) {
    $('#alert_placeholder').html('<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>')
}

function AlertCenter(message) {
    var htmc = '<div style="cursor: pointer; position:fixed; text-align: center; top: 30%; left: 50%; bottom:0px; width:400px; height:100px; margin: -30px 0 0 -200px; z-index: 9999;" ';
    htmc += ' class="alert alert-danger">';
    htmc += '<div style="font-family: calibri; margin-right: -8px; margin-top: -18px; color: #000; text-align: right;">x</div>';
    htmc += '<span>' + message + '</span></div > ';
    $('#alert_placeholder2').html(htmc);

}


function AlertCenterBig(message) {
    var htmc = '<div style="cursor: pointer; position:fixed; text-align: left; top: 30%; left: 50%; bottom:0px; width:600px; height:500px; margin: -30px 0 0 -200px; z-index: 9999;" ';
    htmc += ' class="alert alert-danger2">';
    htmc += '<div style="font-size: 18px;  font-family: calibri; margin-right: -8px; margin-top: -18px; color: #000; text-align: right;">x</div>';
    htmc += '<h4 style="color: #0095d4;">REGOLE DAL 6 AGOSTO:</h4>';
    htmc += '<span>' + message + '</span></div > ';
    $('#alert_placeholder2').html(htmc);

}


function AlertCenterBigMobile(message) {
    var htmc = '<div style="cursor: pointer; position:fixed; text-align: left; top: 6%; left: 50%; bottom:0px; width:300px; height:615px; margin: -30px 0 0 -150px; z-index: 9999;" ';
    htmc += ' class="alert alert-danger3">';
    htmc += '<div style="font-size: 18px;  font-family: calibri; margin-right: -8px; margin-top: -18px; color: #000; text-align: right;">x</div>';
    htmc += '<h4 style="color: #0095d4;">REGOLE DAL 6 AGOSTO:</h4>';
    htmc += '<span>' + message + '</span></div > ';
    $('#alert_placeholder2').html(htmc);

}

function AlertCenterClose()
{
    $('#alert_placeholder2').empty();
}

//close alert bootstrap
function AlertClose() {
    $('#alert_placeholder').html('');   
}


//operatore
function FunzioniOperatore()
{

    $("acqag").css("display", "none");
    $("#register").css("display", "none");

    if (LogUser == 0) {
        //no logged user
        $("#acqop").css("display", "none");
        //$("#acqsv").css("display", "none");
        //$("acqag").css("display", "none");
        $("#logout").css("display", "none");
        $("#login").css("display", "block");
        //$("#register").css("display", "block");
        $("#lbLoggedUser").css("visibility", "hidden");
    }
    else
    {
        $("#lbLoggedUser").text("UTENTE LOGGATO ");
        $("#lbLoggedUser").css("visibility", "visible");
    }


    if (LogUser == 2) {
        //admin
        $("#acqop").css("display", "block");
        //$("#acqsv").css("display", "block");
        //$("#acqag").css("display", "block");
        $("#logout").css("display", "block");
        $("#login").css("display", "none");
        //$("#register").css("display", "none");
    }

    if (LogUser == 4) {
        //operator
        $("#acqop").css("display", "block");
        //$("#acqsv").css("display", "block");
        //$("#acqag").css("display", "block");
        $("#logout").css("display", "block");
        $("#login").css("display", "none");
        //$("#register").css("display", "none");
    }

    if (LogUser == 5) {
        //hotel operator      
        $("#Hotall").css("display", "block");
        $("#Sales").css("display", "block");
        $("#logout").css("display", "block");
        $("#login").css("display", "none");
        //$("#register").css("display", "none");

    }

}



///////////////////////////////////
//cookies

function GetCookieBuyer() {
    Name = getCookie("Name");
    Surname = getCookie("Surname");
    Email = getCookie("Email");
    TelePhone = getCookie("TelePhone");
    Zip = getCookie("Zip");
    Invoice = getCookie("Invoice");
    CompanyName = getCookie("CompanyName");
    VatNumber = getCookie("VatNumber");
    SSNumber = getCookie("SSNumber");
    Address = getCookie("Address");
    City = getCookie("City");
    Province = getCookie("Province");
    Private = getCookie("Private");

    //default:private if cookies empty:
    if (Address === "") {
        $("input[name='oppriv'][value='Y']").prop('checked', true);
        HideCompany();
        $("#Fcodfisc").text("Codice Fiscale *");
        Private = 'Y';
        return;
    }

    //default:company if cookies empty:
    //if (Address === "") {
    //    $("input[name='oppriv'][value='']").prop('checked', true);
    //    ShowCompany();
    //    $("#Fcodfisc").text("Codice Fiscale");
    //    Private = '';
    //    return;
    //}



    ShowCompany();
    if (Private === 'Y') {
        HideCompany();
    }
    
}

function SetCookieBuyer() {
    setCookie("Name", Name);
    setCookie("Surname", Surname);
    setCookie("Email", Email);
    setCookie("TelePhone", TelePhone);
    setCookie("Zip", Zip);

    setCookie("Invoice", Invoice);
    setCookie("CompanyName", CompanyName);
    setCookie("VatNumber", VatNumber);
    setCookie("SSNumber", SSNumber);
    setCookie("Address", Address);
    setCookie("City", City);
    setCookie("Province", Province);
    setCookie("MailingList", MailingList);
    setCookie("Private", Private);
    setCookie("CodUni", CodUni);
    setCookie("Pec", Pec);
}

function DeleteCookieBuyer()
{
    setCookie("Name", Name);
    setCookie("Surname", Surname);
    setCookie("Email", Email);
    setCookie("TelePhone", TelePhone);
    setCookie("Zip", Zip);

    setCookie("Invoice", Invoice);
    setCookie("CompanyName", CompanyName);
    setCookie("VatNumber", VatNumber);
    setCookie("SSNumber", SSNumber);
    setCookie("Address", Address);
    setCookie("City", City);
    setCookie("Province", Province);
    setCookie("MailingList", MailingList);
    setCookie("Private", Private);
    setCookie("CodUni", CodUni);
    setCookie("Pec", Pec);
}

function DeleteAllCookies() {

    delcookie("bscn");

    //delete tickets cookies
    delcookie("Day");
    delcookie("DiscountCode");
    delcookie("ticket");
    delcookie("activity");
    delcookie("menu");
    delcookie("animal");

    delcookie("SubAniQuantity");

    delcookie("Quantity");
    delcookie("AniQuantity");
    delcookie("ActQuantity");
    delcookie("MnuQuantity");
 
  

    //delete hotels cookies
    delcookie("DayArrive");
    delcookie("DayDeparture");
    delcookie("Nrooms");
    delcookie("HotelId");
    delcookie("HotChangeDate");
    delcookie("HotelPriceId");
    delcookie("HotelDescription");
    delcookie("TotPrice");
    delcookie("adults");
    delcookie("kids");
    delcookie("kidsfree");

    //delete subscriptions cookies
    delcookie("SubscriptionId");
    delcookie("Quantity");
    delcookie("SubscriptionTotPrice");
    delcookie("Names");
    delcookie("Surnames");

}

function setCookie(cname, cvalue) {
    if (cvalue == null) { cvalue = "" };

    var exdays = 7;
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function delcookie(cname) {
    var d = new Date();
    d.setTime(d.getTime());
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "='';" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            var dato = c.substring(name.length, c.length);
            return dato;
        }
    }
    return "";
}

//excel functions
function clipboard(_this) {
    var $this = $(_this);
    var $container = $this.closest("div.ibox");
    var $table = $container.find("table");
    var namefile = $this.data("namefile").toUpperCase() + '-' + moment().format("YYYY-MM-DD HH:mm:ss");

    var table_html = $table.prop('outerHTML');

    table_html = table_html.replace(new RegExp('€', "g"), '');


    exportToExcel(table_html, namefile);

}

function exportToExcel(htmls, namefile) {

    var uri = 'data:application/vnd.ms-excel;base64,';
    var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
    var base64 = function (s) {
        return window.btoa(unescape(encodeURIComponent(s)))
    };

    var format = function (s, c) {
        return s.replace(/{(\w+)}/g, function (m, p) {
            return c[p];
        })
    };

    var ctx = { worksheet: 'Worksheet', table: htmls}

    var link = document.createElement("a");
    link.download = namefile;
    link.href = uri + base64(format(template, ctx));
    link.click();
}



