﻿//////////////////////////////////////
//dynamic price calendar
var DataOggi;
var SameDay = false;


var ListaDataOld = [], ListaBasePrice1Old = [], ListaPrice1Old = [], ListaBasePrice2Old = [], ListaPrice2Old = [], ListaTimeEntranceOld = [];
var ListaData = [], ListaBasePrice1 = [], ListaPrice1 = [], ListaBasePrice2 = [], ListaPrice2 = [], ListaTimeEntrance = [];
var ListaHotelPrice = [];
var ListaHotelPriceOld = [];

var ListaDataA = [], ListaTimeEntranceA = [], ListaDataD = [], ListaTimeEntranceD = [];
var ListaDataF = [], ListaTimeEntranceF = [];
var ListaHotelPriceA = [];
var ListaHotelPriceD = [];
var ListaFamilyPrice = [];

var IdGroup = "";

var cal = '<table style="margin:0 auto; padding:0; width: 100%;">'; //95%
cal += '<tr>';
cal += '<td id ="CalPrv" onclick="PrevMonth()" style="width:30px; text-align:center;">';
cal += '<a href="javascript:" class="btn btn-lg" style="display: inline;padding:0;">';
cal += '<i class="fa fa-angle-left" aria-hidden="true" style="font-size:40px;"></i>';
cal += '</a>';
cal += '</td>';
cal += '<td style="margin: 0; padding: 0; width:280px; text-align:center; "><i class="fa fa-calendar calendarioIcon" aria-hidden="true"></i>';
cal += '<span  id="meseanno" style="font-size: 21px;letter-spacing:3px;text-transform: uppercase;"></span>';
cal += '</td>';
cal += '<td id ="CalNex" onclick="NextMonth()" style="width:30px; text-align:center;">';
cal += '<a href="javascript:" class="btn btn-lg" style="display: inline;padding:0;">';
cal += '<i class="fa fa-angle-right" aria-hidden="true" style="font-size:40px;"></i>';
cal += '</a>';
cal += '</td>';
cal += '</tr>';
cal += '</table>';
cal += '<table style="margin:0 auto; padding:0; width: 100%;">'; //95%
cal += '<tr>';
cal += '<td class="cellas">L</td>';
cal += '<td class="cellas">M</td>';
cal += '<td class="cellas">M</td>';
cal += '<td class="cellas">G</td>';
cal += '<td class="cellas">V</td>';
cal += '<td class="cellas">S</td>';
cal += '<td class="cellas">D</td>';
cal += '</tr>';


function LastDay(Day) {
    //detect if is the last day
    //or all the next days empty
    
    var dd = Day.substr(0, 2);
    var mm = Number(Day.substr(3, 2)) - 1;
    var aa = Number(Day.substr(6, 4));

    var last = 32 - new Date(aa, mm, 32).getDate();
    
    if (dd === last) {
        //last day
        return true;
    }

    //all the next days empty
    var ret = true;
    for (var i = dd; i < last; i++) {
        var per = ListaTimeEntrance[i];

        if (per === null) { per = 0;}

        if (per !== "0") {
            return false;
        }
    }

    return ret;
}


function FirstDayInMonth(DayStart) {
    var Day = DayStart;

    var dd = Day.substr(0, 2);
    var mm = Day.substr(3, 2);
    var aa = Day.substr(6, 4);

    var lungmese = ListaData.length;
    var start = Number(dd);

    //default next day available:
    for (var i = start; i <= lungmese; i++) {
        var per = ListaTimeEntrance[i - 1];

        if (per != "0") {
            var df = i;
            if (i < 10) { df = "0" + df; }

            Day = df + "/" + mm + "/" + aa;
            break;
        }
    }

    return Day;
}


function ConvertDate(Date)
{
    //convert dd/mm/yyyy in date
    var dd = DayStart.substr(0, 2);
    var mm = DayStart.substr(3, 2);
    var aa = DayStart.substr(6, 4);
    var mydate = new Date(aa, Number(mm) - 1, dd);

    return mydate;
}

function Tomorrow(DayStart)
{
    var dd = DayStart.substr(0, 2);
    var mm = DayStart.substr(3, 2);
    var aa = DayStart.substr(6, 4);
    var mydate = new Date(aa, Number(mm) - 1, dd);
    mydate.setDate(mydate.getDate() + 1);               
    dd = ("0" + mydate.getDate()).slice(-2);
    mm = ("0" + Number(mydate.getMonth() + 1)).slice(-2);
    aa = mydate.getFullYear();
    var Day = dd + "/" + mm + "/" + aa;

    return Day;
}

function FirstDayAvailable(DayStart) {
    
    //var Day = DataOggi;
    var Day = DayStart;

    var dd = Day.substr(0, 2);
    var mm = Day.substr(3, 2);
    var aa = Day.substr(6, 4);
    var mmlista;

    if (LastDay(Day) == true) {

        NextMonth();

        if (mm == 12) {
            aa = Number(aa) + 1;
            mm = 0;
        }

        mm = Number(mm) + 1;
        mm = ("0" + mm).slice(-2);

        Day = "01/" + mm + "/" + aa;

        Day = FirstDayInMonth(Day);

        return Day;      
    }  

        mmlista = ListaData[0].substr(3, 2);
        //primo giorno disponibile al mese successivo
        if (mm != mmlista) {
            dd = "0";
            mm = mmlista;
            Day = "01/" + mm + "/" + aa;
        }

    var lungmese = ListaData.length;

        //acquajoss same day:
        //var start = Number(dd) + 1;
    debugger;
        var start = Number(dd);

        //default next day available:
        for (var i = start; i <= lungmese; i++) {
            var per = ListaTimeEntrance[i - 1];

            if (per != "0") {
                var df = i;
                if (i < 10) { df = "0" + df; }

                Day = df + "/" + mm + "/" + aa;
                break;
            }
        }

        return Day;

}

function GetCalendario(anno, mese) {

    var URL = "";

    if (IdGroup !== "")
    {
        URL = "/Acquisto/CreaCalendarioGruppi";
    }
    else
    {
        URL = "/Acquisto/CreaCalendario";
    }
    

    var Info = {
        Anno: anno,
        Mese: mese
    };

    var promise = $.post(URL, Info);

    promise.done(function (data) {    
        ListaData = data.ListaData;

        ListaId1 = data.ListaId1;
        ListaBasePrice1 = data.ListaBasePrice1;
        ListaPrice1 = data.ListaPrice1;

        ListaId2 = data.ListaId2;
        ListaBasePrice2 = data.ListaBasePrice2;
        ListaPrice2 = data.ListaPrice2;



        ListaTimeEntrance = data.ListaTimeEntrance;

        if (data.ListaData.length == 0)
        {
            //over the last month available go back
            ListaData = ListaDataOld;
            ListaBasePrice1 = ListaBasePrice1Old;
            ListaPrice1 = ListaBasePrice1Old;
            ListaBasePrice2 = ListaBasePrice2Old;
            ListaPrice2 = ListaPrice2Old;
            ListaTimeEntrance = ListaTimeEntranceOld;

            return;
        }

        ListaDataOld = ListaData;
        ListaBasePrice1Old = ListaBasePrice1;
        ListaPrice1Old = ListaPrice1;
        ListaBasePrice2Old = ListaBasePrice2;
        ListaPrice2Old = ListaPrice2;
        ListaTimeEntranceOld = ListaTimeEntrance;
        ListaHotelPriceOld = ListaHotelPrice;

        ///////////////////////////////////
        $("#DynCalendar").empty();
        CreaCalendario();
    });

    promise.fail(function (err) { console.log("error promise: " + URL); });

}

function Mesi(mm) {
    var mese = "";

    switch (mm) {
        case 0:
            mese = "Gennaio";
            break;
        case 1:
            mese = "Febbraio";
            break;
        case 2:
            mese = "Marzo";
            break;
        case 3:
            mese = "Aprile";
            break;
        case 4:
            mese = "Maggio";
            break;
        case 5:
            mese = "Giugno";
            break;
        case 6:
            mese = "Luglio";
            break;
        case 7:
            mese = "Agosto";
            break;
        case 8:
            mese = "Settembre";
            break;
        case 9:
            mese = "Ottobre";
            break;
        case 10:
            mese = "Novembre";
            break;
        case 11:
            mese = "Dicembre";
    }

    return mese;

}

function CreaCalendario() {

    var sec = "";

    for (var i = 1; i < 7; i++) {
        sec += "<tr>";

        for (var j = 1; j < 8; j++) {
            sec += '<td id="cal_' + i + j + '"></td>';
        }
        sec += "</tr>";
    }

    cal = cal + sec + '</table>';

    var htm = cal;

    $("#DynCalendar").append(htm);

    var data = ListaData[0];

    var gg = "01";
    var mm = data.substr(3, 2);
    mm = mm - 1;
    var aa = data.substr(6, 4);

    var mese = Mesi(mm);

    $("#meseanno").text(mese + " " + aa);

    var doggi = DataOggi;
    var moggi = Number(doggi.substr(3, 2)) - 1;
    var noggi = Number(doggi.substr(0, 2));

    //week day of the first month day, compatible with IE:
    var d = new Date(aa, mm, 1);
    start = d.getDay();
    if (start == 0) { start = 7; }

    //vuoti prima:
    if (start > 1) {
        for (var i = 1; i < start; i++) {
            $("#cal_1" + i).addClass("cella");
        }
    }

    var riga = 1;
    var lungmese = ListaData.length;

    for (var i = 1; i <= lungmese; i++) {
        colonna = (start - 1 + i) % 7;
        var col = colonna;
        if (col == 0) { col = 7; }

        if (colonna == 1 && i > 1) { riga++; }

        var per = ListaTimeEntrance[i - 1];

        $("#cal_" + riga + col).addClass("cella");


        var cell = '<div class="sopra" onclick="SetCalendar(' + i + ' )">' + i + '</div>';
        cell += '<div style="background-color:' + ListaTimeEntrance[i - 1] + '; height:33px; color: white;">';
        cell += '<div class="dynprice">' + FormatMoney(ListaPrice1[i - 1]) + '</div>';
        cell += '<div class="strikec">' + FormatMoney(ListaBasePrice1[i - 1]) + '</div>';
        cell += '</div>';

        var cellno = '<div class="soprag">' + i + '</div>';
        cellno += '<div class="grigio"></div>';

        if (per == "0") {
            $("#cal_" + riga + col).append(cellno);
            continue;
        }


        //for acquajoss the same day also
        if (moggi == mm) {
            if (i >= noggi && i <= lungmese) {
                $("#cal_" + riga + col).append(cell);
            }
            else {
                $("#cal_" + riga + col).append(cellno);
            }
           
        }
        else {
            if (i <= lungmese) {
                $("#cal_" + riga + col).append(cell);
            }
        }

    }

    //vuoti dopo:
    for (var i = col + 1; i < 8; i++) {
        $("#cal_" + riga + i).addClass("cella");
    }

    if (riga == 4) {
        for (var i = 1; i < 8; i++) { $("#cal_5" + i).addClass("cella"); }
        for (var i = 1; i < 8; i++) { $("#cal_6" + i).addClass("cella"); }
    }

    if (riga == 5) {
        for (var i = 1; i < 8; i++) { $("#cal_6" + i).addClass("cella"); }
    }

}

function PrevMonth() {
    var data = ListaData[0];
    var mm = parseInt(data.substr(3, 2)) - 1;
    var aa = data.substr(6, 4);
 
    //non deve andare al mese prima di oggi
    var moggi = parseInt(DataOggi.substr(3, 2));
    var aoggi = parseInt(DataOggi.substr(6, 4));

    if (mm < moggi && aa == aoggi) {
        return;
    }

    if (mm == 0) {
        //da gennaio a dicembre anno precedente
        mm = 12;
        aa = Number(aa) - 1;
    }

    GetCalendario(aa, mm);
}

function NextMonth() {
    var data = ListaData[0];
    
    var mm = parseInt(data.substr(3, 2)) + 1;
    var aa = data.substr(6, 4);


    if (mm == 13) {
        //next year
        aa = Number(aa) + 1;
        mm = 1;
    }

    GetCalendario(aa, mm);


}

function SetCalendar(i) {
    FirstTime = false;
    TotPrice = "0";
    $("#TotPrice").text(String.fromCharCode(160));

    Day = ListaData[i - 1];
    
    $(".sopra").on('click', function ()
    {
        if ($(window).width() <= 768) {
            $('html, body').animate({
                scrollTop: parseInt($(".bookReserv").offset().top)
            }, 2000);

        }       
    });

    //if (Day == DataOggi) {
    //    AlertShow("Non è possibile acquistare biglietti per il giorno stesso");
    //    return;
    //}

    //special for halloween
    var ds = new Date('2019-09-28');
    var de = new Date('2019-11-03');
    var dd = ConvertDate(Day);

    if (dd >= ds && dd <= de) {
        //$("#Not_1").text("Bambini tra 100 e 130 cm MASCHERATI");
    }
    else
    {
        //$("#Not_1").text("Bambini tra 100 e 130 cm ed over 65");
    }  

    $("#datafinale").val(Day);

    NoCalcSmq = false;
    if (Day != "") {
        if (Day == OldDay) {
            //return;
        }

        ///
        //another day choose
        ////$('#loader').modal('show');
        NoCalcSmq = true;
        OldDay = Day;

        //$('#loader').modal('show');
        

        if (Discount != "") {
            Prices[0] = 0;
            Prices[1] = 0;
            TotPrice = 0;
            $("#TotPrice").text(FormatMoney(TotPrice) + "€")
            return;
        }

        Prices[0] = ListaPrice1[i - 1];
        Prices[1] = ListaPrice2[i - 1];

        $("#" + phprc + "0").text(FormatMoney(Prices[0]) + "€");
        $("#" + phprc + "1").text(FormatMoney(Prices[1]) + "€");

        TotPrice = 0;
        for (var i = 0; i < ticket.length; i++) {
            TotPrice += Number(Quantity[i]) * Number(Prices[i].replace(",", "."));
        }

        $("#TotPrice").text(FormatMoney(TotPrice) + "€");
    }
    else {

        $("#continue").hide();
    }

}

//////////////////////////
//hotel calendars
var calH = '<table style="margin:0; padding:0; width: 100%; background-color: white; border: 1px solid black;">'; //95%
calH += '<tr>';
calH += '<td id ="CalPrv" onclick="PrevMonthH_()" style="width:30px; text-align:center;">';
calH += '<a href="javascript:" class="btn btn-lg" style="display: inline;">';
calH += ' <span class="glyphicon glyphicon-chevron-left"></span>';
calH += '</a>';
calH += '</td>';
calH += '<td style="margin: 0; padding: 0; text-align:center; ">';
calH += '<span  id="MeseAnnoH_" style="font-size: 18px;"></span>';
calH += '</td>';
calH += '<td id ="CalNex" onclick="NextMonthH_()" style="width:30px; text-align:center;">';
calH += '<a href="javascript:" class="btn btn-lg" style="display: inline;">';
calH += '<span class="glyphicon glyphicon-chevron-right"></span>';
calH += '</a>';
calH += '</td>';
calH += '</tr>';
calH += '</table>';
calH += '<table style="margin:0; padding:0; width: 100%; background-color: white; border: 1px solid black;">'; //95%
calH += '<tr>';
calH += '<td class="cellash">L</td>';
calH += '<td class="cellash">M</td>';
calH += '<td class="cellash">M</td>';
calH += '<td class="cellash">G</td>';
calH += '<td class="cellash">V</td>';
calH += '<td class="cellash">S</td>';
calH += '<td class="cellash">D</td>';
calH += '</tr>';


function GetCalendarioFamily(anno, mese) {

    var URL = "/Acquisto/CreaCalendarioFamily";

    var Info = {
        Anno: anno,
        Mese: mese
    };

    var promise = $.post(URL, Info);

    promise.done(function (data) {

        ListaData = data.ListaProductData;

        if (ListaData.length == 0) {
            return;
        }

        ListaDataF = ListaData;
        ListaFamilyPrice = data.ListaProductPrice;

        CreaCalendarioHotel_dynamic("F");
        //CreaCalendarioHotel("F");
    });

    promise.fail(function (err) { console.log("error promise: " + URL); });
}


function GetCalendarioB(anno, mese) {

    var URL = "/Acquisto/CreaCalendario";

    var Info = {
        Anno: anno,
        Mese: mese
    };

    var promise = $.post(URL, Info);

    promise.done(function (data) {

        ListaData = data.ListaData;

        if (ListaData.length == 0) {
            return;
        }

        ListaDataF = ListaData;
        ListaFamilyPrice = data.ListaProductPrice;

        CreaCalendarioHotel("F");
    });

    promise.fail(function (err) { console.log("error promise: " + URL); });
}

function GetCalendarioHotel(anno, mese, scelta) {

    if (scelta == "F") {
        GetCalendarioFamily(anno, mese);
        return;
    }

    var URL = "/Acquisto/CreaCalendarioHotel";

    var Info = {
        Anno: anno,
        Mese: mese
    };

    var promise = $.post(URL, Info);

    promise.done(function (data) {

        ListaData = data.ListaProductData;
        ListaTimeEntrance = data.ListaTimeEntrance;

        if (ListaData.length == 0) {
            return;
        }

        if (scelta == "A") {
            ListaDataA = ListaData;
            //ListaTimeEntranceA = data.ListaTimeEntrance;
            ListaHotelPriceA = data.ListaProductPrice;
            $("#HotelCalArrive").empty();
        }

        if (scelta == "D") {
            ListaDataD = ListaData;
            //ListaTimeEntranceD = data.ListaTimeEntrance;
            ListaHotelPriceD = data.ListaProductPrice;
            $("#HotelCalDeparture").empty();
        }

        if (data.ListaProductData.length == 0) {
            //over the last month available go back
            ListaDataA = ListaDataOld;
            //ListaTimeEntranceA = ListaTimeEntranceOld;
            ListaHotelPriceA = ListaHotelPriceOld;

            ListaDataD = ListaDataOld;
            //ListaTimeEntranceD = ListaTimeEntranceOld;
            ListaHotelPriceD = ListaHotelPriceOld;

            return;
        }

        CreaCalendarioHotel(scelta);
    });

    promise.fail(function (err) { console.log("error promise: " + URL); });

}


function GetCalendarioH(anno, mese, scelta) {

    var URL = "/Acquisto/CreaCalendario";

    var Info = {
        Anno: anno,
        Mese: mese,
        Promotion: true
    };

    var promise = $.post(URL, Info);

    promise.done(function (data) {

        ListaData = data.ListaData;

        if (ListaData.length == 0) {
            return;
        }


        ListaTimeEntrance = data.ListaTimeEntrance;

        ///////////////////////////////////
        if (scelta == "A") {
            ListaDataA = data.ListaData;
            ListaTimeEntranceA = data.ListaTimeEntrance;
            $("#HotelCalArrive").empty();
        }

        if (scelta == "D") {
            ListaDataD = data.ListaData;
            ListaTimeEntranceD = data.ListaTimeEntrance;
            $("#HotelCalDeparture").empty();
        }

        if (scelta == "F" || scelta == "B") {
            ListaDataF = data.ListaData;
            ListaTimeEntranceF = data.ListaTimeEntrance;
            $("#FamilyCal").empty();
        }

        if (data.ListaData.length == 0) {
            //over the last month available go back
            ListaDataA = ListaDataOld;
            ListaTimeEntranceA = ListaTimeEntranceOld;

            ListaDataD = ListaDataOld;
            ListaTimeEntranceD = ListaTimeEntranceOld;

            ListaDataF = ListaDataOld;
            ListaTimeEntranceF = ListaTimeEntranceOld;

            return;
        }

        CreaCalendarioH(scelta);
    });

    promise.fail(function (err) { console.log("error promise: " + URL); });

}

function CreaCalendarioH(scelta) {
    
    var sec = "";

    var box = "cal" + scelta + "_";

    for (var i = 1; i < 7; i++) {
        sec += "<tr>";

        for (var j = 1; j < 8; j++) {
            sec += '<td class="cellah" id="' + box + i + j + '"></td>';
        }
        sec += "</tr>";
    }

    var htm = calH + sec + '</table>';

    var prevm = "PrevMonthH" + scelta;
    var nextm = "NextMonthH" + scelta;
    var meseanno = "meseannoH" + scelta;

    htm = htm.replace("PrevMonthH_", prevm);
    htm = htm.replace("NextMonthH_", nextm);
    htm = htm.replace("MeseAnnoH_", meseanno);

    var data;
    var lungmese;
    var TimeEntrance;

    if (scelta == "A") {
        data = ListaDataA[0];
        lungmese = ListaDataA.length;
        TimeEntrance = ListaTimeEntranceA;
        $("#HotelCalArrive").append(htm);
    }

    if (scelta == "D") {
        data = ListaDataD[0];
        lungmese = ListaDataD.length;
        TimeEntrance = ListaTimeEntranceD;
        $("#HotelCalDeparture").append(htm);
    }


    if (scelta == "F" || scelta == "B") {
        data = ListaDataF[0];
        lungmese = ListaDataF.length;
        TimeEntrance = ListaTimeEntranceF;
        //$("#FamilyCal").empty();
        $("#FamilyCal").append(htm);
        $(".pickerCalen").addClass('viewCalendar');
    }


    var gg = "01";
    var mm = data.substr(3, 2);
    mm = mm - 1;
    var aa = data.substr(6, 4);

    var mese = Mesi(mm);

    $("#" + meseanno).text(mese + " " + aa);

    var doggi = DataOggi;
    var moggi = Number(doggi.substr(3, 2)) - 1;
    var noggi = Number(doggi.substr(0, 2));

    //week day of the first month day, compatible with IE:
    var d = new Date(aa, mm, 1);
    start = d.getDay();
    if (start == 0) { start = 7; }

    //vuoti prima:
    if (start > 1) {
        for (var i = 1; i < start; i++) {
            $("#" + box + "_1" + i).addClass("cellah");
        }
    }

    var riga = 1;

    for (var i = 1; i <= lungmese; i++) {
        colonna = (start - 1 + i) % 7;
        var col = colonna;
        if (col == 0) { col = 7; }

        if (colonna == 1 && i > 1) { riga++; }

        var per = TimeEntrance[i - 1];

        var cell;

        if (scelta == "A") {
            cell = '<div onclick="SetCalendarHA(' + i + ')">' + i + '</div>';

        }

        if (scelta == "D") {
            cell = '<div onclick="SetCalendarHD(' + i + ')">' + i + '</div>';
        }

        if (scelta == "F") {
            cell = '<div onclick="SetCalendarHF(' + i + ')">' + i + '</div>';
        }

        if (scelta == "B") {
            cell = '<div onclick="SetCalendarHB(' + i + ')">' + i + '</div>';
        }


        var cellno = '<div>' + i + '</div>';

        if (per == "0") {
            $("#" + box + riga + col).addClass("cellah");
            $("#" + box + riga + col).append(cellno);
            continue;
        }



        if (moggi == mm) {
            if (i > noggi && i <= lungmese) {
                $("#" + box + riga + col).addClass("soprah");
                $("#" + box + riga + col).css("background-color", TimeEntrance[i - 1]); 
                $("#" + box + riga + col).append(cell);
            }
            else {
                $("#" + box + riga + col).addClass("cellah");
                $("#" + box + riga + col).append(cellno);
            }
        }
        else {
            if (i <= lungmese) {
                $("#" + box + riga + col).addClass("soprah");
                $("#" + box + riga + col).css("background-color", TimeEntrance[i - 1]); 
                $("#" + box + riga + col).append(cell);
            }
        }

    }

    //vuoti dopo:
    for (var i = col + 1; i < 8; i++) {
        $("#" + box + riga + i).addClass("cellah");
    }

    if (riga == 4) {
        for (var i = 1; i < 8; i++) { $("#" + box + "_5" + i).addClass("cellah"); }
        for (var i = 1; i < 8; i++) { $("#" + box + "_6" + i).addClass("cellah"); }
    }

    if (riga == 5) {
        for (var i = 1; i < 8; i++) { $("#" + box + "_6" + i).addClass("cellah"); }
    }

}

function CreaCalendarioHotel(scelta) {
    //static
    var sec = "";
 
    var lcella = '54';

    var box = "cal" + scelta + "_";

    for (var i = 1; i < 7; i++) {
        sec += "<tr>";

        for (var j = 1; j < 8; j++) {
            sec += '<td class="cellah" id="' + box + i + j + '"><div></div></td>';
        }

        //style="width: ' + lcella + 'px;"
        sec += "</tr>";
    }

    var htm = calH + sec + '</table>';

    var prevm = "PrevMonthH" + scelta;
    var nextm = "NextMonthH" + scelta;
    var meseanno = "meseannoH" + scelta;

    htm = htm.replace("PrevMonthH_", prevm);
    htm = htm.replace("NextMonthH_", nextm);
    htm = htm.replace("MeseAnnoH_", meseanno);

    var data;
    var lungmese;
    var HotelPrice;
    var HotelBasePrice;
    var FamilyPrice;
    var ProductPrice;

    if (scelta == "A") {
        data = ListaDataA[0];     
        lungmese = ListaDataA.length;
        HotelPrice = ListaHotelPriceA;
        ProductPrice = HotelPrice;
        //$("#HotelCalArrive").empty();
        $("#HotelCalArrive").append(htm);
    }

    if (scelta == "D") {
        data = ListaDataD[0];
        lungmese = ListaDataD.length;
        HotelPrice = ListaHotelPriceD;
        ProductPrice = HotelPrice;
        //$("#HotelCalDeparture").empty();
        $("#HotelCalDeparture").append(htm);
    }


    if (scelta == "F") {
        data = ListaDataF[0];
        lungmese = ListaDataF.length;
        FamilyPrice = ListaFamilyPrice;
        ProductPrice = FamilyPrice;
        $("#FamilyCal").empty();
        $("#FamilyCal").append(htm);
        $(".pickerCalen").addClass('viewCalendar');
    }


    var gg = "01";
    var mm = data.substr(3, 2);
    mm = mm - 1;
    var aa = data.substr(6, 4);

    var mese = Mesi(mm);

    $("#" + meseanno).text(mese + " " + aa);

    var doggi = DataOggi;
    var moggi = Number(doggi.substr(3, 2)) - 1;
    var noggi = Number(doggi.substr(0, 2));

    //week day of the first month day, compatible with IE:
    var d = new Date(aa, mm, 1);
    start = d.getDay();
    if (start == 0) { start = 7; }

    //vuoti prima:
    if (start > 1) {
        for (var i = 1; i < start; i++) {
            $("#" + box + "_1" + i).addClass("cellah");
        }
    }

    var riga = 1;
    
    for (var i = 1; i <= lungmese; i++) {
        colonna = (start - 1 + i) % 7;

        var col = colonna;
        if (col == 0) { col = 7; }

        if (colonna == 1 && i > 1) { riga++; }

        //var per = TimeEntrance[i - 1];
        var DynPr = FormatMoney(ProductPrice[i - 1]);
        
        var cell;

        if (scelta == "A") {
            cell = '<div onclick="SetCalendarHA(' + i + ')">' + i + '</div>';
        }

        if (scelta == "D") {
            cell = '<div onclick="SetCalendarHD(' + i + ')">' + i + '</div>';
        }

        if (scelta == "F") {
            cell = '<div onclick="SetCalendarHF(' + i + ')">' + i + '</div>';
        }

       
        var cellno = i;

        if (DynPr == "0,00") {
            $("#" + box + riga + col).addClass("cellah");
            $("#" + box + riga + col).append(cellno);
            continue;
        }



        if (moggi == mm) {
            var append = true;

            if (Prodotto == "hotel")
            {
                //for parco + hotel the same day also
                if (i >= noggi && i <= lungmese) { append = false;}               
            }
            else
            {
                //from the day tomorrow
                if (i > noggi && i <= lungmese) {append = false;}            
            }

            if (append == false) {
                $("#" + box + riga + col).addClass("soprah");
                $("#" + box + riga + col).css("background-color", ListaTimeEntrance[i - 1]); 
                $("#" + box + riga + col).append(cell);
            }
            else {
                $("#" + box + riga + col).addClass("cellah");
                $("#" + box + riga + col).append(cellno);
            }

           
        }
        else {
            if (i <= lungmese) {
                $("#" + box + riga + col).addClass("soprah");
                $("#" + box + riga + col).css("background-color", ListaTimeEntrance[i - 1]); 
                $("#" + box + riga + col).append(cell);
            }
        }

    }

    //vuoti dopo:
    for (var i = col + 1; i < 8; i++) {
        $("#" + box + riga + i).addClass("cellah");
    }

    if (riga == 4) {
        for (var i = 1; i < 8; i++) { $("#" + box + "_5" + i).addClass("cellah"); }
        for (var i = 1; i < 8; i++) { $("#" + box + "_6" + i).addClass("cellah"); }
    }

    if (riga == 5) {
        for (var i = 1; i < 8; i++) { $("#" + box + "_6" + i).addClass("cellah"); }
    }

}

//old way dynamic prices
function CreaCalendarioHotel_dynamic(scelta) {

    var sec = "";

    var lcella = '54';

    var box = "cal" + scelta + "_";

    for (var i = 1; i < 7; i++) {
        sec += "<tr>";

        for (var j = 1; j < 8; j++) {
            //sec += '<td class="cellah" id="' + box + i + j + '"><div style="width: ' + lcella + 'px;"></div></td>';
            sec += '<td class="cellah" id="' + box + i + j + '"><div></div></td>';
        }
        sec += "</tr>";
    }

    var htm = calH + sec + '</table>';

    var prevm = "PrevMonthH" + scelta;
    var nextm = "NextMonthH" + scelta;
    var meseanno = "meseannoH" + scelta;

    htm = htm.replace("PrevMonthH_", prevm);
    htm = htm.replace("NextMonthH_", nextm);
    htm = htm.replace("MeseAnnoH_", meseanno);

    var data;
    var lungmese;
    var HotelPrice;
    var FamilyPrice;
    var ProductPrice;

    if (scelta == "A") {
        data = ListaDataA[0];
        lungmese = ListaDataA.length;
        HotelPrice = ListaHotelPriceA;
        ProductPrice = HotelPrice;
        //$("#HotelCalArrive").empty();
        $("#HotelCalArrive").append(htm);
    }

    if (scelta == "D") {
        data = ListaDataD[0];
        lungmese = ListaDataD.length;
        HotelPrice = ListaHotelPriceD;
        ProductPrice = HotelPrice;
        //$("#HotelCalDeparture").empty();
        $("#HotelCalDeparture").append(htm);
    }


    if (scelta == "F") {      
        data = ListaDataF[0];
        lungmese = ListaDataF.length;
        FamilyPrice = ListaFamilyPrice;
        ProductPrice = FamilyPrice;
        $("#FamilyCal").empty();
        $("#FamilyCal").append(htm);
        $(".pickerCalen").addClass('viewCalendar');
    }

    var gg = "01";
    var mm = data.substr(3, 2);
    mm = mm - 1;
    var aa = data.substr(6, 4);

    var mese = Mesi(mm);

    $("#" + meseanno).text(mese + " " + aa);

    var doggi = DataOggi;
    var moggi = Number(doggi.substr(3, 2)) - 1;
    var noggi = Number(doggi.substr(0, 2));

    //week day of the first month day, compatible with IE:
    var d = new Date(aa, mm, 1);
    start = d.getDay();
    if (start == 0) { start = 7; }

    //vuoti prima:
    if (start > 1) {
        for (var i = 1; i < start; i++) {
            $("#" + box + "_1" + i).addClass("cellah");
            $("#" + box + "_1" + i).attr('style', 'height: 59px');          
        }
    }

    var riga = 1;


    for (var i = 1; i <= lungmese; i++) {
        colonna = (start - 1 + i) % 7;

        var col = colonna;
        if (col == 0) { col = 7; }

        if (colonna == 1 && i > 1) { riga++; }

        //var per = TimeEntrance[i - 1];
        var DynPr = FormatMoney(ProductPrice[i - 1]);

        

        var cell;

        var cellpr = '<div style="background-color:#999; height:33px; color: white;">';
        cellpr += '<div class="dynprice">' + DynPr + '</div><div class="strikec">' + FormatMoney(DynBasePrice) + '</div></div>';

        if (scelta == "A") {
            cell = '<div onclick="SetCalendarHA(' + i + ')">' + i + cellpr + '</div>';
        }

        if (scelta == "D") {
            cell = '<div onclick="SetCalendarHD(' + i + ')">' + i + cellpr + '</div>';
        }

        if (scelta == "F") {
            cell = '<div onclick="SetCalendarHF(' + i + ')">' + i + cellpr + '</div>';
        }


        var cellno = i;

        if (DynPr == "0,00") {
            $("#" + box + riga + col).addClass("cellah");
            $("#" + box + riga + col).append(cellno);
            continue;
        }



        if (moggi == mm) {
            var append = true;

            if (Prodotto == "hotel") {
                //for parco + hotel the same day also
                if (i >= noggi && i <= lungmese) { append = false; }
            }
            else {
                //acquajoss same day also
                if (i >= noggi && i <= lungmese) { append = false; }
            }

            if (append == false) {
                $("#" + box + riga + col).addClass("soprah");
                $("#" + box + riga + col).append(cell);
            }
            else {
                $("#" + box + riga + col).addClass("cellah");
                $("#" + box + riga + col).append(cellno);
            }


        }
        else {
            if (i <= lungmese) {
                $("#" + box + riga + col).addClass("soprah");
                $("#" + box + riga + col).append(cell);
            }
        }

    }

    //vuoti dopo:
    for (var i = col + 1; i < 8; i++) {
        $("#" + box + riga + i).addClass("cellah");
        $("#" + box + riga + i).attr('style', 'height: 59px');
    }

    if (riga == 4) {
        for (var i = 1; i < 8; i++) { $("#" + box + "_5" + i).addClass("cellah"); }
        for (var i = 1; i < 8; i++) { $("#" + box + "_6" + i).addClass("cellah"); }
    }

    if (riga == 5) {
        for (var i = 1; i < 8; i++) { $("#" + box + "_6" + i).addClass("cellah"); }

    }

}


function SetCalendarF(dd) {
    var data = ListaDataF[dd - 1];
    FamilylPrice = ListaFamilyPrice[dd - 1];
}



function SetCalendarHA(dd) {
    var data = ListaDataA[dd - 1];
    HotelPrice = ListaHotelPriceA[dd - 1];

    $("#dataArrivo").val(data);

    //two days default
    var dataN;

    if ((dd < ListaDataA.length)) {
        dataN = ListaDataA[dd];
    }
    else {
        var mm = parseInt(data.substr(3, 2)) + 1;

        if (mm == 13) {
            return;
        }

        if (mm < 10) {
            mm = "0" + mm;
        }

       
        //GetCalendarioHotel(data.substr(6, 4), mm, 'D');

        dataN = "01/" + mm + "/" + data.substr(6, 4);
    }


    $("#dataPartenza").val(dataN);

    $("#HotelCalArrive").css("visibility", "hidden");
    $("#ncamere").prop('disabled', false);

    if (ListaTimeEntranceA[dd] == 0) {
        var msg = "Si avvisa che il giorno: " + dataN + " il parco sarà chiuso.";
        //AlertShow(msg);
        AlertCenter(msg);
        return;
    }
    else {
        //AlertClose();
    }


    $("#HotelCalArrive").css("visibility", "hidden");
    Arrivo();
}

function SetCalendarHD(dd) {
    var data = ListaDataD[dd - 1];
    $("#dataPartenza").val(data);
    $("#HotelCalDeparture").css("visibility", "hidden");
    Partenza();
}




function SetCalendarHB(dd) {
    var dataCustomer = ListaDataF[dd - 1];
    //FamilyPrice = ListaFamilyPrice[dd - 1];

    $("#continue").show();

    //if the event has a range of dates the date choosed by calendar must be in the range 
    if (EventDateStart != "" && EventDateEnd != "") {
        //convert dd/mm/yyyy to Jsdates
        var DateStart = ConvertDate(EventDateStart);
        var DateEnd = ConvertDate(EventDateEnd);
        var dataC = ConvertDate(dataCustomer);

        if (dataC < DateStart) {
            AlertCenter("La data è prima del periodo dell'offerta");
            return;
        }


        if (dataC > DateEnd) {
            AlertCenter("La data è oltre il periodo dell'offerta");
            return;
        }

        AlertCenterClose();
    }


    $("#datafinale").val(dataCustomer);
    $("#FamilyCal").css("visibility", "hidden");

    //TotPrice = FamilyPrice;
    TotPrice = "0,00";

    $("#TotPrice").text(FormatMoney(TotPrice) + "€");
    Day = dataCustomer;
  
    GetDisCode(Day);
}



function SetCalendarHF(dd) {
    var dataCustomer = ListaDataF[dd - 1];
    FamilyPrice = ListaFamilyPrice[dd - 1];

    $("#continue").show();

    //if the event has a range of dates the date choosed by calendar must be in the range 
    if (EventDateStart != "" && EventDateEnd != "")
    {
        //convert dd/mm/yyyy to Jsdates
        var DateStart = ConvertDate(EventDateStart);
        var DateEnd = ConvertDate(EventDateEnd);
        var dataC = ConvertDate(dataCustomer);

        if (dataC < DateStart) {
            AlertCenter("La data è prima del periodo dell'offerta");
            return;
        }


        if (dataC > DateEnd)
        {
            AlertCenter("La data è oltre il periodo dell'offerta");
            return;
        }

        AlertCenterClose();           
    }
   
    TotPrice = "0,00";
    $("#datafinale").val(dataCustomer);

    //acquajoss same day also
    if (ConvertDate(Day) >= ConvertDate(DataOggi))
    {
        TotPrice = FamilyPrice;
    } 
    $("#TotPrice").text(FormatMoney(TotPrice) + "€");
    Day = dataCustomer;
}



////////////////////////////////////////////
//color schedule
var Coln = "Coln_";
var StartH = "StartH_";
var EndH = "EndH_";

var StartCol = '<div><i class="fa fa-circle cell" aria-hidden="true" style="color:#8b8989;"></i> <span class="cell">PARCO CHIUSO</span></div>';
var EndCol = '';
//var EndCol = '<div><i class="fa fa-circle cell" aria-hidden="true" style="color:#d51f3b;"></i> <span class="cell">ANIMAL EXPLORER</span></div>';
var Colsch = '<div><i class="fa fa-circle cell" aria-hidden="true" style="color:' + Coln + ' ;"></i><div class="cell"><span>' + StartH + '</span> <span>' + EndH + '</span></div></div>';


function FillColorSchedule(Colori, ColorDes) {
    var htm = StartCol;

    for (var i = 0; i < Colori.length; i++) {
        var dat = Colsch.replace("Coln_", Colori[i]);

        var n = ColorDes[i].indexOf("-");
        var ini = ColorDes[i].substr(0, n - 1);
        var fin = ColorDes[i].substr(n + 2);

        dat = dat.replace("StartH_", ini);
        dat = dat.replace("EndH_", fin);

        htm = htm + dat;
    }

    htm = htm + EndCol;

    $("#colorschedule").append(htm);

}


function PrevMonthHF(){
    var data = ListaDataF[0];
    var mm = parseInt(data.substr(3, 2)) - 1;
    var aa = data.substr(6, 4);

    //non deve andare al mese prima di oggi
    var moggi = parseInt(DataOggi.substr(3, 2));
    var aoggi = parseInt(DataOggi.substr(6, 4));

    if (mm < moggi && aa == aoggi) {
        return;
        }

    if (mm == 0) {
    //da gennaio a dicembre anno precedente
        mm = 12;
    aa = Number(aa) -1;
    }

    GetCalendarioHotel(aa, mm, 'F');
}

function NextMonthHF() {
    var data = ListaDataF[0];
    var mm = parseInt(data.substr(3, 2)) + 1;
    var aa = data.substr(6, 4);

    if (mm == 13) {
        //next year
        aa = Number(aa) + 1;
        mm = 1;
    }

    GetCalendarioHotel(aa, mm, 'F');
}



function PrevMonthHB() {
    var data = ListaDataF[0];
    var mm = parseInt(data.substr(3, 2)) - 1;
    var aa = data.substr(6, 4);

    //non deve andare al mese prima di oggi
    var moggi = parseInt(DataOggi.substr(3, 2));
    var aoggi = parseInt(DataOggi.substr(6, 4));

    if(mm < moggi && aa == aoggi) {
        return;
    }

    //date start present
    if (EventDateStart != "") {
        var mmstart= parseInt(EventDateStart.substr(3, 2)) - 1;
        var aastart = parseInt(EventDateStart.substr(6, 4));

        if (mm <= mmstart && aa == aastart) {
            //first month reached
            return;
        }
    }

    if(mm == 0) {
        //da gennaio a dicembre anno precedente
        mm = 12;
        aa = Number(aa) -1;
    }

    GetCalendarioH(aa, mm, "B");
}


function NextMonthHB() {
    var data = ListaDataF[0];
    var mm = parseInt(data.substr(3, 2)) + 1;
    var aa = data.substr(6, 4);

    //date end present
    if (EventDateEnd != "") {
        var mmend = parseInt(EventDateEnd.substr(3, 2)) + 1;
        var aaend = parseInt(EventDateEnd.substr(6, 4));

        if (mm >= mmend && aa == aaend)
        {
            //last month reached
            return;
        }      
    }

    if (mm == 13) {
        //next year
        aa = Number(aa) + 1;
        mm = 1;
    }

    GetCalendarioH(aa, mm, "B");
}


function PrevMonthHA() {
    var data = ListaDataA[0];
    var mm = parseInt(data.substr(3, 2)) - 1;
    var aa = data.substr(6, 4);

    //non deve andare al mese prima di oggi
    var moggi = parseInt(DataOggi.substr(3, 2));
    var aoggi = parseInt(DataOggi.substr(6, 4));

    if (mm < moggi && aa == aoggi) {
        return;
    }

    if (mm == 0) {
        //da gennaio a dicembre anno precedente
        mm = 12;
        aa = Number(aa) - 1;
    }

    GetCalendarioHotel(aa, mm, 'A');
}


function NextMonthHA() {
    var data = ListaDataA[0];
    var mm = parseInt(data.substr(3, 2)) + 1;
    var aa = data.substr(6, 4);

    if (mm == 13) {
        //next year
        aa = Number(aa) + 1;
        mm = 1;
    }

   GetCalendarioHotel(aa, mm, 'A');
}


function PrevMonthHD() {
    var data = ListaDataD[0];
    var mm = parseInt(data.substr(3, 2)) - 1;
    var aa = data.substr(6, 4);


    //non deve andare al mese prima di oggi
    var moggi = parseInt(DataOggi.substr(3, 2));
    var aoggi = parseInt(DataOggi.substr(6, 4));

    if (mm < moggi && aa == aoggi) {
        return;
    }

    if (mm == 0) {
        //da gennaio a dicembre anno precedente
        mm = 12;
        aa = Number(aa) - 1;
    }

    GetCalendarioHotel(aa, mm, 'D');
}

function NextMonthHD() {
    var data = ListaDataD[0];
    var mm = parseInt(data.substr(3, 2)) + 1;
    var aa = data.substr(6, 4);

    if (mm == 13) {
        //next year
        aa = Number(aa) + 1;
        mm = 1;
    }

    GetCalendarioHotel(aa, mm, 'D');
}
