﻿
//resize
var lastBp = null;
(function ($, viewport) {
    $(window).on('load resize', viewport.changed(function () {
        if (lastBp !== viewport.current()) {
            $('.make_static').trigger('detach.ScrollToFixed');
            if (viewport.is('<sm')) {
                sliders(true);
                calendari(true);
                Mobilecarousel(true);
            } else {
                sliders(false);
                calendari(false);
                Mobilecarousel(false);

                $('.make_static').scrollToFixed({
                    marginTop: $('.navbar').length ? ($('.navbar').height() + 10) : 10,
                    removeOffsets: true,
                    limit: function () {
                        return (($('#Buyer').length ? $('#Buyer').offset().top : $("footer").offset().top) - $('.make_static').outerHeight(true)) -10;
                    },
                    zIndex: 1,
                });
            }
        }
        lastBp = viewport.current();
    }, 0.1));
})(jQuery, ResponsiveBootstrapToolkit);

//slider home
var sliderOri = $("#slider").clone();
$("#slider").hide();
sliders = function (xs) {
    var slider = sliderOri.clone();
    if (sliderOri.find("a").length > 1) {
        var oldSlider = $('#slider');
        oldSlider.after(slider);
        oldSlider.remove();
        slider = $("#slider");
        var optionsSlider = {
            effect: 'fade',
            animSpeed: 700,
            pauseTime: 7000,
            startSlide: 0,
            directionNav: !0,
            controlNav: 1,
            controlNavThumbs: !1,
            pauseOnHover: !0,
            manualAdvance: !1,
            prevText: '<i class="fa fa-chevron-circle-left fa-4x" aria-hidden="true"></i>',
            nextText: '<i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true"></i>'

        };
        if (slider.length) {
            if (xs) {
                var smalls = slider.find('[xs="True"]');
                if (smalls.length) {
                    slider.find('[xs="False"]').remove();
                    optionsSlider.prevText = '<i class="fa fa-chevron-circle-left fa-2x" aria-hidden="true"></i>';
                    optionsSlider.nextText = '<i class="fa fa-chevron-circle-right fa-2x" aria-hidden="true"></i>'
                }
            } else {
                slider.find('[xs="True"]').remove()

            }
            slider.nivoSlider(optionsSlider);
            if (slider.find(".nivo-imageLink").length == 1) {
                $('a.nivo-nextNav, a.nivo-prevNav').hide();
            }

            if ("ontouchstart" in document.documentElement) {
                $('a.nivo-nextNav, a.nivo-prevNav').hide();
                slider = document.getElementById("slider");
                Hammer(slider).on("swipeleft", function (event) {
                    $('a.nivo-nextNav').trigger('click');
                    return !1
                });
                Hammer(slider).on("swiperight", function (event) {
                    $('a.nivo-prevNav').trigger('click');
                    return !1
                })
            }
            $(slider).find('.hide').removeClass('hide');
            $(slider).hide().fadeIn('slow');

        }
    } else {
        $('#slider').find('.hide').removeClass('hide');
        $('#slider').removeClass("nivoSlider").hide().fadeIn('slow')

    }
};


$(document).ready(function () {

    $("[data-fancybox]").fancybox({
        // Options will go here
    });

    //menu scroll
    $(function () {
        var shrinkHeader = 10;
        $(window).scroll(function () {
            var scroll = getCurrentScroll();
            if (scroll >= shrinkHeader) {
                $('header').addClass('shrink');
             
            } else {
                $('header').removeClass('shrink');
               
            }
        });
        function getCurrentScroll() {
            return window.pageYOffset || document.documentElement.scrollTop;
        }
    });
    //account
$(function () {
    $(".dropdownCount").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
                $('i', this).toggleClass("fa-chevron-down fa-chevron-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
                $('i', this).toggleClass("fa-chevron-down fa-chevron-up");                
            });
    });

    //carousel home il programmas
$(".center").on('init', function (event, slick) {
    $(event.currentTarget).show();
}).slick({
       
        infinite: true,
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 3,
        slidesToScroll: 3,
        autoplay: false,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows:false,
                    centerMode: true,
                    centerPadding: '20px',
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 620,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '10px',
                    slidesToShow: 1,
                    dots: false,
                    autoplay: true,
                }
            }
        ]
});
   
    $("#accordion").on("show.bs.collapse", function (e) {
        console.log(e);
        console.log($(e.currentTarget).find('.panel-collapse.in'));
        $(e.currentTarget).attr("target", $(e.target).attr("id"));
        if ($(e.target).find('.slick-track:first-child').width() == 0) {
            $(e.target).find('.slick-slider').hide();

        }
    }).on("shown.bs.collapse", function (e) {
        if ($(e.target).find('.slick-track:first-child').width() == 0) {
            $(e.target).find('.slick-slider').show().slick('setPosition');
        }
        //$(e.currentTarget).show();
    }).on("hide.bs.collapse", function (e) {
        if ($(e.currentTarget).attr("target") == $(e.target).attr("id")) {
            $(e.currentTarget).removeAttr("target");
        }
    });

    //interna ilparco
    $('.slider-for').on('init', function (event, slick) {
        $(event.currentTarget).show();
    }).slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: false,
        asNavFor: '.slider-nav',
        autoplay: false
    });
    $('.slider-nav').on('init', function (event, slick) {
        $(event.currentTarget).show();
    }).slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        autoplay: true
    });
});
//form home
$('.datepicker').datepicker({
    language: "it",
    forceParse: true,
    todayHighlight: true,
    todayBtn: "linked",
    keyboardNavigation: true,
    calendarWeeks: true,
    autoclose: true,
    toggleActive: false,
    orientation: "auto bottom",
});

//date color home
$(function () {
    $('.datetimepicker').datetimepicker();

    var schedules = [
        {
            ParkSchedule_Id: 1,
            ParkSchedule_TimeStart: '10:00 AM',
            ParkSchedule_TimeEnd: '05:00 PM',
            ParkSchedule_TimeColor: '#ffbd00',
            ParkSchedule_Date: '01/02/2018'
        },
        {
            ParkSchedule_Id: 2,
            ParkSchedule_TimeStart: '10:00 AM',
            ParkSchedule_TimeEnd: '06:00 PM',
            ParkSchedule_TimeColor: '#1da423',
            ParkSchedule_Date: '07/02/2018'
        },
        {
            ParkSchedule_Id: 3,
            ParkSchedule_TimeStart: '10:00 AM',
            ParkSchedule_TimeEnd: '08:00 PM',
            ParkSchedule_TimeColor: '#5b15bb',
            ParkSchedule_Date: '12/02/2018'
        }
    ];
    
    moment.locale("it");
    $.fn.datetimepicker.defaults.locale = "it";
    $.fn.datetimepicker.defaults.format = "L";

    var format = moment().localeData().longDateFormat($.fn.datetimepicker.defaults.format);
    var $dateTimePicker = $('#datetimepicker');
    var $dtp = $dateTimePicker.datetimepicker({
        inline: true,
        //enabledDates: schedules.map(s => moment(s.ParkSchedule_Date, format).toDate())
        enabledDates: schedules.map(function () {
            return moment(this.ParkSchedule_Date, format).toDate()
        })
    }).on("dp.show", getSchedulesByDate).on("dp.update", getSchedulesByDate).on("dp.change", function (e) {
   
    });

    calendari = function (xs) {
        if ($dateTimePicker.hasClass("calendariEvent")) {
            var weekdays = moment.localeData()._weekdays;
            var weekdaysMin = moment.localeData()._weekdaysMin;
            if (xs) {
            
            } else {
                $dtp.find(".datepicker-days thead tr:last-child th").each(function () {
                    $(this).text(weekdays[$.inArray($(this).text(), weekdaysMin)]);
                });

            }
       
        }

    }
    
    function getSchedulesByDate(e) {
        var i = 0;
        var $dtp = $(e.currentTarget).find(".bootstrap-datetimepicker-widget");
        $dtp.find(".active").removeClass("active today");
        $dtp.find('[data-action="selectDay"]').each(function () {
            var date = $(this).attr('data-day');
            //var schedule = schedules.find(s => s.ParkSchedule_Date == date);
            var schedule = schedules.find(function (scheduleDate) {
                console.log(scheduleDate, date);
                return scheduleDate.ParkSchedule_Date == date ? scheduleDate.ParkSchedule_Date : null
                
            })
            if (schedule) {
                $(this).append('<div style="background: ' + schedule.ParkSchedule_TimeColor + '"> E' + i++ + '</div>');
            }
        });
    }

    var i = 0;
    $dtp.find(".active").removeClass("active today");
    $dtp.find('[data-action="selectDay"]').each(function () {
        var date = $(this).attr('data-day');
        //var schedule = schedules.find(s => s.ParkSchedule_Date == date);
        var schedule = schedules.find(function (scheduleDate) {
            return scheduleDate.ParkSchedule_Date == date ? scheduleDate.ParkSchedule_Date : null
        });
        if (schedule) {
            $(this).append('<div style="background: ' + schedule.ParkSchedule_TimeColor + '"> E' + i++ + '</div>');
        }
    });

});

//map
$(function () {
    $("#btn-start").click(function () {
        $("iframe[src*='google.com/maps']").scrollprevent({
            printLog: true
        }).start();
        return $("#btn-stop").click(function () {
            return $("iframe[src*='google.com/maps']").scrollprevent().stop();
        });
    });
    return $("#btn-start").trigger("click");
});

//il parco activitys mix
if ($('#container').length)    
{
    var mixer = mixitup('#container', {
        load: {
            filter: '.category-1'
        }

    });
  
}

//contatti form
//$('#FormContatti').validator().on('submit', function (e) {
//    if (e.isDefaultPrevented()) {
//        // handle the invalid form...
//    } else {
//        // everything looks good!
//        e.preventDefault();
//        $("#FormContatti")[0].reset();
//        $(".exitomensaje").show();
       
//    }

//})

//$(document).ready(function ()
//{
//    if ($.serializeJSON) {
//        $("#FormContatti").on("submit", function (e)
//        {
//            //console.log("entracontact");
//            e.preventDefault();
//            //console.log(e);
//            var $form = $(this);
//            var $btn = $form.find(":submit").button("loading");
//            if ($form.valid()) {
//                var data = $form.serializeJSON();
//                var $msn = $form.find("#alert");
//                if ($msn.length) $msn.removeAttr("class").addClass("alert");
//                //console.log(data);

//                $.post("/home/SendRequest/", data, function (response)
//                {
//                    console.log(response);
//                    $("#FormContatti")[0].reset();
//                    if ($msn.length) $msn.addClass("alert-" + response.type).html("<b>" + response.type.toUpperCase() + "!</b> " + $form.attr("user_" + response.status)).show().delay(3000).slideUp('fast');
//                }, "JSON").fail(function ()
//                {
//                    if ($msn.length) $msn.addClass("alert-danger").html("<b>ERROR!</b> " + $form.attr("user_error")).show().delay(3000).slideUp('fast');
//                }).always(function ()
//                {
//                    $("#FormContatti")[0].reset();
//                    $btn.button("reset");
//                });
//            }
//        });
//    }
//    else {
//        console.log("script serializeJSON is required");
//    }
//});


//scroll gruppi
function scrollNav() {
    $('.paqueteScuole a').click(function () {
        //Toggle Class
        //$(".active").removeClass("active");
        //$(this).closest('li').addClass("active");
        //var theClass = $(this).attr("class");
        //$('.' + theClass).parent('li').addClass('active');
        //Animate
        $('html, body').stop().animate({
            scrollTop: $($(this).attr('href')).offset().top - 40
        }, 400);
        return false;
    });
    //$('.scrollTop a').scrollTop();
}
scrollNav();


//section chisiamo
$('.mobileDesp').readmore({
    // Be sure to use the same speed value in your CSS transition property.
    speed: 500,

    // Add the class 'transitioning' before toggling begins.
    beforeToggle: function (trigger, element, expanded) {
        element.addClass('transitioning');

    },

    // Remove the 'transitioning' class when toggling completes.
    afterToggle: function (trigger, element, expanded) {
        element.removeClass('transitioning');
    }
});

//slider  chisiamo
$(".logosChi").on('init', function (e, slick) {
        $(e.currentTarget).show();
    }).slick({
        adaptiveHeight: true,
    });

//slider  eduscienza
$(".slideOneEdu").on('init', function (e, slick) {
    $(e.currentTarget).show();
}).slick({
    slidesToShow: 3,
    autoplay: true,
    responsive: [
          {
              breakpoint: 768,
              settings: {
                  arrows: false,
                  centerMode: true,
                  centerPadding: '10px',
                  slidesToShow: 1,
                  dots: true,
              }
          },
          {
              breakpoint: 480,
              settings: {
                  arrows: false,
                  centerMode: true,
                  centerPadding: '10px',
                  slidesToShow: 1,
                  dots: false,
              }
          }
    ]
});

    //video home
$('#foto').on('click', function () {
        $("#video_foto").hide();
        var replaceSrc = $('#iframeYoutube').attr('src') + '&autoplay=1';
        $('#iframeYoutube').attr('src', replaceSrc);
        $('.boxIframe').show();
});

//vip

    $(".galleryVip").on('init', function (e, slick) {
        $(e.currentTarget).show();
    }).slick({
        lazyLoad: 'ondemand', // ondemand progressive anticipated
        infinite: true,
        adaptiveHeight: false,
        autoplay: true,
        centerPadding: '40px',
        slidesToScroll: 1,
        rows: true
    });



//vip carousel
Mobilecarousel = function (xs) {
    if (typeof xs === "undefined") xs = (lastBp == "xs");
    $.fn.unslick = function () {
        var _ = this;
        return _.each(function (index, element) {
            if (element.slick) {
                element.slick.destroy();
            }
        });
    };
    
    if (xs) {
        if ($(".CarouselVip").length && $(".CarouselVip").get(0).childElementCount > 0) {
            $(".CarouselVip").on('init', function (e, slick) {
                $(e.currentTarget).show();
            }).slick({
                lazyLoad: 'ondemand', // ondemand progressive anticipated
                infinite: true,
                adaptiveHeight: false,
                autoplay: true,
                centerPadding: '30px',
                rows: false,
                slidesToShow: 3,
            });
        }

        if ($(".single-item-rtl").length && $(".single-item-rtl").get(0).childElementCount > 1) {
            $(".single-item-rtl").on('init', function (e, slick)
            {
                //console.log("INIT");
                $(e.currentTarget).show();
            }).slick({              
                rtl: true
               
            });
        }
        if ($(".sliderShop").length && $(".sliderShop").get(0).childElementCount > 1) {
            $(".sliderShop").on('init', function (e, slick)
            {
                //console.log("INIT");
                $(e.currentTarget).show();
            }).slick({
                autoplay: false,
                dots: true,

            });
        }
        $(".subTextShop").shorten({
            moreText: 'read more',
            lessText: 'read less',
            showChars: 50,
        });


        //calendari activities
        $(".listado-actividades").on('init', function (e, slick) {
            $(e.currentTarget).show();
        }).slick({
            lazyLoad: 'ondemand', // ondemand progressive anticipated
            infinite: true,
            adaptiveHeight: true,
            autoplay: true,
            centerPadding: '40px',
            dots: true,
            slidesToScroll: 1,
        });

    } else {
        $(".CarouselVip").unslick();
        $(".listado-actividades").unslick();
        $(".sliderShop").unslick();
    }
}


//tripavisor
$(".sliderTrip").on('init', function (e, slick) {
    $(e.currentTarget).show();
}).slick({
    lazyLoad: 'ondemand', // ondemand progressive anticipated
    infinite: true,
    adaptiveHeight: false,
    autoplay: true,
    rows: false,
    slidesToShow: 1,
});

//restaurant
$(".restaurants").on('init', function (e, slick) {
    $(e.currentTarget).show();
}).slick({
    lazyLoad: 'ondemand', // ondemand progressive anticipated
    infinite: true,
    adaptiveHeight: true,
    autoplay: true,
    centerPadding: '40px',
    dots: true,
    slidesToScroll: 1,
});
//restaurant
$(".eventislider").on('init', function (e, slick) {
    $(e.currentTarget).show();
}).slick({
    lazyLoad: 'ondemand', // ondemand progressive anticipated
    infinite: true,
    adaptiveHeight: true,
    autoplay: true,
    centerPadding: '40px',
    dots: true,
    slidesToScroll: 1,
});

$(".thumbsVip").on("click", function (e) {
    e.preventDefault();
    var img = $(this);
    var url = img.data("largeimg");
    var urlMobile = img.data("largemobile");
    var galeria = $(".galleryVip");

    galeria.find(".hidden-xs").attr("src", url);
    galeria.find(".visible-xs").attr("src", urlMobile);

});
$(".itemVip").on("click", function (e) {
    e.preventDefault();
    var text = $(this).find("span").text();
    console.log(text);
    $(".galleryVip").find(".slide__caption").text(text)

})
 
//eventi list grid
//if ($('#instgram-eventi').length) {
//    var masonry = new Macy({
//        container: '#instgram-eventi',
//        trueOrder: false,
//        waitForImages: false,
//        useOwnImageLoader: false,
//        debug: true,
//        mobileFirst: true,
//        columns: 1,
//        margin: 25,
//        breakAt: {
//            1200: 3,
//            940: 3,
//            768: 2,
//            700: 1,
//            400: 1
//        }
//    });
//}


//par el loading de shopping cart
$('#ajaxBusy').css({
    position: "fixed",
    top: "0px",
    left: "0px",
    height: "100%",
    width: "100%",
    backgroundColor: "rgba(255, 255, 255, 0.901961)",
    textAlign: "center",
    zIndex: "99"
});

$('#ajaxBusy img').css({
    paddingTop: "25%",
    width: "100px",
    marginBottom: "10px"
});



