﻿var Prodotto = null;
var FamilyTitle = '';
var FamilyDescription = '';
var FamilyNote = '';
var FamilyImage = '';
var DiscountImage = '';
var DiscountCode = '';
var DiscountTitle = '';

///////////////////////////////////////////
//SHOPPING CART TICKET
var SCphrow = [], SCphdes = [], SCphprc = [], SCphqnt = [], SCphtot = [], SCphdel = [];
//var dist = "140px;"
var dist = "0px;"
var lft = "20px;"
var maxcombo = 15;

////////////////////////////////////////////
//dynamic ticket section
SCphrow[0] = "SCTckRiga_"
SCphdes[0] = "SCTicketDes_";
SCphprc[0] = "SCTicketPrice_";
SCphqnt[0] = "SCTicketQuantity_";
SCphtot[0] = "SCTicketTotal_";

var SCTckSec = '<div id="' + SCphrow[0] + '" style="color:Gray;">';
SCTckSec += '<div  class="row" >';
SCTckSec += '<div class="col-sm-5 col-xs-9" style="left:' + lft + '"><h5 style="display: inline;">Ingresso:&nbsp;</h5><label id="' + SCphdes[0] + '"></label></div>';
SCTckSec += '<div class="col-sm-2 col-xs-4" style="text-align: left; padding-right: ' + dist + '"><small class="visible-xs">Prezzo</small><h5 style="display: inline;">€&nbsp;</h5><label id="' + SCphprc[0] + '"></label></div>';
SCTckSec += '<div class="col-sm-2 col-xs-4"><small class="visible-xs">Quant.</small><h5 style="display: inline;">&nbsp;</h5><label id="' + SCphqnt[0] + '"></label></div>';
SCTckSec += '<div class="col-sm-2 col-xs-3" style="text-align: left; padding-right: ' + dist + '"><small class="visible-xs">Totale</small><h5 style="display: inline;">€&nbsp;</h5><label id="' + SCphtot[0] + '"></label></div>';
SCTckSec += '<div class="col-sm-1 col-xs-2"></div>';
SCTckSec += '</div>';
SCTckSec += '</div>';

///////////////////////////////////////////
//family pack
var FamSec = "<div style='color:Gray;'>";
FamSec += '<div  class="row" >';
FamSec += '<div class="col-md-9" style="left:' + lft + '"><label id="' + SCphdes[0] + '"></label></div>';
FamSec += '<div class="col-md-3" style="text-align: left; padding-right: ' + dist + '"><small class="visible-xs">Prezzo</small><h5 style="display: inline;">€&nbsp;</h5><label id="' + SCphtot[0] + '"></label></div>';
FamSec += "</div></div>";


//////////////////////////////////////////
//dynamic animal section
SCphrow[1] = "SCAnimalRiga_"
SCphdes[1] = "SCAnimalDes_";
SCphprc[1] = "SCAnimalPrice_";
SCphqnt[1] = "SCAnimalQuantity_";
SCphtot[1] = "SCAnimalTotal_";
SCphdel[1] = "SCAnimalDelRiga_";

SCphdes[4] = "SCSubAnimalId_";


var SCAniSec = '<div id="' + SCphrow[1] + '" style="color:Gray;">'
SCAniSec += '<div class="col-sm-5 col-xs-9" style="left: 2px;"><h5 style="display: inline;">Attività con animali:&nbsp;</h5><label id="' + SCphdes[1] + '"></label></div>';
SCAniSec += '<div class="col-sm-2 col-xs-4" style="text-align: left; padding-right: ' + dist + '"><small class="visible-xs">Prezzo</small><h5 style="display: inline;">€&nbsp;</h5><label id="' + SCphprc[1] + '"></label></div>';
SCAniSec += '<div class="col-sm-2 col-xs-3"><small class="visible-xs">Quant.</small><h5 style="display: inline;">&nbsp;</h5><label id="' + SCphqnt[1] + '"></label></div>';
SCAniSec += '<div class="col-sm-2 col-xs-3" style="text-align: left; padding-right: ' + dist + '"><small class="visible-xs">Totale</small><h5 style="display: inline;">€&nbsp;</h5><label id="' + SCphtot[1] + '"></label></div>';
SCAniSec += '<div class="col-sm-1 col-xs-2" style="text-align: center;">';
SCAniSec += '<span class="glyphicon glyphicon-remove" style="color: darkred; cursor: pointer;" id= "' + SCphdel[1] + '" onclick ="DelAniQuantity(DelAniLineId)"></span>';
SCAniSec += '</div>';
SCAniSec += '</div>';
SCAniSec += '<div id="' + SCphdes[4] + '" style="clear:both;"></div>';

////////////////////////////////////////////
//dynamic sub animal section
SCphrow[3] = "SCSubAnimalRiga_"
SCphdes[3] = "SCSubAnimalDes_";
SCphprc[3] = "SCSubAnimalPrice_";
SCphqnt[3] = "SCSubAnimalQuantity_";
//SCphtot[3] = "SCSubAnimalTotal_";

var SubSCAniSec = '<div id="' + SCphrow[3] + '" class="row SCitem" style="color:Gray;">'
SubSCAniSec += '<div class="col-md-6" style="left:' + lft + '"><h5 style="display: inline;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Orario ingresso:&nbsp;</h5><label id="' + SCphdes[3] + '"></label></div>';
SubSCAniSec += '<div class="col-md-6" style="text-align: left; padding-right: ' + dist + '"></div>';
SubSCAniSec += '</div>';


////////////////////////////////////////////
//dynamic activity section
SCphrow[5] = "SCActivityRiga_"
SCphdes[5] = "SCActivityDes_";
SCphprc[5] = "SCActivityPrice_";
SCphqnt[5] = "SCActivityQuantity_";
SCphtot[5] = "SCActivityTotal_";
SCphdel[5] = "SCActivityDelRiga_";

var SCActSec = '<div id="' + SCphrow[5] + '" style="color:Gray;">'
SCActSec += '<div class="row SCitem">';
SCActSec += '<div class="col-sm-5 col-xs-9" style="left:' + lft + '"><h5 style="display: inline;">Attrazioni extra:&nbsp;</h5><label id="' + SCphdes[5] + '"></label></div>';
SCActSec += '<div class="col-sm-2 col-xs-4" style="text-align: left; padding-right: ' + dist + '"><small class="visible-xs">Prezzo</small><h5 style="display: inline;">€&nbsp;</h5><label id="' + SCphprc[5] + '"></label></div>';
SCActSec += '<div class="col-sm-2 col-xs-3"><small class="visible-xs">Quant.</small><h5 style="display: inline;">&nbsp;</h5><label id="' + SCphqnt[5] + '"></label></div>';
SCActSec += '<div class="col-sm-2 col-xs-3" style="text-align: left; padding-right: ' + dist + '"><small class="visible-xs">Totale</small><h5 style="display: inline;">€&nbsp;</h5><label id="' + SCphtot[5] + '"></label></div>';
SCActSec += '<div class="col-sm-1 col-xs-2">';
SCActSec += '<span class="glyphicon glyphicon-remove" style="color: darkred; cursor: pointer;" id= "' + SCphdel[5] + '" onclick ="DelActQuantity(DelActLineId)"></span>';
SCActSec += '</div>';
SCActSec += '</div>';
////////////////////////////////////////////
//dynamic menu section
SCphrow[6] = "SCMenuRiga_"
SCphdes[6] = "SCMenuDes_";
SCphprc[6] = "SCMenuPrice_";
SCphqnt[6] = "SCMenuQuantity_";
SCphtot[6] = "SCMenuTotal_";
SCphdel[6] = "SCMenuDelRiga_";

var SCMnuSec = '<div id="' + SCphrow[6] + '" style="color:Gray;">'
SCMnuSec += '<div class="row">';
SCMnuSec += '<div class="col-sm-5 col-xs-9" style="left:' + lft + '"><h5 style="display: inline;">Menu:&nbsp;</h5><label id="' + SCphdes[6] + '"></label></div>';
SCMnuSec += '<div class="col-sm-2 col-xs-4" style="text-align: left; padding-right: ' + dist + '"><small class="visible-xs">Prezzo</small><h5 style="display: inline;">€&nbsp;</h5><label id="' + SCphprc[6] + '"></label></div>';
SCMnuSec += '<div class="col-sm-2 col-xs-3"><small class="visible-xs">Quant.</small><h5 style="display: inline;">&nbsp;</h5><label id="' + SCphqnt[6] + '"></label></div>';
SCMnuSec += '<div class="col-sm-2 col-xs-3" style="text-align: left; padding-right: ' + dist + '"><small class="visible-xs">Totale</small><h5 style="display: inline;">€&nbsp;</h5><label id="' + SCphtot[6] + '"></label></div>';
SCMnuSec += '<div class="col-sm-1 col-xs-2">';
SCMnuSec += '<span class="glyphicon glyphicon-remove removelistX" style="color: darkred; cursor: pointer;" id= "' + SCphdel[6] + '" onclick ="DelMnuQuantity(DelMnuLineId)"></span>';
SCMnuSec += '</div > ';
SCMnuSec += '</div>';
////////////////////////////////////////////


////////////////////////////////////////////
//shopping cart TICKET PLUS HOTEL
var DayArrive;
var DayDeparture;
var AssicCambioData;
var ServizioNavetta;
var HotChangeDate;

var TotPrice = 0;
var TotHotelPrice = 0;

var HotelId;
var HotelPrice;
var DynBasePrice;
var HotelPriceId;
var AssicPrice;
var NavPrice;
var ExtraNight1 = 0;
var NextHotels;
var ExtraOpen;

//var hotDes; non la vogliono piu
var Nrooms;
var adults, kids, kidsfree;

var HotelSec = '';

HotelSec += '<div class="col-md-3 style="word-wrap: break-word;"><h5><span id="hotdes" style="color: #003851;"></span></h5></div>';
HotelSec += '<div class="col-md-3" style="padding:0;"><h5><span id="hotperiodo" style="color: #003851;"></span></h5></div>';
HotelSec += '<div class="col-md-2"><h5>Persone: <span id="Npag" style="color: #003851;"></span></h5></div>';
HotelSec += '<div class="col-md-2"><h5>Notti: <span id="Nnotti" style="word-wrap: break-word; color: #003851;"></span></h5></div>';
HotelSec += '<div class="col-md-2"><h5>Totale: <span id="Ntot" style="color: #00B240;"></span></h5></div>';


HotelSec += ' <div class="col-md-12">';
HotelSec += '<div class="col-md-12"><h5 id="Nnavetta" style="margin:0;"></h5></div>';




HotelSec += '<div class="col-md-12"><h5 id="Nassic" style="margin-top:-3px; margin-left: -14px;"></h5></div>';

HotelSec += '</div>';



////////////////
var cam = "Camera_";
var ncam = "NumCam_";
var nadu = "Nadults_";
var nkid = "Nkids_";
var nkidf = "Nkidf_";

var Roomsec = '<div id="' + cam + '" class="row">';
Roomsec += '<div class="col-md-2"><h5 style="display: inline;">&nbsp;Camera n. </h5><label id="' + ncam + '"></label></div>';
Roomsec += '<div class="col-md-2"><h5 style="display: inline;">Adulti: </h5><label id="' + nadu + '"></label></div>';
Roomsec += '<div class="col-md-3"><h5 style="display: inline;">Bambini da  1 a  1,3 mt: </h5><label id="' + nkid + '"></label></div>';
Roomsec += '<div class="col-md-3"><h5 style="display: inline;">Bambini sotto 1 mt: </h5><label id="' + nkidf + '"></label></div>';
Roomsec += '</div>';
////////////////////////////////////////////

////////////////////////////////////////////
//shopping cart SUBSCRIPTIONS (SEASONAL TICKETS)
var nsub;
var SubscriptionId;
var Description;
var Quantity;
var TckPrices;
var Names;
var Surnames;
var TotNames = 0;

/////////////////////////////////////////
var SCphabb = "SubNumber_";
var SCphdsc = "SubDescription_";
var SCphqua = "SubQuantity_";
var SCphprz = "SubPrice_";
var SCphtol = "SubTotPrice_";
var SCphsct = "SectionNames_";

var SubSec = '<div id="' + SCphabb + '" class="row">';
SubSec += '<div class="col-md-5"><h5 style="display: inline;">&nbsp;</h5><label id="' + SCphdsc + '"></label></div>';
SubSec += '<div class="col-md-2"><small class="visible-xs" style="float:left; margin-top: -2px;">Prezzo:&nbsp;</small><h5 style="display: inline;">&nbsp;</h5><label id="' + SCphprz + '"></label>&nbsp;€</div>';
SubSec += '<div class="col-md-2"><small class="visible-xs" style="float:left; margin-top: -2px;">N. abbonati:&nbsp;</small><h5 style="display: inline;">&nbsp;</h5><label id="' + SCphqua + '"></label></div>';
SubSec += '<div class="col-md-2"><small class="visible-xs" style="float:left; margin-top: -2px;">Totale:&nbsp;</small><h5 style="display: inline;">&nbsp;</h5><label id="' + SCphtol + '"></label>&nbsp;€</div>';
SubSec += '<div class="col-md-1"></div>';
SubSec += '</div>';
SubSec += '<div id="' + SCphsct + '"></div>';

var phnln = "SubscriberLine_";
var phnam = "SubscriberName_";
var phcog = "SubscriberSurname_";

var NamSec = '<div id="' + phnln + '" class="row">';
NamSec += '<div class="col-md-3"><h5 style="display: inline;">&nbsp;Nome:&nbsp;</h5><label id="' + phnam + '" style="font-weight: bold;"></label></div>';
NamSec += '<div class="col-md-4"><h5 style="display: inline;">&nbsp;Cognome:&nbsp;</h5><label id="' + phcog + '" style="font-weight: bold;"></label></div>';
NamSec += '</div>';
/////////////////////////////////////////

function ManageAcquistoId() {
    if (LogUser == 4) {
        //if operator logged no show telephone ass.
        $("#AssTel").css("visibility", "hidden");

        //only for operators
        $("#Contanti").css("display", "block");
        $("#Pos").css("display", "block");
    }



    if (VerificaCliente == "true") {
        $("#LogoutVer").css("display", "block");
    }


    if (AcquistoId != '') {

        //if (Prodotto == "ticket") {
        //    $("#buyhotel").css("display", "none");
        //    $("#buysubscription").css("display", "none");
        //    $("#Biglietto").attr("href", "");
        //}

        //if (Prodotto == "hotel") {
        //    $("#Biglietto").css("display", "none");
        //    $("#buysubscription").css("display", "none");
        //    $("#buyhotel").attr("href", "");
        //}

        //if (Prodotto == "abbonamento") {
        //    $("#Biglietto").css("display", "none");
        //    $("#buyhotel").css("display", "none");
        //    $("#buysubscription").attr("href", "");
        //}


        DeleteCookieBuyer();
        DeleteAllCookies();
    }
}

function CodeProdotto() {
    if (Prodotto == "ticket") {
        return 1;
    }

    if (Prodotto == "hotel") {
        return 2;
    }

    if (Prodotto == "abbonamento") {
        return 3;
    }

}


function DelMnuQuantity(i) {
    MnuQuantity[i] = 0;
    RiempiSC();
    TotPrezzo();
    SubTotPrezzo();
    $("#Mnu" + phqnt + i).val("0");
}

function DelAniQuantity(i) {


    AniQuantity[i] = 0;
    RiempiSC();
    TotPrezzo();
    SubTotPrezzo();
    $("#Ani" + phqnt + i).val("0");

    for (var j = 0; j < SubAniQuantity[i].length; j++) {
        SubAniQuantity[i][j] = 0;
        $("#SubAniQuantity_" + i + " option:first").attr('selected', 'selected');
    }

}

function DelActQuantity(i) {
    ActQuantity[i] = 0;
    RiempiSC();
    TotPrezzo();
    SubTotPrezzo();
    $("#Act" + phqnt + i).val("0");
}

function LoopCreationSC() {

    if (Prodotto == "ticket" || Prodotto == null) {
        var Section = SCTckSec;

        if (FamilyPack != "") {
            Titles[0] = FamilyTitle;

            Section = FamSec;
        }

        for (var i = 0; i < ticket.length; i++) {
            var htm = Sostituzione(Section, CreaListaSC(0), i);
            $("#SCticketlist").append(htm);
        }
    }


    //loop creation activities
    for (var i = 0; i < activity.length; i++) {

        if (ActPrices[i] != "0,00") {
            var htm = Sostituzione(SCActSec, CreaListaSC(5), i);
            htm = htm.replace("DelActLineId", i);
            $("#SCactivitylist").append(htm);
        }

    }

    //loop creation menus
    for (var i = 0; i < menu.length; i++) {
        var htm = Sostituzione(SCMnuSec, CreaListaSC(6), i);
        htm = htm.replace("DelMnuLineId", i);
        $("#SCmenulist").append(htm);
    }

    //loop animals and subanimals not here GetSubAnimalPricesSCop

}

function FillAllSC() {

    if (Prodotto == "ticket" || Prodotto == null) {

        for (var ind = 0; ind < ticket.length; ind++) {

            if (Prices[ind] == null) { Prices[ind] = "0,00"; }
            if (Quantity[ind] == null) { Quantity[ind] = 0; }


            $("#" + SCphqnt[0] + ind).text(Quantity[ind]);
            $("#" + SCphdes[0] + ind).text(Titles[ind]);
            $("#" + SCphprc[0] + ind).text(FormatMoney(Prices[ind]));
            var subtot = Number(Quantity[ind]) * Number(Prices[ind].replace(",", "."));

            $("#" + SCphtot[0] + ind).text(FormatMoney(subtot));
            if (Quantity[ind] == 0) {
                $("#" + SCphrow[0] + ind).hide();
            }
        }
    }

    for (var ind = 0; ind < activity.length; ind++) {
        $("#" + SCphqnt[5] + ind).text(ActQuantity[ind]);
        $("#" + SCphdes[5] + ind).text(ActTitles[ind]);
        $("#" + SCphprc[5] + ind).text(ActPrices[ind]);

        if (ActQuantity[ind] == null) { ActQuantity[ind] = 0; }
        if (ActPrices[ind] == null) { ActPrices[ind] = "0,00"; }
        var subtot = Number(ActQuantity[ind]) * Number(ActPrices[ind].replace(",", "."));

        $("#" + SCphtot[5] + ind).text(FormatMoney(subtot));
        if (Quantity[ind] == 0) {
            $("#" + SCphrow[0] + ind).hide();
        }

        if (ActQuantity[ind] == 0) {
            $("#" + SCphrow[5] + ind).hide();
        }
    }

    for (var ind = 0; ind < menu.length; ind++) {
        $("#" + SCphqnt[6] + ind).text(MnuQuantity[ind]);
        $("#" + SCphdes[6] + ind).text(MnuTitles[ind]);
        $("#" + SCphprc[6] + ind).text(MnuPrices[ind]);

        if (MnuQuantity[ind] == null) { MnuQuantity[ind] = 0; }
        if (MnuPrices[ind] == null) { MnuPrices[ind] = "0,00"; }
        var subtot = Number(MnuQuantity[ind]) * Number(MnuPrices[ind].replace(",", "."));

        $("#" + SCphtot[6] + ind).text(FormatMoney(subtot));
        if (Quantity[ind] == 0) {
            $("#" + SCphrow[0] + ind).hide();
        }

        if (MnuQuantity[ind] == 0) {
            $("#" + SCphrow[6] + ind).hide();
        }
    }



    for (var ind = 0; ind < animal.length; ind++) {
        $("#" + SCphqnt[1] + ind).text(AniQuantity[ind]);
        $("#" + SCphdes[1] + ind).text(AniTitles[ind]);
        $("#" + SCphprc[1] + ind).text(AniPrices[ind]);


        if (AniQuantity[ind] == null) { AniQuantity[ind] = 0; }
        if (AniPrices[ind] == null) { AniPrices[ind] = "0"; }

        var subtot = Number(AniQuantity[ind]) * Number(AniPrices[ind].replace(",", "."));
        $("#" + SCphtot[1] + ind).text(FormatMoney(subtot));

        if (AniQuantity[ind] == 0) {
            $("#" + SCphrow[1] + ind).hide();
            $("div[id^='" + SCphrow[3] + ind + "']").hide();
        }
        else {
            if (SubAniQuantity[ind] != null) {
                for (var j = 0; j < SubAniQuantity[ind].length; j++) {

                    if (SubAniQuantity[ind][j] == 1) {

                        $("#" + SCphdes[3] + ind + "_" + j).text(SubAniTitles[ind][j]);
                        $("#" + SCphprc[3] + ind + "_" + j).text(SubAniPrices[ind][j]);
                        $("#" + SCphqnt[3] + ind + "_" + j).text(AniQuantity[ind]);

                        //var subtot = Number(AniQuantity[ind]) * Number(SubAniQuantity[ind][j]) * Number(SubAniPrices[ind][j].replace(",", "."));

                        //$("#" + SCphtot[3] + ind + "_" + j).text(FormatMoney(subtot));
                    }
                    else {
                        $("#" + SCphrow[3] + ind + "_" + j).hide();
                    }

                }
            }

        }
    }


}

function NoDelButtons() {
    for (var i = 0; i < menu.length; i++) {
        $("#" + SCphdel[6] + i).css("visibility", "hidden");
    }

    for (var i = 0; i < activity.length; i++) {
        $("#" + SCphdel[5] + i).css("visibility", "hidden");
    }

    for (var i = 0; i < animal.length; i++) {
        $("#" + SCphdel[1] + i).css("visibility", "hidden");

    }
}

function NoChangeSelect() {
    for (var i = 0; i < menu.length; i++) {
        $("#Mnu" + phqnt + i).prop('disabled', true);
    }

    for (var i = 0; i < activity.length; i++) {
        $("#Act" + phqnt + i).prop('disabled', true);
    }

    for (var i = 0; i < animal.length; i++) {
        $("#Ani" + phqnt + i).prop('disabled', true);

    }

}


function GetSubAnimalPricesSC(AnimalIndex) {

    SubAniQuantity = new Array(animal.length);
    SubAniTitles = new Array(animal.length);
    SubAniDescriptions = new Array(animal.length);
    SubAniPrices = new Array(animal.length);

    var URL = "/Acquisto/GetSubAnimalTimes";

    var IdAnimalFather = animal[AnimalIndex];

    var Info = {
        IdAnimalFather: IdAnimalFather,
        Quant: ""
    };

    var promise = $.post(URL, Info);

    promise.done(function (data) {
        ArrFinish++;
        var lung = data.length;
        var i;
        subanimal[AnimalIndex] = data;
        SubAniQuantity[AnimalIndex] = new Array(lung);
        SubAniTitles[AnimalIndex] = new Array(lung);
        SubAniDescriptions[AnimalIndex] = new Array(lung);
        SubAniPrices[AnimalIndex] = new Array(lung);

        if (lung == 0 && AnimalIndex == (animal.length - 1)) {
            Finish();
        }

        for (i = 0; i < lung; i++) {
            SubAniQuantity[AnimalIndex][i] = 0;
            SubAniTitles[AnimalIndex][i] = data[i].Title;
            SubAniDescriptions[AnimalIndex][i] = data[i].Description;
            SubAniPrices[AnimalIndex][i] = data[i].Price;

            var htm = SubSCAniSec.replace(SCphdes[3], SCphdes[3] + AnimalIndex + '_' + i);

            htm = htm.replace(SCphrow[3], SCphrow[3] + AnimalIndex + '_' + i);
            htm = htm.replace(SCphprc[3], SCphprc[3] + AnimalIndex + '_' + i);
            htm = htm.replace(SCphqnt[3], SCphqnt[3] + AnimalIndex + '_' + i);
            //htm = htm.replace(SCphtot[3], SCphtot[3] + AnimalIndex + '_' + i);
            $("#" + SCphdes[4] + AnimalIndex).append(htm);
            $("#" + SCphprc[3] + AnimalIndex + '_' + i).text(data[i].Price);
            $("#" + SCphdes[3] + AnimalIndex + '_' + i).text(data[i].Title);
        }

        //check finish loop
        if (ArrFinish == animal.length) {
            Finish();
        }

    });

    promise.fail(function (err) { console.log("error promise: " + URL); });
}

function LoadFromModel(DataModel) {

    ticket = DataModel.TckId;

    animal = DataModel.TckOptions.AniId;
    subanimal = DataModel.TckOptions.SubAniId;
    activity = DataModel.TckOptions.ActId;
    menu = DataModel.TckOptions.MnuId;

    Quantity = DataModel.TckQuantitities;
    Prices = DataModel.TckPrices;
    Titles = DataModel.TckTitles;
    Descriptions = DataModel.TckDescriptions;
    Notes = DataModel.TckNotes;

    AniQuantity = DataModel.TckOptions.AniQuantitities;
    AniPrices = DataModel.TckOptions.AniPrices;
    AniTitles = DataModel.TckOptions.AniTitles;
    AniDescriptions = DataModel.TckOptions.AniDescriptions;
    AniNotes = DataModel.TckOptions.AniNotes;

    ActQuantity = DataModel.TckOptions.ActQuantitities;
    ActPrices = DataModel.TckOptions.ActPrices;
    ActTitles = DataModel.TckOptions.ActTitles;
    ActDescriptions = DataModel.TckOptions.ActDescriptions;
    ActNotes = DataModel.TckOptions.ActNotes;

    MnuQuantity = DataModel.TckOptions.MnuQuantitities;
    MnuPrices = DataModel.TckOptions.MnuPrices;
    MnuTitles = DataModel.TckOptions.MnuTitles;
    MnuDescriptions = DataModel.TckOptions.MnuDescriptions;
    MnuNotes = DataModel.TckOptions.MnuNotes;

}

function Sostituzione(htm, elements, index) {
    var ret = htm;
    for (var i = 0; i < elements.length; i++) {
        ret = ret.replace(elements[i], elements[i] + index);
    }

    return ret;
}

function CreaListaSC(i) {
    var elements = [SCphdes[i], SCphrow[i], SCphprc[i], SCphqnt[i], SCphtot[i], SCphdel[i]];
    return elements;
}

function GetSubAnimalPricesSCop(AnimalIndex) {

    var lung = subanimal[AnimalIndex].length;

    for (i = 0; i < lung; i++) {

        var htm = SubSCAniSec.replace(SCphdes[3], SCphdes[3] + AnimalIndex + '_' + i);

        htm = htm.replace(SCphrow[3], SCphrow[3] + AnimalIndex + '_' + i);
        htm = htm.replace(SCphprc[3], SCphprc[3] + AnimalIndex + '_' + i);
        htm = htm.replace(SCphqnt[3], SCphqnt[3] + AnimalIndex + '_' + i);
        //htm = htm.replace(SCphtot[3], SCphtot[3] + AnimalIndex + '_' + i);
        $("#" + SCphdes[4] + AnimalIndex).append(htm);

        $("#" + SCphprc[3] + AnimalIndex + '_' + i).text(SubAniPrices[AnimalIndex][i]);
        $("#" + SCphdes[3] + AnimalIndex + '_' + i).text(SubAniDescriptions[AnimalIndex][i]);
    }


    FillAllSC();

}

function GetPrices(Day, Dynamic) {
    //set the prices for each kind of ticket available in the day
    //and all the other options
    Arrloop = 0;
    if (ticket) {
        if (Dynamic == true) {
            for (var i = 0; i < ticket.length; i++) {
                GetTicketPrice(ticket[i], '1', Day, i);
            }
        }
        else {
            for (var i = 0; i < ticket.length; i++) {
                GetTicketFixed(ticket[i], i);
            }
        }
    }

    if (activity) {
        for (var i = 0; i < activity.length; i++) {
            GetActivityPrice(activity[i], '1', Day, i);
        }
    }

    if (menu) {
        for (var i = 0; i < menu.length; i++) {
            GetMenuPrice(menu[i], '1', Day, i);
        }
    }

    if (animal) {
        for (var i = 0; i < animal.length; i++) {
            GetAnimalPrice(animal[i], '1', Day, i);
        }
    }


    //the subanimals starts after in LoopAnimals

}



function GetAddOnPrices(Day) {
    //set the prices for each kind of ticket available in the day
    //only addons
    Arrloop = 0;

    Lungloop = activity.length + menu.length + animal.length;

    if (Lungloop == 0) {
        Riempimento();
    }

    if (activity) {
        for (var i = 0; i < activity.length; i++) {
            GetActivityPrice(activity[i], '1', Day, i);
        }
    }

    if (menu) {
        for (var i = 0; i < menu.length; i++) {
            GetMenuPrice(menu[i], '1', Day, i);
        }
    }

    if (animal) {
        for (var i = 0; i < animal.length; i++) {
            GetAnimalPrice(animal[i], '1', Day, i);
        }
    }


    //the subanimals starts after in LoopAnimals

}

function GetTicketPrice(KindTicket, Quantity, Day, option) {

    var URL = "/Acquisto/GetPrice";

    var Info = {
        Codice: KindTicket,
        Quantita: Quantity,
        Giorno: Day
    };

    var promise = $.post(URL, Info);

    promise.done(function (data) {
        Arrloop++;

        //get the price info
        Titles[option] = escapeHTML(data.Title);
        Descriptions[option] = escapeHTML(data.Description);
        Notes[option] = escapeHTML(data.Note);
        BasePrices[option] = data.BasePrice;
        Prices[option] = data.Price;

        $("#riga_" + option).show();

        if (Arrloop == Lungloop) {
            Riempimento();
        }

    });

    promise.fail(function (err) { console.log("error promise: " + URL); });

}

function GetTicketFixed(KindTicket, option) {

    var URL = "/Acquisto/GetTicketFixed";

    var Info = {
        Codice: KindTicket
    };

    var promise = $.post(URL, Info);

    promise.done(function (data) {
        Arrloop++;


        //get the price info
        Titles[option] = escapeHTML(data.Title);
        Descriptions[option] = escapeHTML(data.Description);
        Notes[option] = escapeHTML(data.Note);
        BasePrices[option] = data.BasePrice;

        if (Prodotto == "ticket") {
            Prices[option] = TckPrices[option];
        }
        else {
            Prices[option] = data.Price;
        }


        $("#riga_" + option).show();


        if (Arrloop == Lungloop) {
            Riempimento();
        }

    });

    promise.fail(function (err) { console.log("error promise: " + URL); });

}

function GetActivityPrice(KindActivity, Quantity, Day, option) {
    var URL = "/Acquisto/GetActivityPrice";

    var Info = { Codice: KindActivity, Quantita: Quantity, Giorno: Day };


    var promise = $.post(URL, Info);

    promise.done(function (data) {
        Arrloop++;

        //get the price info
        ActTitles[option] = escapeHTML(data.Title);
        ActDescriptions[option] = escapeHTML(data.Description);
        ActNotes[option] = escapeHTML(data.Note);
        ActBasePrices[option] = data.BasePrice;
        ActPrices[option] = data.Price;
        ActImages[option] = data.Image;

        if (Arrloop == Lungloop) {
            Riempimento();
        }

    });

    promise.fail(function (err) { console.log("error promise: " + URL); });

}

function escapeHTML(html) {
    if (html == null) {
        return "";
    }

    var fn = function (tag) {
        var charsToReplace = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#39;',
            '/': '&#x2F;',
            '`': '&#x60;',
            '=': '&#x3D;'
        };
        return charsToReplace[tag] || tag;
    }
    return html.replace(/[&<>"]/g, fn);
}

var codeHt = ['&', ' ', 'à', '<', '>', '"', "'", '/', '`', '='];
var decodeHt = ['&amp;', '&nbsp;', '&agrave;', '&lt;', '&gt;', '&quot;', '&#39;', '&#x2F;', '&#x60;', '&#x3D;'];



function decodeHTML(html) {
    var str = html;

    for (var i = 0; i < codeHt.length; i++) {
        str = ReplaceAll(str, decodeHt[i], codeHt[i]);
    }

    return str;
}

function ReplaceAll(htm, old_s, new_s) {
    var final;
    final = htm.replace(new RegExp(old_s, "g"), new_s);
    return final;
}


function GetMenuPrice(KindActivity, Quantity, Day, option) {
    var URL = "/Acquisto/GetmenuPrice";

    var Info = { Codice: KindActivity, Quantita: Quantity, Giorno: Day };


    var promise = $.post(URL, Info);

    promise.done(function (data) {
        Arrloop++;
        Title = data.Title;
        Description = data.Description;
        Note = data.Note;

        //get the price info
        MnuTitles[option] = escapeHTML(Title);
        MnuDescriptions[option] = escapeHTML(Description);
        MnuNotes[option] = escapeHTML(Note);

        MnuBasePrices[option] = data.BasePrice;
        MnuPrices[option] = data.Price;
        MnuImages[option] = data.Image;

        if (Arrloop == Lungloop) {
            Riempimento();
        }

    });

    promise.fail(function (err) { console.log("error promise: " + URL); });

}

function GetAnimalPrice(KindAnimal, Quantity, Day, option) {
    var URL = "/Acquisto/GetAnimalPrice";

    var Info = {
        Codice: KindAnimal,
        Quantita: Quantity,
        Giorno: Day
    };

    var promise = $.post(URL, Info);

    promise.done(function (data) {
        Arrloop++;

        //get the price info
        AniTitles[option] = escapeHTML(data.Title);
        AniDescriptions[option] = escapeHTML(data.Description);
        AniNotes[option] = escapeHTML(data.Note);
        AniBasePrices[option] = data.BasePrice;
        AniPrices[option] = data.Price;
        AniImages[option] = data.Image;

        if (Arrloop == Lungloop) {
            Riempimento();
        }

    });

    promise.fail(function (err) { console.log("error promise: " + URL); });

}

function GetSubAnimalPrices(AnimalIndex) {
    var URL = "/Acquisto/GetSubAnimalTimes";

    var IdAnimalFather = animal[AnimalIndex];

    var Info = {
        IdAnimalFather: IdAnimalFather,
        Quant: ""
    };

    var promise = $.post(URL, Info);

    promise.done(function (data) {
        ArrFinish++;
        var lung = data.length;

        subanimal[AnimalIndex] = data;
        SubAniQuantity[AnimalIndex] = new Array(lung);
        SubAniTitles[AnimalIndex] = new Array(lung);
        SubAniDescriptions[AnimalIndex] = new Array(lung);
        SubAniNotes[AnimalIndex] = new Array(lung);
        SubAniPrices[AnimalIndex] = new Array(lung);


        for (var i = 0; i < lung; i++) {
            SubAniQuantity[AnimalIndex][i] = 0;
            SubAniTitles[AnimalIndex][i] = data[i].Title;
            SubAniDescriptions[AnimalIndex][i] = data[i].Description;
            SubAniNotes[AnimalIndex][i] = data[i].Note;
            SubAniPrices[AnimalIndex][i] = data[i].Price;
        }

        //check finish loop
        if (ArrFinish == animal.length) {
            Finish();
        }
    });

    promise.fail(function (err) { console.log("error promise: " + URL); });
}

function ReloadSubAniQuantity() {

    //fill from linear array into the bidimensional
    var SubAniIndex = 0;
    for (var i = 0; i < animal.length; i++) {

        if (subanimal[i] == null) { subanimal[i] = ""; }
        for (var j = 0; j < subanimal[i].length; j++) {
            while (SubAniQuantLinear[SubAniIndex] == "") {
                SubAniIndex++;
            }
            SubAniQuantity[i][j] = SubAniQuantLinear[SubAniIndex];

            //important!
            if (SubAniQuantity[i][j] == null) { SubAniQuantity[i][j] = 0; }

            SubAniIndex++;
        }
    }

}

function TotPrezzo() {
    TotPrice = Number(0);

    if (Prodotto == "ticket" || Prodotto == null) {
        for (var i = 0; i < ticket.length; i++) {

            if (Prices[i] == null) { Prices[i] = "0"; }
            if (Quantity[i] == null) { Quantity[i] = "0"; }

            TotPrice += Number(Prices[i].replace(",", ".")) * Number(Quantity[i]);
        }
    }
    else {
        //hotel      
        //TotPrice = Number(TotHotelPrice);
        TotPrice = TotaleHotel(Npag);
    }



    for (var i = 0; i < activity.length; i++) {
        if (ActQuantity[i] == null) { ActQuantity[i] = 0; }
        if (ActPrices[i] == null) { ActPrices[i] = "0,00"; }
        TotPrice += Number(ActPrices[i].replace(",", ".")) * Number(ActQuantity[i]);
    }


    for (var i = 0; i < menu.length; i++) {
        if (MnuQuantity[i] == null) { MnuQuantity[i] = 0; }
        if (MnuPrices[i] == null) { MnuPrices[i] = "0,00"; }
        TotPrice += Number(MnuPrices[i].replace(",", ".")) * Number(MnuQuantity[i]);
    }



    for (var i = 0; i < animal.length; i++) {
        if (AniQuantity[i] == null) { AniQuantity[i] = 0; }
        if (AniPrices[i] == null) { AniPrices[i] = "0"; }
        TotPrice += Number(AniPrices[i].replace(",", ".")) * Number(AniQuantity[i]);


        if (subanimal[i] != null) {
            for (var j = 0; j < subanimal[i].length; j++) {
                var qnt = 0;

                if (SubAniQuantity[i] != null || SubAniQuantity[i] != "") {
                    if (SubAniQuantity[i][j] != null) {
                        qnt = SubAniQuantity[i][j];
                    }
                }

                var prc = 0;

                if (SubAniPrices[i] != null) {
                    prc = SubAniPrices[i][j];
                }

                TotPrice += Number(AniQuantity[i]) * Number(prc.replace(",", ".")) * Number(qnt);
            }
        }
    }

    TotPrice = TotPrice.toFixed(2);
    TotPrice = FormatMoney(TotPrice);

    if (TotPrice > 0) {
        $("#continue").show();
    }
    else {
        $("#continue").hide();
    }



    $("#TotPrice").text(TotPrice);
}


function FinCalc(Prc, Np) {
    Prc = Prc.replace(",", ".");
    return Number((Prc * Np).toFixed(2));
}


function TotaleHotel(Npag) {
    var Notti = Number(Giorni(DayArrive, DayDeparture));
    var totH = FinCalc(HotelPrice, Npag);

    if (Notti == 2) {
        totH = totH + Number(Npag * ExtraNight1);
    }

    totH = totH + Number(AssicCambioData * AssicPrice);
    totH = totH + Number(ServizioNavetta * Npag * NavPrice);

    return totH;

}

function RoomsAndTotHotel() {
    var Npag = 0;
    for (var i = 0; i < Nrooms; i++) {
        Npag += Number(adults[i]) + Number(kids[i]);
        var room = { Adults: adults[i], Kids: kids[i], KidsFree: kidsfree[i] };
        Rooms.push(room);
    }

    TotHotelPrice = TotaleHotel(Npag);
}


function Giorni(d1, d2) {
    var dt1 = new Date(d1.substr(3, 2) + "/" + d1.substr(0, 2) + "/" + d1.substr(6, 4));
    var dt2 = new Date(d2.substr(3, 2) + "/" + d2.substr(0, 2) + "/" + d2.substr(6, 4));
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
}

function DataIso(data) {
    //date in iso format yyyy-mm-dd starting from dd/mm/yyyy
    var dt = data.substr(6, 4) + "-" + data.substr(3, 2) + "-" + data.substr(0, 2);
    return dt;
}

function RiempiSC() {
    $("#SCticketlist").empty();
    $("#SCanimallist").empty();
    $("#SCactivitylist").empty();
    $("#SCmenulist").empty();

    //shopping cart creation, with data present

    //loop creation tickets
    LoopCreationSC();


    //loop creation animals
    if (animal.length == 0) {
        FillAllSC();
    }


    for (var i = 0; i < animal.length; i++) {

        var htm = SCAniSec.replace(SCphdes[1], SCphdes[1] + i);
        htm = htm.replace(SCphrow[1], SCphrow[1] + i);
        htm = htm.replace(SCphprc[1], SCphprc[1] + i);
        htm = htm.replace(SCphqnt[1], SCphqnt[1] + i);
        htm = htm.replace(SCphtot[1], SCphtot[1] + i);
        htm = htm.replace(SCphdes[4], SCphdes[4] + i);
        htm = htm.replace(SCphdel[1], SCphdel[1] + i);
        htm = htm.replace("DelAniLineId", i);
        $("#SCanimallist").append(htm);

        //loop creation subanimal
        GetSubAnimalPricesSCop(i);

    }

}



function RiempiAnimalSC() {
    //loop creation animals
    if (animal.length == 0) {
        Finish();
    }

    ArrFinish = 0;
    for (var i = 0; i < animal.length; i++) {

        var htm = Sostituzione(SCAniSec, CreaListaSC(1), i);
        //sublevel
        htm = htm.replace(SCphdes[4], SCphdes[4] + i);
        htm = htm.replace("DelAniLineId", i);
        $("#SCanimallist").append(htm);

        //loop creation subanimal
        GetSubAnimalPricesSC(i);
    }
}


function FillCombo(product, param) {
    //fill combo
    for (i = 0; i < product.length; i++) {
        var maxc = maxcombo;

        //dog house eexception
        if (product[i] == 66) {
            maxc = 2;
        }

        for (j = 0; j < maxc + 1; j++) {
            var tx = '...';
            if (j > 0) { tx = j }
            $('#' + param + i).append($('<option>', {
                value: j, text: tx
            }));
        }
    }
}

function FillComboQuant(param, nmax) {
    //fill combo
    for (j = 0; j < nmax + 1; j++) {
        var tx = '...';
        if (j > 0) { tx = j }
        $('#' + param).append($('<option>', {
            value: j, text: tx
        }));
    }
}

function LoopAnimals() {
    //loop creation animals
    if (animal.length == 0) {
        Finish();
    }

    ArrFinish = 0;

    for (var i = 0; i < animal.length; i++) {

        if (azzera == true) {
            AniQuantity[i] = "0";
        }

        //loop creation subanimal

        SubAniQuantity = new Array(animal.length);
        SubAniTitles = new Array(animal.length);
        SubAniDescriptions = new Array(animal.length);
        SubAniNotes = new Array(animal.length);
        SubAniPrices = new Array(animal.length);

        GetSubAnimalPrices(i);
    }
}

function ReadData(data) {
    ticket = data.Tickets;

    if (DiscountCode != "") {
        var URL = "/Acquisto/GetDiscount";

        var Info = { DiscountCode: DiscountCode, Day: Day };
        var promise = $.post(URL, Info);

        promise.done(function (data) {

            for (var i = 0; i < data.length; i++) {
                //replace dynamic ticket with full ticket
                ticket[i] = data[i].TicketId;
                Prices[i] = data[i].CouponPrice;
            }
        });
    }

    animal = data.Animals;
    AnimalMaxQuantity = data.AnimalsQuantity;

    subanimal = data.SubAnimals;
    SubAnimalMaxQuantity = data.SubAnimalsQuantity;
    activity = data.Activities;
    menu = data.Menus;

    Lungloop = ticket ? (ticket.length + activity.length + menu.length + animal.length) : null;

    Quantity = [];
    AniQuantity = [];
    ActQuantity = [];
    MnuQuantity = [];

}


function InizializzaHotel() {
    //delete the others
    DeleteAllCookies();

    //write
    setCookie("DayArrive", DayArrive);
    setCookie("DayDeparture", DayDeparture);
    setCookie("Nrooms", Nrooms);
    setCookie("HotelId", HotelId);

    setCookie("HotChangeDate", HotChangeDate);

    setCookie("HotelPriceId", HotelPriceId);
    setCookie("HotelDescription", hotDes);
    setCookie("TotPrice", TotPrice);

    setCookie("adults", JSON.stringify(adults));
    setCookie("kids", JSON.stringify(kids));
    setCookie("kidsfree", JSON.stringify(kidsfree));

    SetCookieBuyer();
    FillBuyer();

    FillSCHotel();

    $("#TotPrice").text(TotPrice);

    //acquisition options data
    var URL = "/Acquisto/GetData";

    var Info = { Day: DayArrive };

    var promise = $.post(URL, Info);

    promise.done(function (data) {
        ReadData(data);

        GetPrices(DayArrive);

        $("#summary").show();

    });

}

function FillSCHotel() {
    var totAdu = 0;
    var totKid = 0;

    var htm = HotelSec;
    $("#SCHotellist").append(htm);

    if (HotChangeDate == false) {
        $("#Nassic").css("display", "none");
    }

    //$("#hotdes").text(decodeHTML(hotDes));
    $("#hotdes").text("Parco + Hotel ");
    $("#numcams").text(Nrooms);

    for (var i = 0; i < Nrooms; i++) {
        totAdu += Number(adults[i]);
        totKid += Number(kids[i]);

        //htm = Roomsec;

        //htm = htm.replace(cam, cam + i);
        //htm = htm.replace(ncam, ncam + i);
        //htm = htm.replace(nadu, nadu + i);
        //htm = htm.replace(nkid, nkid + i);
        //htm = htm.replace(nkidf, nkidf + i);

        ////dettaglio stanze:
        //$("#SCroomlist").append(htm);
    }

    Npag = totAdu + totKid;

    Notti = Giorni(DayArrive, DayDeparture);

    var Risp = "NO";
    var RispNav = "NO";

    if (AssicCambioData == 1) { Risp = "SI"; }
    if (ServizioNavetta == 1) { RispNav = "SI"; }

    TotPrice = TotaleHotel(Npag);

    $("#hotperiodo").text("Dal:" + DayArrive + " Al:" + DayDeparture);
    $("#Npag").text(Npag);
    $("#Nnotti").text(Notti);
    $("#Ntot").text(FormatMoney(TotPrice) + "€");
    //$("#Nnavetta").text("Servizio navetta: " + RispNav);
    $("#Nassic").text("Servizio cambio data: " + Risp);

    //dettaglio stanze:
    //for (var i = 0; i < Nrooms; i++) {
    //    $("#" + ncam + i).text(i + 1);
    //    $("#" + nadu + i).text(adults[i]);
    //    $("#" + nkid + i).text(kids[i]);
    //    $("#" + nkidf + i).text(kidsfree[i]);   
    //}
}

function InizializzaSubscription() {
    DeleteAllCookies();
    GetBuyer();

    //write
    setCookie("SubscriptionId", JSON.stringify(SubscriptionId));
    setCookie("Quantity", JSON.stringify(Quantity));
    setCookie("Names", JSON.stringify(Names));
    setCookie("Surnames", JSON.stringify(Surnames));
    setCookie("SubscriptionTotPrice", TotPrice);


    SetCookieBuyer();
    FillBuyer();

    var Conta = 0;
    for (var i = 0; i < nsub; i++) {
        htm = SubSec;

        var quant = Number(Quantity[i]);

        if (quant > 0) {
            htm = htm.replace(SCphabb, SCphabb + i);
            htm = htm.replace(SCphdsc, SCphdsc + i);
            htm = htm.replace(SCphqua, SCphqua + i);
            htm = htm.replace(SCphprz, SCphprz + i);
            htm = htm.replace(SCphtol, SCphtol + i);
            htm = htm.replace(SCphsct, SCphsct + i);
            $("#SCsublist").append(htm);

            $("#" + SCphdsc + i).text(Description[i]);
            $("#" + SCphqua + i).text(Quantity[i]);
            $("#" + SCphprz + i).text(FormatMoney(Price[i]));
            $("#" + SCphtol + i).text(FormatMoney(Price[i] * Quantity[i]));


            //subscriber
            htm = NamSec;


            for (var j = Conta; j < Conta + quant; j++) {
                htm = NamSec;
                htm = htm.replace(phnln, phnln + j);
                htm = htm.replace(phnam, phnam + j);
                htm = htm.replace(phcog, phcog + j);

                $("#" + SCphsct + i).append(htm);
            }

            for (var j = Conta; j < Conta + quant; j++) {
                $("#" + phnam + j).text(Names[j]);
                $("#" + phcog + j).text(Surnames[j]);
            }

        }

        Conta = Conta + quant;
    }

    $("#summary").show();
    $("#TotPrice").text(FormatMoney(TotPrice));

}

function HideShopping() {
    //$("#pricesummary").hide();
    //$("#summary").hide();
    $("#SCticketlist").hide();
    $("#SChotellist").hide();
    $("#SCmenulist").hide();
    $("#SCactivitylist").hide();
    $("#SCanimallist").hide();
    $("#SCHotellist").hide();
    $("#SCroomlist").hide();
    $("#SCsublist").hide();
}

function ShowShopping() {
    //$("#pricesummary").show();
    //$("#summary").show();
    $("#SCticketlist").show();
    $("#SCmenulist").show();
    $("#SCactivitylist").show();
    $("#SCanimallist").show();
    $("#SCHotellist").show();
    $("#SCroomlist").show();
    $("#SCsublist").show();
}

function Nobody(val) {
    if (val == "0") {
        return "nessuno";
    }
    else {
        return val;
    }
}

function ReadCookieOptions() {
    animal = JSON.parse(getCookie("animal"));
    activity = JSON.parse(getCookie("activity"));
    menu = JSON.parse(getCookie("menu"));

    AniQuantity = JSON.parse(getCookie("AniQuantity"));
    ActQuantity = JSON.parse(getCookie("ActQuantity"));
    MnuQuantity = JSON.parse(getCookie("MnuQuantity"));


    TotPrice = getCookie("TotPrice");
}

function SetCookieOptions() {
    setCookie("animal", JSON.stringify(animal));
    setCookie("activity", JSON.stringify(activity));
    setCookie("menu", JSON.stringify(menu));
    setCookie("TotPrice", TotPrice);

    setCookie("MnuQuantity", JSON.stringify(MnuQuantity));

    setCookie("AniQuantity", JSON.stringify(AniQuantity));

    setCookie("SubAniQuantity", JSON.stringify(SubAniQuantity));

    setCookie("ActQuantity", JSON.stringify(ActQuantity));

}

