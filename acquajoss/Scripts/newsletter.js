﻿$(document).ready(function () {
    if ($.serializeJSON) {
        $("#inscrivi").on("submit", function (e) {
            console.log("entranews");
            e.preventDefault();
            console.log(e);
            var $form = $(this);
            var recaptcha = grecaptcha.getResponse();
            var $msn = $form.find("#alert");
            //var $btn = $form.find(":submit").button("loading");
            if ($form.validate() && recaptcha != "") {
                var data = $form.serializeJSON();

                if ($msn.length) $msn.removeAttr("class").addClass("alert");
                console.log(data);

                $.post("/ajax/NewsLetter/", data, function (response) {
                    console.log(response);
                    $("#EmailNews").val("");
                    $("#PrivacyCheck").prop("checked", false);
                    grecaptcha.reset();
                    if ($msn.length) $msn.addClass("alert-" + response.type).html("<b>" + response.type.toUpperCase() + "!</b> " + $form.attr("user_" + response.status)).show().delay(3000).slideUp('fast');
                }, "JSON").fail(function () {
                    if ($msn.length) $msn.addClass("alert-danger").html("<b>ERROR!</b> " + $form.attr("user_error")).show().delay(3000).slideUp('fast');
                })//.always(function () {
                //    $btn.button("reset");
                //});
            }
            else {
                $msn.addClass("alert-danger").html("<b>ERROR!</b> Errore di convalida di Goolgle reCaptcha").show().delay(3000).slideUp('fast');
                $("#g-recaptcha > div").css("border", "1px solid red");
                var captcha = $("#g-recaptcha > div");
                setTimeout(function () {
                    captcha.css("border", "1px solid #d3d3d3");
                }, 3000);
                //console.log("Error de validación de Goolgle reCaptcha");
            }
        });
    }
    else {
        console.log("script serializeJSON is required");
    }
});