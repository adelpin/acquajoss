﻿using Core;
using Core.Db;
using System.Web.Mvc;

namespace Payments.Controllers
{
    public class ShoppingCartController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            string color = "red";
            ViewBag.ParkOpen = Repository.GetParkOpen(ref color);
            ViewBag.ParkColor = color;
        }


        //METHODS

        private void SendViewBagOptions()
        {
            ViewBag.PrezzoAssicurazione = Impostazioni.PrezzoAssicurazione;
            ViewBag.PrezzoNavetta = "0";

            ViewBag.FamilyPack = "";
            string fam = Session["FamilyPack"].ToString();
            Product Ticket = Impostazioni.ViewBagFamily(fam);

            if (Ticket != null)
            {
                ViewBag.FamilyTitle = Ticket.Title;
                ViewBag.FamilyDescription = Ticket.Description;
                ViewBag.FamilyPack = fam.ToString();
            }


            ViewBag.Dicitura = InfoShopCart.Dicitura;
            ViewBag.shoptitle = InfoShopCart.shoptitle;
            ViewBag.shopdescription = InfoShopCart.shopdescription;
            ViewBag.shopcomment = InfoShopCart.shopcomment;
            ViewBag.Prodotto = InfoShopCart.Prodotto;
            ViewBag.Day = InfoShopCart.Day;
            ViewBag.Scegli = InfoShopCart.Scegli;
            ViewBag.TotPrice = InfoShopCart.TotPrice;
            ViewBag.Options = InfoShopCart.Options;
            ViewBag.TipoOperatore = Impostazioni.TipoOperatore();

            ViewBag.Step1 = Impostazioni.Steps[0].Title;
            ViewBag.Step2 = Impostazioni.Steps[1].Title;
            ViewBag.Step3 = Impostazioni.Steps[2].Title;
        }

        //actionresults
        public ActionResult GetShopping(ShoppingList Shopping)
        {
            Session["Lista"] = Shopping;
            return Json("");
        }

        public ActionResult Checkout(string Scelta)
        {

            //acquistion ticket
            ShoppingList Shopping = (ShoppingList)Session["Lista"];

            if (Shopping == null)
            {
                //cookies
                if (Impostazioni.EsisteCookie("Day") != "")
                {
                    string Day = Request.Cookies["Day"].Value;
                    Shopping = Impostazioni.RebuildShopping(Day);
                }                  
            }


            //acquistion ticket + hotel
            ShoppingListHotel ShoppingHotel = (ShoppingListHotel)Session["ListaHotel"];

            if (ShoppingHotel == null)
            {
                //cookies
                if (Request.Cookies["DayArrive"] != null)
                {
                    ShoppingHotel = Impostazioni.RebuildShoppingHotel();
                }            
            }


            //acquistion subscriptions
            ShoppingListSubscription ShoppingSubscription = (ShoppingListSubscription)Session["ListaAbbonamenti"];

            if (ShoppingSubscription == null)
            {
                //cookies
                if (Request.Cookies["SubscriptionTotPrice"] != null)
                {
                    ShoppingSubscription = Impostazioni.RebuildShoppingSubscriptions();
                }
            }


            //however buyer info always get from cookies
            
            if (Shopping != null)
            {
                Session["Lista"] = Shopping;
            }

            if (ShoppingHotel != null){
                Session["ListaHotel"] = ShoppingHotel;
            }

            if (ShoppingSubscription != null) {
                Session["ListaAbbonamenti"] = ShoppingSubscription;
            }

            return Json(Url.Action(Scelta, "Checkout"));
        }

        //overload
        public ActionResult RedirCheckout(string Scelta)
        {
            //acquistion
            ShoppingList Shopping = (ShoppingList)Session["Lista"];


            return Json(Url.Action(Scelta, "Checkout"));
        }


        public ActionResult ShoppingCart()
        {
            Session["FromShopping"] = true;

            Shops Shop = Repository.GetShopFromShopping();

            if (Shop == null)
            {
                Log.Write("Shoppingcart: no cookies found redirect to home");
                return RedirectToAction("Index", "Home");
            }

            //acquisto operatore o verifica cliente
            ViewBag.VerificaCliente = Session["VerificaCliente"] == null ? "" : Session["VerificaCliente"].ToString();

            ViewBag.TipoOperatore = Impostazioni.TipoOperatore();
            ViewBag.IdOperatore = Impostazioni.IdOperatore();
            ViewBag.AcquistoId = Session["ReservationId"] == null ? "" : Session["ReservationId"].ToString();
            ViewBag.Svincolato = Session["Svincolato"] == null ? "" : Session["Svincolato"].ToString();


            //if (ViewBag.Svincolato == "")
            //{
            if (Repository.ManageAnomaly(Shop, "ShoppingCart") == true)
            {
                return RedirectToAction("Index", "Home");
            }
            //}


            Impostazioni.FillShopInfo(Shop);
                  
            SendViewBagOptions();

           

            /////////////////////////////
            //groups
            ViewBag.IdGroup = "";
            int TipoAcquisto = Repository.GetPurchaseType(Shop);
            if (TipoAcquisto == 1)
            {
                string TckId = Shop.ShoppingState.TckId[0];

                if (TckId == Repository.GetProdIdGroup())
                {
                    ViewBag.IdGroup = TckId;

                    ViewBag.ShopTitle = "CENTRI ESTIVI";
                }
            }

            ViewBag.DataOggi = Impostazioni.DataOggi();

            TicketOptions Options = new TicketOptions();
            string Prodotto = Repository.GetProductOptions(Shop, ref Options);

            ViewBag.Prodotto = Prodotto;
            ViewBag.Options = Options;


            //overwrite for acquajoss:
            int Type = Repository.GetPurchaseType(Shop);

            if (Type == 1)
            {
                ViewBag.shopcomment = "Il biglietto può essere acquistato anche il giorno stesso della visita al parco";
            }
                
            return View(Shop);

        }

    }
}

