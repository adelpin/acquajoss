﻿//bank reference

using Core;
using Core.Db;
using System;
using System.Data.SqlClient;
using System.Web.Mvc;
using System.Xml;

//using Zoomarine.it.sella.ecomms2s; //PRODUCTION
//using Zoomarine.it.sella.testecomm; //TEST

namespace Payments.Controllers
{
    public class CheckoutController : Controller
    {

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            string color = "red";
            ViewBag.ParkOpen = Repository.GetParkOpen(ref color);
            ViewBag.ParkColor = color;
        }

        //PAYMENTS:
        public ActionResult Complete()
        {
            Log.Write("Checkout complete session: " + Session.SessionID);

            //complete bank payment
            int LogUser = Impostazioni.IdOperatore();

            string BankShopId = "";

            if (Session["BankShopId"] != null)
            {
                BankShopId = Session["BankShopId"].ToString();
            }
            else
            {
                //recovery purchase:
                bool Recovery = Repository.RecoveryPurchase(ref BankShopId);

                if (Recovery == false)
                {
                    return RedirectToAction("Index", "Home");
                }

                Session["BankShopId"] = BankShopId;
            }


            //Shops Shop = Acquisizione();
            Shops Shop = Impostazioni.GetAcquistoOp(BankShopId);


            AcquistoFull AcquistoFull = Repository.GetAcquistoFull(Shop);
            Purchase Acquisto = AcquistoFull.Acquisto;
            InfoBuyer Buyer = AcquistoFull.Buyer;
            Acquisto.Payment_State = "KO";
            Acquisto.Payment_Type = "CC";

            Acquisto.Purchase_Id = BankShopId;


            string Esito = "0";

            string InfoTr = " " + Buyer.Name + " " + Buyer.Surname + " " + Buyer.Email + " Totale: " + Acquisto.Total;

            Log.Write("Processo pagamento banca completato transazione: " + BankShopId + InfoTr);


            if (Impostazioni.SkipBank != "on")
            {
                //after bank payment
                string ScegliBanca = "BS";

                string Nomebanca = Impostazioni.MapBank(ScegliBanca);

                //banca sella:
                Esito = Impostazioni.BancaSellaPayment(BankShopId, LogUser, InfoTr, Acquisto);
             
                ///////////////////////////////////
                //ticket emission
                if (Esito == "0" || Esito == "ErrDec")
                {
                    Acquisto.Purchase_Id = BankShopId;

                    //verify ticket emission
                    int rt = VerifyEmission(Acquisto.Purchase_Id, BankShopId);
                    if (rt == -1) { RedirectToAction("Confirmation", "Home"); }

                    Acquisto.Payment_Type = "CC";

                    if (Esito == "0")
                    {
                        //successo
                        Log.Write("pagamento Banca " + Nomebanca + "riuscito transazione: " + BankShopId + InfoTr);
                        Acquisto.Payment_State = "OK";
                        //activecampaign  validation by email,update with the last data 
                        //CheckactiveCampaign CheckClientActiveCampaign = new CheckactiveCampaign();
                        //CheckClientActiveCampaign.UpdateActiveCampaignClient(Shop, true);
                    }

                    if (Esito == "ErrDec")
                    {
                        Acquisto.Payment_State = "PR";

                        //is not possible to verify go ahead with purchase
                        string Msg = "Non è stato possibile verificare pagamento " + Nomebanca + " transazione: " + BankShopId + " si procede con l'acquisto in stato PR";
                        Msg += InfoTr;
                        Log.Write(Msg);

                        string Body = Msg + " verificare su " + Nomebanca + " lo stato dell'acquisto";
                        Mail.InvioMail("Mancata verifica " + Nomebanca + " transazione", "Zoomarine", Impostazioni.MailCCuser, Body, null, "", null);

                    }


                }
                else
                {
                    //fallimento                  
                    Log.Write("Errore pagamento Banca " + Nomebanca + " transazione: " + BankShopId + InfoTr);
                    Session["BankError"] = "Errore pagamento carta";
                    return RedirectToAction("CarteCredito", "Checkout");
                }


            }
            else
            {
                //skip bank             
                Acquisto.Payment_State = "OK";
            }

            Acquisto.Operator = Impostazioni.IdOperatore();
            Repository.UpdatePurchasePayment(Acquisto.Payment_State, BankShopId);
            TicketCreationAndSend(Acquisto, Shop, Buyer, BankShopId);

            Session["BankShopId"] = null;
            return RedirectToAction("Confirmation", "Home");
        }

        public ActionResult CarteCredito()
        {
            string Pares = Request.Params["PARes"];


            if (Impostazioni.SkipBank == "on")
            {
                return RedirectToAction("Complete");
            }

            Shops Shop = Acquisizione();
            if (Shop == null)
            {
                Log.Write("CarteCredito: no cookies found redirect to home");
                return RedirectToAction("Index", "Home");
            }

            string AcquistoId = Repository.SetPurchase(ref Shop);

            if (AcquistoId == "-1")
            {
                Log.Write("checkout controller: NoPurchase sessionid: " + Session.SessionID);
                return RedirectToAction("Index", "Home");
            }

            Log.Write("Carte credito BankShopId: " + AcquistoId);


            if (Session["BankError"] != null)
            {
                string ErrB = Session["BankError"].ToString();
                Log.Write("Carte credito Errore: " + ErrB + " Id:" + AcquistoId);
                ViewBag.BankError = ErrB;
                Session["BankError"] = null;
            }

            InfoBuyer Buyer = new InfoBuyer();
            string TotPrice = "0";

            int TipoAcquisto = Repository.GetPurchaseType(Shop, out TotPrice, out Buyer);
            string Title = Repository.GetPurchaseTitle(TipoAcquisto, Shop);

            string BuyerName = Buyer.Name + " " + Buyer.Surname;
            TotPrice = TotPrice.Replace(",", ".");

            InfoAcquisto InfoA = Impostazioni.GetAcquisto(Buyer, TotPrice, Title);

            Purchase Acquisto = Repository.GetAcquistoFull(Shop).Acquisto;
            Acquisto.Payment_State = "KO";
            Acquisto.Payment_Type = "CC";

            if ((bool)Session["FromShopping"] == true)
            {
                //come from shoppingcart
                Session["FromShopping"] = false;
                AcquistoId = InsPurchase(Acquisto, Buyer).ToString();

                //inspurchase failed
                if (AcquistoId == "-1")
                {
                    return RedirectToAction("Index", "Error");
                }

                Acquisto.Purchase_Id = AcquistoId;
                InsertFirstPayment(Acquisto, Buyer);

                //Zoho crm:  
                Repository.InsertZoHoCrmSale(AcquistoId, false);
            }
            else
            {
                //come from bank payment
                Impostazioni.ReadBsCn();
            }

            Session["BankShopId"] = AcquistoId;
            Log.Write("Carte credito session: " + Session.SessionID + " acquisto: " + AcquistoId);

            InfoBancaSella InfoBs = Repository.InfoBancaSellaPayment(Acquisto, InfoA, AcquistoId, TotPrice, BuyerName, Buyer.Email, Pares);

            //iframe:
            ViewBag.BsIframeJs = Impostazioni.BsIframeJs;


            //for 3d secure              
            if (!string.IsNullOrEmpty(InfoBs.PARes))
            {
                Log.Write("checkout controller cartecredito banca sella id: " + AcquistoId + " Pares: " + InfoBs.PARes);
            }

            string crypt = WsGenerici.PagaBancaSella(InfoBs);
            Log.Write("checkout controller cartecredito banca sella redirect su pagina cartecredito id: " + AcquistoId + " Session: " + Session.SessionID);

            return View("Cartecredito", InfoBs);

        }

        public ActionResult AssTel()
        {

            ViewBag.PrezzoAssicurazione = Impostazioni.PrezzoAssicurazione;
            ViewBag.PrezzoNavetta = "0";

            Shops Shop = Acquisizione();
            if (Shop == null)
            {
                Log.Write("AssTel: no cookies found redirect to home");
                return RedirectToAction("Index", "Home");
            }

            ViewBag.TipoOperatore = Impostazioni.TipoOperatore();
            ViewBag.IdOperatore = Impostazioni.IdOperatore();
            return View(Shop);
        }

        public ActionResult Bonifico()
        {

            ViewBag.PrezzoAssicurazione = Impostazioni.PrezzoAssicurazione;
            ViewBag.PrezzoNavetta = "0";

            Shops Shop = Acquisizione();
            if (Shop == null)
            {
                Log.Write("Bonifico: no cookies found redirect to home");
                return RedirectToAction("Index", "Home");
            }


            ViewBag.TipoOperatore = Impostazioni.TipoOperatore();
            ViewBag.IdOperatore = Impostazioni.IdOperatore();
            return View(Shop);
        }

        //CONTROLLER METHODS:
        private void TicketCreationAndSend(Purchase Acquisto, Shops Shop, InfoBuyer Buyer, string BankShopId)
        {
            /////////////////////////////////////////
            if (Acquisto.Payment_State == "OK")
            {
                //update products allotment 
                Repository.UpdateProductsAlltoment(Shop);

                //update hotel allotment
                if (Repository.GetPurchaseType(Shop) == 2)
                {
                    UpdateHotelAllotment(Acquisto.Purchase_Id);
                }

                //Ticket Emission

                ///////////////////////////////
                //p + h travel
                int TravelPackage = 0;
                bool Travel = Repository.ParcoPlusHotelTravel(Acquisto, Shop, ref TravelPackage);

                //iva assolta for the invoices
                bool IvaAssolta = false;
                if (Buyer.Invoice == "Y" || Travel == true) { IvaAssolta = true; }
                string EsitoTicket = Repository.TicketEmission(Acquisto.Purchase_Type_Id, Travel, TravelPackage, IvaAssolta);

                if (EsitoTicket != "")
                {
                    string Msg = "mancata emissione biglietto, l inserimento dati prosegue Id: " + Acquisto.Purchase_Id;
                    Msg += " " + Buyer.Name + " " + Buyer.Surname;
                    Log.Write(Msg);

                }
            }

            //insert tmaster data on db
            TmTransazioni Tm = (TmTransazioni)Session["TmTransactions"];

            if (Tm != null)
            {
                Repository.InsertPurchaseTmaster(Tm, BankShopId);
            }

            Log.Write("inizio invio mail cliente acquisto id: " + BankShopId + " mail: " + Buyer.Email);

            //invio mail al cliente
            Mail.InvioMailCliente(Acquisto, Tm, Buyer);


            //send electronic invoice
            if (Impostazioni.ElectronicInvoiceSend == "on")
            {
                if (Buyer.Invoice == "Y")
                {
                    Repository.SendElectronicInvoice(Shop, Buyer, BankShopId, Acquisto.Payment_Type);
                }
            }
        }

        private void UpdateHotelAllotment(string ReservationId)
        {
            Repository.NewBackofficeChangeHotelAllotment(ReservationId);
        }
      
        private int VerifyEmission(string Purchase_Id, string BankShopId)
        {
            if (!String.IsNullOrEmpty(Purchase_Id))
            {
                int ReservationId = Convert.ToInt32(Purchase_Id);
                if (Repository.TicketsEmitted(ReservationId) > 0)
                {
                    Log.Write("Zoomarine complete transazione Banca biglietti già emessi per ReservationId: " + ReservationId);
                    return -1;
                }
            }
            else
            {
                if (Repository.TicketsEmittedByBank(BankShopId) > 0)
                {
                    Log.Write("Zoomarine complete transazione Banca biglietti già emessi per codice trans. banca: " + BankShopId);
                    return -1;
                }

            }

            return 0;
        }

        private void InsertFirstPayment(Purchase Acquisto, InfoBuyer Buyer)
        {
            string Processor = "";

            Processor = Repository.GetProcessor("BANCA SELLA").ToString();

            //if (Impostazioni.Bank == "BS")
            //{
            //    Processor = Repository.GetProcessor("BANCA SELLA").ToString();
            //}
            //else
            //{
            //    Processor = Repository.GetProcessor("UNICREDIT").ToString();
            //}
            string Payment = Repository.GetPaymentCode(Acquisto.Payment_Type).ToString();
            string State = Repository.GetPaymentStateId(Acquisto.Payment_State).ToString();

            Repository.InsertPayments(Payment, Acquisto.Purchase_Id, Acquisto.Purchase_Id, "", Buyer.Email, Buyer.Name,
                Buyer.Surname, Acquisto.Total, Processor, Acquisto.Purchase_Id, "", "242", "", State, "");
        }
        
        private InfoBancaSella BancaSellaPayment(InfoAcquisto InfoA, string BankShopId, string TotPrice, string BuyerName, string Email)
        {
            //true bancasella data:         
            string BsShopLogin = Impostazioni.BsShopLogin;
            string AuthUrl = Impostazioni.BsUrl;

            InfoBancaSella InfoBS = Impostazioni.GetInfoBancaSella(InfoA, BsShopLogin, BankShopId, AuthUrl);

            string crypt = "";

            //webrequest
            string Ws = WsGenerici.BancaSellaRequest(BsShopLogin, TotPrice, BankShopId, BuyerName, Email);
            crypt = WsGenerici.BancasSellaReadWs(Ws);

            InfoBS.XMLOUT = crypt;

            XmlDocument XmlReturn = new XmlDocument();

            XmlReturn.LoadXml(InfoBS.XMLOUT);
            XmlNode ThisNode = XmlReturn.SelectSingleNode("//GestPayCryptDecrypt/ErrorCode");
            InfoBS.ErrorCode = ThisNode.InnerText;
            if (InfoBS.ErrorCode == "0")
            {
                XmlNode ThisNode2 = XmlReturn.SelectSingleNode("//GestPayCryptDecrypt/CryptDecryptString");
                InfoBS.EncryptedString = ThisNode2.InnerText;
            }
            else
            {
                //Put error handle code HERE
                ThisNode = XmlReturn.SelectSingleNode("//GestPayCryptDecrypt/ErrorDescription");
                InfoBS.ErrorDescription = ThisNode.InnerText;
                InfoBS.ErrorClass = "on";
            }

            //no iframe
            //var percorso = AuthUrl + "?a=" + ShopLogin + "&b=" + crypt;
            //return Redirect(percorso);

            //iframe:
            ViewBag.BsIframeJs = Impostazioni.BsIframeJs;
            return InfoBS;
        }

        public void SendViewBagOptions()
        {

            ViewBag.PrezzoAssicurazione = Impostazioni.PrezzoAssicurazione;
            ViewBag.PrezzoNavetta = "0";

            //family pack
            ViewBag.FamilyPack = "";
            string fam = Session["FamilyPack"].ToString();
            Product Ticket = Impostazioni.ViewBagFamily(fam);

            if (Ticket != null)
            {
                ViewBag.FamilyTitle = Ticket.Title;
                ViewBag.FamilyDescription = Ticket.Description;
                ViewBag.FamilyPack = fam.ToString();
            }

            ViewBag.Dicitura = InfoShopCart.Dicitura;
            ViewBag.shoptitle = InfoShopCart.shoptitle;
            ViewBag.shopdescription = InfoShopCart.shopdescription;
            ViewBag.shopcomment = InfoShopCart.shopcomment;
            ViewBag.Prodotto = InfoShopCart.Prodotto;
            ViewBag.Day = InfoShopCart.Day;
            ViewBag.Scegli = InfoShopCart.Scegli;
            ViewBag.TotPrice = InfoShopCart.TotPrice;
            ViewBag.Options = InfoShopCart.Options;
            ViewBag.TipoOperatore = Impostazioni.TipoOperatore();
            ViewBag.IdOperatore = Impostazioni.IdOperatore();
        }

        public Shops Acquisizione()
        {
            Shops Shop = Repository.GetShopFromShopping();


            ///////////////////////////////////////////////////////////////////////////
            //amount calculation for online payments made it again for security reason:
            string Description = "";
            string Money = "";

            InfoBuyer Utente = Repository.GetInfoAcquisto(Shop, ref Money, ref Description);

            InfoAcquisto InfoA = Impostazioni.GetAcquisto(Utente, Money, Description);

            //InfoPaypal InfoPP = Impostazioni.GetInfoPayPal(InfoA);

            ViewBag.Acquisto = InfoA;
            return Shop;
        }

        public int InsPurchase(Purchase Acquisto, InfoBuyer Buyer)
        {
                 
            string Customer_Id = Repository.InsUpdateCustomer(Buyer);
            Log.Write("Inserito o variato nel db il  Cliente: " + Buyer.Name + " " + Buyer.Surname + " con n: " + Customer_Id);

            Acquisto.Customer_Id = Convert.ToInt32(Customer_Id);

            //disabled:
            //string OldAcquistoId = Session["ReservationId"] == null ? "" : Session["ReservationId"].ToString();


            //transaction id by server
            TmTransazioni Tm = new TmTransazioni();

            SqlConnection Conn = new SqlConnection(Impostazioni.Conexion);
            Conn.Open();

            SqlTransaction Transaction = Conn.BeginTransaction();
            Session["TmTransactions"] = null;

            try
            {
                           
                //date now must be added by the server
                Acquisto.Purchase_date = DateTime.Now;

                //insert the purchase on db
                int InsPur = Repository.InsPurchase(Acquisto, Conn, Transaction, "");

                //insert the purchase on db
                string InfoP = Repository.MakeInsPurchaseInfo(Acquisto, Buyer);

                if (InsPur == 0)
                {
                    Log.Write("Errore InsPurchase fallito: " + InfoP);
                    Transaction.Rollback();
                    Repository.CloseTransaction(Conn, Transaction);

                    //ripulisce tutto
                    Session.Clear();
                    Session.Abandon();

                    return -1;
                }

                //success
                Acquisto.Purchase_Id = InsPur.ToString();

                Session["ReservationId"] = Acquisto.Purchase_Id;
                Session["PurchaseDate"] = Impostazioni.DataOggi();

                Transaction.Commit();

                InfoP = "Inserimento acquisto: " + Acquisto.Purchase_Id + InfoP;

                Log.Write(InfoP);
            }
            catch (Exception ex)
            {
                Log.Write("Errore InsPurchase: " + ex.Message);

                Transaction.Rollback();
                Repository.CloseTransaction(Conn, Transaction);

                //ripulisce tutto
                Session.Clear();
                Session.Abandon();

                return -1;
            }

            Repository.CloseTransaction(Conn, Transaction);

            int Ret = Convert.ToInt32(Acquisto.Purchase_Id);

            return Ret;
        }


    }
}