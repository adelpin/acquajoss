﻿using Core;
using System.Collections.Generic;
using System.Web.Mvc;

namespace AcquaJoss.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            if (Session["ErrorMsg"] != null)
            {
                ViewBag.Error = Session["ErrorMsg"].ToString();
            }


            //clear all
            Impostazioni.ClearShopSessions();
            Impostazioni.DeleteAllCookies();
            Session.Clear();
            Session.Abandon();

            return View();
        }


        public ActionResult NotFound()
        {
            string CallingUrl = Request.Url.ToString();

            List<string> OldList = new List<string>();
            OldList.Add("it-IT/index-index/");                                                              //0
            OldList.Add("it-IT/lavora-con-noi/service-/");                                                  //1
            OldList.Add("it-IT/contattaci/service-/");                                                      //2
            OldList.Add("it-IT/mappa-del-parco/index-/");                                                   //3
            OldList.Add("it-IT/bar/index-/");                                                               //4
            OldList.Add("it-IT/bazar/index-/");                                                             //5
            OldList.Add("it-IT/zona-relax/index-/");                                                        //6
            OldList.Add("it-IT/piscina-ad-onde/index-/");
            OldList.Add("it-IT/laguna-idromassaggio/index-/");
            OldList.Add("it-IT/river/index-/");
            OldList.Add("it-IT/acquascivoli-e-kamikaze/index-/");
            OldList.Add("it-IT/colline-scivolose/index-/");
            OldList.Add("it-IT/piscina/index-/");
            OldList.Add("it-IT/piscina-bimbi/index-/");
            OldList.Add("it-IT/beach-volley-e-racchettoni/index-/");
            OldList.Add("it-IT/listino-prezzi-2015/index-/");
            OldList.Add("it-IT/formule-risparmio/index-/");
            OldList.Add("it-IT/event-detail/");
            OldList.Add("it-IT/event-allnews/");
            OldList.Add("it-IT/index/");
            OldList.Add("it-IT/dove-siamo/index-/");
            OldList.Add("it-IT/come-raggiungerci/index-/");
            OldList.Add("it-IT/le-strutture/index-/");
            OldList.Add("it-IT/");
            OldList.Add("public/upl_images/epweb3/informativa-completa-CLIENTI-Fotografie.pdf");
            OldList.Add("public/upl_images/epweb3/informativa-completa-CLIENTI.pdf");
            OldList.Add("public/upl_images/epweb3/informativa-completa-FORNITORI.pdf");

            OldList = OldList.ConvertAll(d => d.ToUpper());

            //redirect old pages
            int pos = CallingUrl.IndexOf("aspxerrorpath");

            string OldUr = CallingUrl.Substring(pos + 15).ToUpper();


            //Dictionary<string, string> Dict = new Dictionary<string, string>();
            //Dict.Add("IT-IT/INDEX-INDEX/", "/Home/Index");

            //string Host = "localhost:53150";


            //for (int i=0; i < Dict.Count; i++)
            //{

            //    if (Dict.ContainsKey(Oldurl))
            //    {
            //        string value = Dict[Oldurl];
            //        return Redirect(Host + "value");
            //    }

            //}



            if (OldUr == OldList[0])
            {
                return RedirectToAction("index", "home");
            }

            if (OldUr == OldList[1] || OldUr == OldList[2])
            {
                return RedirectToAction("contatti", "home");
            }

            if (OldUr == OldList[3])
            {
                return Redirect("/content/documents/mappa-acquajoss2020.pdf");
            }

            if (OldUr == OldList[4] || OldUr == OldList[5] || OldUr == OldList[6] || OldUr == OldList[7] || OldUr == OldList[8])
            {
                return RedirectToAction("ilparco", "home");
            }

            if (OldUr == OldList[9] || OldUr == OldList[10] || OldUr == OldList[11] || OldUr == OldList[12] || OldUr == OldList[13] || OldUr == OldList[14])
            {
                return RedirectToAction("ilparco", "home");
            }

            if (OldUr == OldList[15] || OldUr == OldList[16])
            {
                return RedirectToAction("prezzidinamici", "acquisto");
            }

            if (OldUr == OldList[17] || OldUr == OldList[18] || OldUr == OldList[19] || OldUr == OldList[20] || OldUr == OldList[21])
            {
                return RedirectToAction("index", "home");
            }

            if (OldUr == OldList[22] )
            {
                return RedirectToAction("ilparco", "home");
            }


            if (OldUr == OldList[23])
            {
                return RedirectToAction("index", "home");
            }


            Response.StatusCode = 404;
            //ViewBag.footer = "relative";
            return View();
        }
    }
}