﻿using Core;
using Core.Db;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Payments.Controllers
{
    public class AcquistoController : Controller
    {
        //start date in web.config for acquajoss
        public string anno = Impostazioni.DataOggi().Substring(6, 4);
        public string mese = Impostazioni.DataOggi().Substring(3, 2);
        public string giorno = Impostazioni.DataOggi().Substring(0, 2);


        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            string color = "red";
            ViewBag.ParkOpen = Repository.GetParkOpen(ref color);
            ViewBag.ParkColor = color;
        }


        private ShoppingState TrovaShopping()
        {
            ShoppingList List = new ShoppingList();

            if (Session["Lista"] != null)
            {
                List = (ShoppingList)Session["Lista"];
            }
            else
            {
                if (Session["FromShopping"] != null)
                {
                    if ((bool)Session["FromShopping"] == true)
                    {
                        //rebuild by cookies
                        string Day = "";

                        if (Impostazioni.EsisteCookie("Day") != "")
                        {
                            Day = Request.Cookies["Day"].Value;
                        }

                        if (Day != "")
                        {
                            List = Impostazioni.RebuildShopping(Day);
                        }

                    }
                }

            }


            return Impostazioni.ConvertToState(List);

        }

        private ShoppingStateHotel TrovaShoppingHotel()
        {
            ShoppingListHotel List = new ShoppingListHotel();

            if (Session["ListaHotel"] != null)
            {
                List = (ShoppingListHotel)Session["ListaHotel"];
            }
            else
            {
                if (Session["FromShopping"] != null)
                {
                    if ((bool)Session["FromShopping"] == true)
                    {
                        //rebuild by cookies
                        string DayArrive = "";

                        if (Request.Cookies["DayArrive"] != null)
                        {
                            DayArrive = Request.Cookies["DayArrive"].Value;
                        }

                        if (DayArrive != "")
                        {
                            List = Impostazioni.RebuildShoppingHotel();
                        }

                    }
                }

            }


            return Impostazioni.ConvertToStateHotel(List);

        }

        private ShoppingList TrovaShoppingState()
        {
            ShoppingList List = new ShoppingList();

            if (Session["Lista"] != null)
            {
                List = (ShoppingList)Session["Lista"];
            }
            else
            {
                if (Session["FromShopping"] != null)
                {
                    if ((bool)Session["FromShopping"] == true)
                    {
                        //rebuild by cookies
                        string Day = "";

                        if (Impostazioni.EsisteCookie("Day") != "")
                        {
                            Day = Request.Cookies["Day"].Value;
                        }

                        if (Day != "")
                        {
                            List = Impostazioni.RebuildShopping(Day);
                        }

                    }
                }

            }

            return List;
        }

        private Shops GetShops()
        {
            Shops Shop = Repository.GetShops();
          
            return Shop;
        }

        public void SendViewBagOptions()
        {
            ViewBag.PrezzoAssicurazione = Impostazioni.PrezzoAssicurazione;
            ViewBag.PrezzoNavetta = "0";

            //family pack
            ViewBag.FamilyPack = "";
            string fam = Session["FamilyPack"].ToString();
            Product Ticket = Impostazioni.ViewBagFamily(fam);

            if (Ticket != null)
            {
                ViewBag.FamilyTitle = Ticket.Title;
                ViewBag.FamilyDescription = Ticket.Description;
                ViewBag.FamilyPack = fam.ToString();
            }

            if (Session["Lista"] != null)
            {
                ShoppingList Shopping = (ShoppingList)Session["Lista"];
                //ViewBag.DiscountCode = Shopping.DiscountCode;
            }

            ViewBag.Dicitura = InfoShopCart.Dicitura;
            ViewBag.shoptitle = InfoShopCart.shoptitle;
            ViewBag.shopdescription = InfoShopCart.shopdescription;
            ViewBag.shopcomment = InfoShopCart.shopcomment;
            ViewBag.Prodotto = InfoShopCart.Prodotto;
            ViewBag.Day = InfoShopCart.Day;
            ViewBag.Scegli = InfoShopCart.Scegli;
            ViewBag.TotPrice = InfoShopCart.TotPrice;
            ViewBag.Options = InfoShopCart.Options;
            ViewBag.TipoOperatore = Impostazioni.TipoOperatore();
            ViewBag.IdOperatore = Impostazioni.IdOperatore();

            ViewBag.Step1 = Impostazioni.Steps[0].Title;
            ViewBag.Step1Des = Impostazioni.Steps[0].Description;

            ViewBag.Step2 = Impostazioni.Steps[1].Title;
            ViewBag.Step2Des = Impostazioni.Steps[1].Description;

            ViewBag.Step3 = Impostazioni.Steps[2].Title;
            ViewBag.Step3Des = Impostazioni.Steps[2].Description;
        }



        //ACTIONRESULTS
        public ActionResult Listener()
        {
            string a = Request.QueryString["a"];
            string b = Request.QueryString["b"];

            Log.Write("Banca sella server response to Listener:" + a + " b:" + b);

            return null;
        }


        public ActionResult Biglietto(string Scelta)
        {
            if (String.IsNullOrEmpty(Scelta)) { return RedirectToAction("Index", "Home"); }

            //if (Repository.FromDiscount(Scelta) == true)
            //{
            //    Scelta = "discount";
            //}

            Scelta = Repository.Redirects(Scelta);

            Session["Lista"] = null;
            Session["ListaHotel"] = null;
            Session["ListaAbbonamenti"] = null;
            ShoppingState LstView = OttieniBiglietto(Scelta);

            if (LstView == null)
            {
                //no longer available
                return RedirectToAction("Index", "Home");
            }


            if (Session["FamilyPack"] != null)
            {
                return RedirectToAction("Packs");
            }

            ViewBag.Scelta = Scelta;

            //covid info
            ViewBag.InfoCovid = Repository.GetInfoCovid();

            //ticket info
            string ReduceInfo = "";
            ViewBag.FullInfo = Repository.GetFullInfo(Scelta, ref ReduceInfo);
            ViewBag.ReduceInfo = ReduceInfo;

            return View(LstView);
        }

        public ActionResult Packs(string scelta)
        {
            //in case of section not active in production:
            if (Impostazioni.SiteSettings.FamPacks == false && GlobalSettings.EnProduccion == true)
            {
                return RedirectToAction("Index", "Home");
            }


            //family pack
            if (String.IsNullOrEmpty(scelta)) { return RedirectToAction("Index", "Home"); }


            string Fam = scelta;

            //validity
            string Dis = Repository.GetDiscountCodeByProduct(Fam);

            PromotionDeals Pmd = Repository.GetPromotionDeals(Dis);

            if (Pmd.PromotionDeal_DateStart == null)
            {
                //no longer avilable
                return RedirectToAction("Index", "Home");
            }

            Impostazioni.DeleteAllCookies();
            Session["Lista"] = null;
            Session["ListaHotel"] = null;
            Session["ListaAbbonamenti"] = null;

            Session["FamilyPack"] = Fam;

            ViewBag.shopdescription = "PACCHETTO FAMIGLIA";
            ViewBag.FamilyPack = Fam;
            ViewBag.Discount = "";

            Product Family = Repository.GetFamilyPack(Fam);
            ViewBag.FamilyTitle = Family.Title;
            ViewBag.FamilyDescription = TMasterClass.HtmlToPlainText(Family.Description);
            ViewBag.FamilyNote = Family.Note;
            ViewBag.FamilyQuantity = Family.Quantity;

            ViewBag.DynBasePrice = Family.BasePrice;

            //ShoppingState LstView = TrovaShopping();
            ShoppingState LstView = Impostazioni.ConvertToState(new ShoppingList());

            ViewBag.Price = "0";
            if (LstView.TckPrices.Count() > 0)
            {
                ViewBag.Price = LstView.TckPrices[0];
            }

            //ViewBag.DataOggi = Repository.ManageDataOggi(giorno, mese, anno);

            //ViewBag.FixedMonth = "0";
            //string FixedDate = "";
            //bool Promotion = false;
            //ViewBag.Familycal = Repository.ConvertFamilyCal(Convert.ToInt32(anno), Convert.ToInt32(mese), Fam, ref FixedDate, ref Promotion);


            string DtOggi = Repository.ManageDataOggi(giorno, mese, anno);
            ViewBag.DataOggi = DtOggi;
            string aa = DtOggi.Substring(6, 4);
            string mm = DtOggi.Substring(3, 2);

            ViewBag.FixedMonth = "0";
            string FixedDate = "";
            bool Promotion = false;
            ViewBag.Familycal = Repository.ConvertFamilyCal(Convert.ToInt32(aa), Convert.ToInt32(mm), Fam, ref FixedDate, ref Promotion, true);

            if (FixedDate != "")
            {
                DateTime dd = Impostazioni.ConvertiDataDateFormatTwo(FixedDate);
                if (dd < Impostazioni.DataOggiDt())
                {
                    //no longer avilable
                    return RedirectToAction("Index", "Home");
                }

                LstView.DayNow = FixedDate;             
            }

            ViewBag.FixedDate = FixedDate;

            if (Promotion == true)
            {
                ViewBag.FixedMonth = "1";
            }

            ViewBag.FakeUser = Impostazioni.FakeUser;

            //important
            DateTime date = DateTime.Now;
            ViewBag.OraOggi = Impostazioni.Adatta(date.Hour.ToString()) + Impostazioni.Adatta(date.Minute.ToString());

            if (string.IsNullOrEmpty(LstView.DayNow))
            {
                LstView.DayNow = Impostazioni.DataOggi();
            }

            return View(LstView);
        }


        private ShoppingState OttieniBiglietto(string Scelta)
        {
            Log.Write("AcquistoController OttieniBiglietto session:" + Session.SessionID);

            ViewBag.DataOggi = Impostazioni.Adatta(giorno) + "/" + Impostazioni.Adatta(mese) + "/" + anno;




            int Option = 1; //ticket

            string Dis;

            //from page promozioni 


            if (String.IsNullOrEmpty(Scelta) == false)
            {
                Impostazioni.DeleteAllCookies();
                Log.Write("AcquistoController OttieniBiglietto scelta: " + Scelta + " session:" + Session.SessionID + " ClearShopSessions");
                Impostazioni.ClearShopSessions();


                Dis = TypeDiscount(Scelta);
                Option = GetPromoscelta(Scelta);
            }
            else
            {

                Dis = ManageDiscount();

                if (Dis == "")
                {
                    Option = 1;
                }
                else
                {
                    string Type = Repository.GetDiscountAndEvent(Dis).Trim();
                    if (Type == "admission") { Option = 3; }
                    if (Type == "promotion") { Option = 4; }
                }

            }


            if (Option == 1)
            {
                //biglietto
                ViewBag.shopdescription = "ACQUISTA IL BIGLIETTO";
                ViewBag.FamilyPack = "";
                ViewBag.Discount = "";
            }

            PromotionDeals Pmd = new PromotionDeals();
            if (Option == 3)
            {
                //discount code
                Pmd = Repository.GetPromotionDeals("CODSCONTO");
                ViewBag.shopdescription = "CODICE SCONTO";
                ViewBag.EventDateStart = "";
                ViewBag.EventDateEnd = "";

                if (Session["Discode"] != null)
                {
                    ViewBag.DiscountCode = Session["Discode"].ToString();
                }
                else
                {
                    ViewBag.DiscountCode = Dis;
                }
            }

            if (Option == 4)
            {
                //promotion event
                Pmd = Repository.GetPromotionDeals(Dis);

                if (Pmd.PromotionDeal_DateStart == null)
                {
                    //return RedirectToAction("Biglietto", "Acquisto");
                    return null;
                }

                ViewBag.shopdescription = "PROMOZIONE";
                ViewBag.EventDateStart = Pmd.PromotionDeal_DateStart.Substring(0, 10);
                ViewBag.EventDateEnd = Pmd.PromotionDeal_DateEnd.Substring(0, 10);
                ViewBag.DiscountCode = Dis;
            }

            if (Option == 3 || Option == 4)
            {

                ViewBag.PromoInfo = Pmd.PromotionDeal_Description;

                if (Pmd.PromotionImage_Url != null)
                {
                    ViewBag.DiscountImage = Repository.VirtualDirImages(Pmd.PromotionImage_Url);
                }

                ViewBag.DiscountTitle = Pmd.PromotionDeal_Title;
                ViewBag.DiscountInfo = Repository.GetDiscountInfo(Dis);

                ViewBag.FamilyPack = "";
                ViewBag.Discount = "true";
            }

            ViewBag.Option = Option;

            ShoppingState LstView = TrovaShopping();

            List<DynamicPrice> ListaPrezzi;

            //manage calendar
            if (Session["ListaPrezzi"] == null)
            {
                if (Option == 4)
                {
                    string mm = Pmd.PromotionDeal_DateStart.Substring(3, 2);
                    string aa = Pmd.PromotionDeal_DateStart.Substring(6, 4);

                    int Datast = Impostazioni.ConvertiDataNumero(Pmd.PromotionDeal_DateStart);
                    int Datogg = Impostazioni.ConvertiDataNumero(Impostazioni.DataOggi());


                    if (Datogg < Datast)
                    {
                        // the promotion start is after the current date
                        ListaPrezzi = Impostazioni.ImpostaListaPrezzi(aa, mm, "01", true);
                    }
                    else
                    {
                        //the current date is after the promotion start
                        ListaPrezzi = Impostazioni.ImpostaListaPrezzi(anno, mese, "01", true);
                    }

                }
                else
                {
                    ListaPrezzi = Impostazioni.ImpostaListaPrezzi(anno, mese, "01");
                }


                if (!string.IsNullOrEmpty(LstView.DayNow))
                {
                    string MeseStart = LstView.DayNow.Substring(3, 2);
                    string anno = LstView.DayNow.Substring(6, 4);
                    //string MeseStart = Impostazioni.ListaPrezzi[0].Data.Substring(3,2);

                    if (Convert.ToInt32(MeseStart) != Convert.ToInt32(mese))
                    {
                        ListaPrezzi = Impostazioni.ImpostaListaPrezzi(anno, MeseStart, "01");
                    }

                }

            }
            else
            {

                ListaPrezzi = (List<DynamicPrice>)Session["ListaPrezzi"];

                //if previous month 
                if (!string.IsNullOrEmpty(LstView.DayNow))
                {
                    string MeseStart = LstView.DayNow.Substring(3, 2);
                    string anno = LstView.DayNow.Substring(6, 4);
                    //string MeseStart = Impostazioni.ListaPrezzi[0].Data.Substring(3,2);

                    if (Convert.ToInt32(MeseStart) != Convert.ToInt32(mese))
                    {
                        ListaPrezzi = Impostazioni.ImpostaListaPrezzi(anno, MeseStart, "01");
                    }

                }

            }

            Session["ListaPrezzi"] = ListaPrezzi;

            ViewBag.cal = Repository.ConvertCal();

            ///////////////////////////
            //no reduced price
            ViewBag.NoReduce = "0";

            if (Dis == "PARKINGABB")
            {
                ViewBag.NoReduce = "1";
            }



            ///////////////////////////
            //acquisto operatore
            ViewBagOperatore();

            return LstView;
        }


        public ActionResult Gruppi()
        {
            //in case of section not active in production:
            if (Impostazioni.SiteSettings.Groups == false && GlobalSettings.EnProduccion == true)
            {
                return RedirectToAction("Index", "Home");
            }


            //just for acquajoss
            Repository.UpdateStartCalendar(ref anno, ref mese);

            ViewBag.DataOggi = Repository.ManageDataOggi(giorno, mese, anno);

            Session["FamilyPack"] = null;
            Session["FromShopping"] = false;

            ShoppingState LstView = TrovaShopping();

            List<TimeEntrColors> ListaColori = Repository.GetColorList();

            List<string> Colori = ListaColori.Select(x => x.Color).ToList();
            List<string> ColorDes = ListaColori.Select(x => x.Description).ToList();

            ViewBag.Colori = Colori;
            ViewBag.ColorDes = ColorDes;

            //take the prices too
            string IdGroup = Repository.GetProdIdGroup();
            ViewBag.cal = Repository.ConvertCalGroup(IdGroup);

            ViewBag.FakeUser = Impostazioni.FakeUser;

            Product prod = Repository.GetTicketFixed(IdGroup, Impostazioni.Conexion);

            ViewBag.TckId = prod.Id;
            ViewBag.TckTit = HttpUtility.HtmlDecode(prod.Title).Replace("\n", "");
            ViewBag.TckDes = HttpUtility.HtmlDecode(prod.Description).Replace("\n", "");

            //important
            ViewBag.DataOggi = Repository.ManageDataOggi(giorno, mese, anno);

            //important
            DateTime date = DateTime.Now;
            ViewBag.OraOggi = Impostazioni.Adatta(date.Hour.ToString()) + Impostazioni.Adatta(date.Minute.ToString());


            return View(LstView);
        }


        public ActionResult PrezziDinamici(string Scelta, string Day)
        {
            //in case of section not active in production:
            if (Impostazioni.SiteSettings.Tickets == false && GlobalSettings.EnProduccion == true)
            {
                return RedirectToAction("Index", "Home");
            }

            Session["FromShopping"] = false;

            ShoppingState LstView = OttieniBiglietto("ticket");

            ViewBagColorDes();

            if (ViewBag.cal == null)
            {
                ViewBag.cal = Repository.ConvertCal();
            }

            CalendarState Cal = ViewBag.cal;


            //dynamic ticket in promotion for a specific day 
            if (Scelta != "")
            {
                PromotionDeals Pmd = Repository.GetPromotionDeals(Scelta);

                ViewBag.EventDate = Pmd.PromotionDeal_DateStart;
            }


            List<DynamicPrice> ListaPrezzi = (List<DynamicPrice>)Session["ListaPrezzi"];

            string DataAv = Repository.ManageDataAv(ListaPrezzi, ViewBag.DataOggi, ref Cal);

            //come from specific day
            if (!string.IsNullOrEmpty(Day))
            {
                Repository.ManageSpecificDay(Day, ref DataAv, ref Cal);
            }

            ViewBag.cal = Cal;
            ViewBag.DataAv = DataAv;

            ViewBag.SoldOut = Repository.CheckSoldOut(DataAv);

            ViewBag.StartCalendarDate = Impostazioni.StartCalendarDate;

            InfoShopCart.shopcomment = Impostazioni.ManageSameDayComment();

            ViewBag.shopcomment = InfoShopCart.shopcomment;

            //same day hour limit important
            ManageSameDayHourLimit();

            //covid info
            ViewBag.InfoCovid = Repository.GetInfoCovid();

            return View(LstView);
        }

        private void ManageSameDayHourLimit()
        {
            DateTime Today = DateTime.Now;
            string OraOggiStr = Impostazioni.Adatta(Today.Hour.ToString()) + Impostazioni.Adatta(Today.Minute.ToString());

            ViewBag.OraOggi = OraOggiStr;
            ViewBag.HourLimit = Impostazioni.HourLimit;
            ViewBag.SameDay = Impostazioni.SameDay;
        }


        private void ViewBagColorDes()
        {
            List<TimeEntrColors> ListaColori = Repository.GetColorList();
            List<string> Colori = ListaColori.Select(x => x.Color).ToList();
            List<string> ColorDes = ListaColori.Select(x => x.Description).ToList();
            ViewBag.Colori = Colori;
            ColorDes = Impostazioni.MatchColorDes(ColorDes, ref Colori);
            ViewBag.ColorDes = ColorDes;
        }

        private void ManageSpecificDay(string Day, ref string DataAv)
        {
            try
            {
                string Anno = Day.Substring(0, 4);
                string Mese = Day.Substring(4, 2);

                Day = Day.Substring(6, 2) + "/" + Mese + "/" + Anno;

                DateTime dd = Impostazioni.ConvertiDataDateFormatTwo(Day);

                if (Impostazioni.ConvertiDataNumero(Day) >= Impostazioni.ConvertiDataNumero(DataAv))
                {
                    DataAv = Day;

                    Session["ListaPrezzi"] = Repository.ReadDynamicPrices(Anno, Mese);
                    ViewBag.cal = Repository.ConvertCal();
                }

            }
            catch
            {
                //wrong date
            }
        }


        private int GetPromoscelta(string scelta)
        {
            Repository Rep = new Repository();

            if (scelta == "ticket")
            {
                Session["FamilyPack"] = null;
                return 1;
            }

            
            //discount
            if (scelta == "discount")
            {
                Session["FamilyPack"] = null;
                return 3;
            }

            //event promotion
            Session["FamilyPack"] = null;
            return 4;

        }

        private void ViewBagOperatore()
        {
            //acquisto operatore o verifica cliente
            ViewBag.VerificaCliente = Session["VerificaCliente"] == null ? "" : Session["VerificaCliente"].ToString();

            ViewBag.TipoOperatore = Impostazioni.TipoOperatore();
            ViewBag.IdOperatore = Impostazioni.IdOperatore();
            ViewBag.AcquistoId = Session["ReservationId"] == null ? "" : Session["ReservationId"].ToString();
            ViewBag.Svincolato = Session["Svincolato"] == null ? "" : Session["Svincolato"].ToString();
        }


        private string TypeDiscount(string scelta)
        {
            string Dis = "";

            if (scelta == "ticket")
            {
                return "";
            }

            if (scelta == "discount")
            {
                return "";
            }

            Dis = scelta;

            return Dis;

        }

        private string ManageDiscount()
        {
            string Dis = "";


            if (Session["Lista"] != null)
            {
                ShoppingList List = (ShoppingList)Session["Lista"];

                Dis = List.DiscountCode;
            }

            return Dis;
        }

        private string Managescelta(string scelta, ShoppingList List_in, ShoppingState LstView_in, out ShoppingList List, out ShoppingState LstView)
        {
            Repository Imp = new Repository();

            string Fam = "";

            List = List_in;
            LstView = LstView_in;


            if (scelta != "ticket" && scelta != null)
            {
                //familypack is a number
                int n = 0;
                int.TryParse(scelta, out n);

                if (n > 0)
                {
                    if (Repository.GetFamilyPack(n.ToString()) != null)
                    {
                        Fam = scelta;
                        Session["FamilyPack"] = Fam;
                        return Fam;
                    }

                }


                Session.Remove("FamilyPack");

                if (scelta == "discount")
                {
                    string Day = Impostazioni.DataOggi();
                    Impostazioni.ImpostaListaPrezzi(Day.Substring(6, 4), Day.Substring(3, 2), Day.Substring(0, 2));
                    Session["ListaPrezzi"] = Impostazioni.ImpostaListaPrezzi(Day.Substring(6, 4), Day.Substring(3, 2), Day.Substring(0, 2));

                }

                //promotion events


                return "";
            }


            if (scelta != "ticket" || scelta == null)
            {
                if (Session["FamilyPack"] != null)
                {
                    Fam = Session["FamilyPack"].ToString();

                    //pacchetto diverso dal precedente
                    //if (scelta != Fam && scelta != null)
                    //{
                    //    Session.Remove("Lista");
                    //    Fam = scelta;
                    //    Session["FamilyPack"] = Fam;

                    //    List = Imp.BuildFamilyPackFull(Fam);

                    //    LstView = Impostazioni.ConvertToState(List);

                    //}
                    //else
                    //{
                    //    Session["Lista"] = null;
                    //    LstView = TrovaShopping();
                    //}
                }
                else
                {
                    if (string.IsNullOrEmpty(scelta) == false)
                    {
                        Session.Remove("FamilyPack");
                        Session.Remove("ListaPrezzi");
                        Fam = scelta;
                        Session["FamilyPack"] = Fam;

                        List = Imp.BuildFamilyPackFull(Fam);
                        Session["Lista"] = List;
                        LstView = Impostazioni.ConvertToState(List);
                    }
                }
            }
            else
            {
                if (Session["FamilyPack"] != null)
                {
                    Session.Remove("FamilyPack");

                    Session.Remove("Lista");

                    HttpCookie aCookie = new HttpCookie("Day");
                    aCookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(aCookie);

                    HttpCookie bCookie = new HttpCookie("ticket");
                    bCookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(bCookie);
                    //cookie delete is not immediate

                    Impostazioni.NoCookie = true;
                }

            }

            return Fam;
        }



        public ActionResult GetShopping(ShoppingList Shopping)
        {
            string Day = Shopping.Day;
            Session["ListaPrezzi"] = Impostazioni.ImpostaListaPrezzi(Day.Substring(6, 4), Day.Substring(3, 2), Day.Substring(0, 2));

            Session["ListaHotel"] = null;
            Session["ListaAbbonamenti"] = null;
            Session["old_date"] = "";
            if (Session["Lista"] != null)
            {
                ShoppingList Sh = (ShoppingList)Session["Lista"];

                Session["old_date"] = Sh.Day;
            }
            else
            {
                Session["old_date"] = Shopping.Day;
            }

            Session["Lista"] = Shopping;
            return Json("");
        }


        public ActionResult GetShoppingHotel(ShoppingListHotel Shopping)
        {
            Session["ListaPrezzi"] = null;
            Session["ListaAbbonamenti"] = null;
            Session["Lista"] = null;
            Session["ListaHotel"] = Shopping;
            return Json("");
        }

        public ActionResult GetShoppingSubscription(ShoppingListSubscription Shopping)
        {

            Session["ListaHotel"] = null;
            Session["Lista"] = null;
            Session["ListaAbbonamenti"] = Shopping;
            return Json("");
        }

        public ActionResult BuyMenu()
        {
            if (Session["FamilyPack"] != null)
            {
                return RedirectToAction("BuyAnimal");
            }


            Shops Shop = new Shops();

            //acquisto operatore
            ViewBagOperatore();


           
            //if (ViewBag.AcquistoId != "" && Session["Lista"] == null)
            if (ViewBag.AcquistoId != "" && Impostazioni.IdOperatore() != 0)
            {
                Shop = Impostazioni.GetAcquistoOp(ViewBag.AcquistoId);
            }
            else
            {
                Shop = GetShops();
            }

            if (Shop.ShoppingState != null)
            {
                string Day = Shop.ShoppingState.DayNow;

                if (String.IsNullOrEmpty(Shop.ShoppingState.DiscountCode))
                {

                    Session["Discode"] = null;
                }
                else
                {
                    string Discode = Shop.ShoppingState.DiscountCode;

                    List<DisPrice> Ls = Repository.GetDiscountPrices(Discode, Day);

                    if (Ls[0].ProductType.Trim() == "admission")
                    {
                        Session["Discode"] = Discode;
                    }
                    else
                    {
                        Session["Discode"] = null;
                    }
                }
            }


            Impostazioni.FillShopInfo(Shop);


            SendViewBagOptions();

            return View(Shop);
        }



        public ActionResult BuyActivity()
        {
            Shops Shop = GetShops();


            Impostazioni.FillShopInfo(Shop);

            //acquisto operatore
            ViewBagOperatore();

            SendViewBagOptions();

            //just for activity:
            int Npay = 0;
            string Day = "";
            List<string> Activities = Repository.GetActInfo(Shop, ref Npay, ref Day);

            List<string> ProductAllotments = new List<string>();
            foreach (string Act in Activities)
            {
                string Lista = Repository.GetProductNoScheduleAllotment(Act, Day);
                ProductAllotments.Add(Lista);
            }

            ViewBag.Npag = Npay;
            ViewBag.ProductAllotments = ProductAllotments;

            return View(Shop);
        }

        public ActionResult BuyAnimal()
        {
            Shops Shop = GetShops();


            //acquisto operatore
            ViewBagOperatore();

            if (ViewBag.AcquistoId != "" && Impostazioni.IdOperatore() != 0)
            {
                Shop = Impostazioni.GetAcquistoOp(ViewBag.AcquistoId);
            }

            Impostazioni.FillShopInfo(Shop);

            SendViewBagOptions();

            List<string> SubAnimalMaxQuantity = new List<string>();
            List<int> AniPosition = Repository.GetAniInfo(Shop, ref SubAnimalMaxQuantity);

            ViewBag.AniPosition = AniPosition;
            ViewBag.SubAnimalMaxQuantity = SubAnimalMaxQuantity;

            return View(Shop);
        }

        public ActionResult Hotel()
        {

            //in case of section not active in production:
            if (Impostazioni.SiteSettings.Hotels == false && GlobalSettings.EnProduccion == true)
            {
                return RedirectToAction("Index", "Home");
            }

            Session["ListaHotel"] = null;
            Session["Lista"] = null;
            Session["ListaAbbonamenti"] = null;
            Session["FamilyPack"] = null;

            ViewBag.DataOggi = Impostazioni.DataOggi();

            ShoppingListHotel List = new ShoppingListHotel();


            ViewBag.HotelInfo = Repository.GetHotelInfo().Description;
            ViewBag.Hotelcal = Repository.ConvertHotelCal(Convert.ToInt32(anno), Convert.ToInt32(mese));
            ViewBag.BaseHotelPrice = Impostazioni.BaseHotelPrice;


            //acquisto operatore
            ViewBag.TipoOperatore = Impostazioni.TipoOperatore();
            ViewBag.IdOperatore = Impostazioni.IdOperatore();
            ViewBag.AcquistoId = Session["ReservationId"] == null ? "" : Session["ReservationId"].ToString();


            ShoppingStateHotel LstView = Impostazioni.ConvertToStateHotel(List);
            ViewBag.PrezzoAssicurazione = Impostazioni.PrezzoAssicurazione;
            ViewBag.PrezzoNavetta = "0";


            string DesAssicurazione = Repository.GetDescriptionInfo(Repository.GetProductByProductKey("CHANGEDATE"));
            DesAssicurazione = Impostazioni.CleanHtml(DesAssicurazione);
            ViewBag.DesAssicurazione = DesAssicurazione;

            List<string> HotelCities = Repository.GetHotelCities();
            ViewBag.HotelCities = HotelCities;

            ViewBag.FakeUser = Impostazioni.FakeUser;

            ViewBagColorDes();

            //covid info
            ViewBag.InfoCovid = Repository.GetInfoCovid();

            return View(LstView);
        }

        public ActionResult Abbonamenti(string Scelta)
        {
            //in case of section not active in production:
            if (Impostazioni.SiteSettings.Subscriptions == false && GlobalSettings.EnProduccion == true)
            {
                return RedirectToAction("Index", "Home");
            }


            Session["FamilyPack"] = null;
            Session["Subold"] = null;
            ShoppingListSubscription List = new ShoppingListSubscription();

            //reserved offer for old customers
            if (!String.IsNullOrEmpty(Scelta))
            {
                //verify the offer
                if (Repository.VerifyOfferta(Scelta).Count > 0)
                {
                    Session["ListaAbbonamenti"] = null;
                    Session["Subold"] = Scelta;
                }         
                else
                {                                   
                    if (Session["ListaAbbonamenti"] != null)
                    {
                        List = (ShoppingListSubscription)Session["ListaAbbonamenti"];
                    }
                    else
                    {
                        if (Session["FromShopping"] != null)
                        {
                            if ((bool)Session["FromShopping"] == true)
                            {
                                //rebuild by cookies
                                if (Request.Cookies["SubscriptionTotPrice"] != null)
                                {
                                    List = Impostazioni.RebuildShoppingSubscriptions();
                                }
                            }
                        }
                    }

                }
                                           
            }
        
            Shops Shop = new Shops();

            ShoppingStateSubscriptions LstView = new ShoppingStateSubscriptions();

            //acquisto operatore
            ViewBag.VerificaCliente = Session["VerificaCliente"] == null ? "" : Session["VerificaCliente"].ToString();

            ViewBag.TipoOperatore = Impostazioni.TipoOperatore();
            ViewBag.IdOperatore = Impostazioni.IdOperatore();
            ViewBag.AcquistoId = Session["ReservationId"] == null ? "" : Session["ReservationId"].ToString();
            //if (ViewBag.AcquistoId != "")
            if (ViewBag.AcquistoId != "" && Impostazioni.IdOperatore() != 0)
            {
                Shop = Impostazioni.GetAcquistoOp(ViewBag.AcquistoId);
                Impostazioni.FillShopInfo(Shop);
                LstView = Shop.ShoppingStateSubscriptions;
                return View(LstView);
            }
            else
            {
                Shop = GetShops();
            }

            Impostazioni.FillShopInfo(Shop);
            LstView = Shop.ShoppingStateSubscriptions;

            
            return View(LstView);
        }


        public ActionResult BacktoBuy(ShoppingList Shopping)
        {
            Session["ListaHotel"] = null;
            Session["ListaAbbonamenti"] = null;
            Session["Lista"] = Shopping;

            ViewBag.cal = Repository.ConvertCal();

            if (Session["FamilyPack"] != null)
            {
                return Json(Url.Action("Packs", "Acquisto", new { scelta = Session["FamilyPack"].ToString() }));
            }

            if (String.IsNullOrEmpty(Shopping.DiscountCode))
            {
                return Json(Url.Action("PrezziDinamici", "Acquisto"));
            }

            string Discode = Shopping.DiscountCode;

            if (Session["Discode"] != null)
            {
                Discode = "discount";
            }

            return Json(Url.Action("Biglietto", "Acquisto", new { scelta = Discode }));
        }

        public ActionResult BacktoMenu(ShoppingList Shopping)
        {
            Session["ListaHotel"] = null;
            Session["ListaAbbonamenti"] = null;
            Session["Lista"] = Shopping;

            return Json(Url.Action("BuyMenu", "Acquisto"));
        }

        public ActionResult BacktoAnimal(ShoppingList Shopping)
        {
            Session["ListaHotel"] = null;
            Session["ListaAbbonamenti"] = null;
            Session["Lista"] = Shopping;

            return Json(Url.Action("BuyAnimal", "Acquisto"));
        }

        public ActionResult BacktoActivity(ShoppingList Shopping)
        {
            Session["Lista"] = Shopping;
            Session["ListaHotel"] = null;
            Session["ListaAbbonamenti"] = null;

            return Json(Url.Action("BuyActivity", "Acquisto"));
        }

        public ActionResult BacktoSubscriptions(ShoppingListSubscription Shopping)
        {
            Session["ListaHotel"] = null;
            Session["ListaAbbonamenti"] = Shopping;
            Session["Lista"] = null;

            return Json(Url.Action("Abbonamenti", "Acquisto"));
        }

       
        public ActionResult BacktoBuyHotel(ShoppingListHotel Shopping)
        {
            Session["ListaHotel"] = Shopping;
            Session["ListaAbbonamenti"] = null;
            Session["Lista"] = null;

            return Json(Url.Action("Hotel", "Acquisto"));
        }

        public ActionResult backToHotel()
        {
            //Session["Lista"] = null;
            //Session["ListaHotel"] = Shopping;           
            //Session["ListaAbbonamenti"] = null;

            return Json(Url.Action("Hotel", "Acquisto"));
        }

        public ActionResult ShoppingCart(ShoppingList Shopping)
        {
            //ZoHo campaign
            if (Shopping.Buyer.MailingList == "Y")
            {
                ZoHoCampaign.ListSubscribe(Shopping.Buyer.Name, Shopping.Buyer.Surname, Shopping.Buyer.TelePhone, Shopping.Buyer.Zip, Shopping.Buyer.Email);
            }

            Session["ListaHotel"] = null;
            Session["ListaAbbonamenti"] = null;
            Session["Lista"] = Shopping;

            return Json(Url.Action("ShoppingCart", "ShoppingCart"));
        }

        public ActionResult ShoppingCartHotel(ShoppingListHotel Shopping)
        {
            //ZoHo campaign
            if (Shopping.Buyer.MailingList == "Y")
            {
                ZoHoCampaign.ListSubscribe(Shopping.Buyer.Name, Shopping.Buyer.Surname, Shopping.Buyer.TelePhone, Shopping.Buyer.Zip, Shopping.Buyer.Email);
            }


            Session["Lista"] = null;
            Session["ListaAbbonamenti"] = null;
            Session["ListaHotel"] = Shopping;

            return Json(Url.Action("ShoppingCart", "ShoppingCart"));
        }

        public ActionResult ShoppingCartSubscriptions(ShoppingListSubscription Shopping)
        {
            //ZoHo campaign
            if (Shopping.Buyer.MailingList == "Y")
            {
                ZoHoCampaign.ListSubscribe(Shopping.Buyer.Name, Shopping.Buyer.Surname, Shopping.Buyer.TelePhone, Shopping.Buyer.Zip, Shopping.Buyer.Email);
            }

            Session["Lista"] = null;
            Session["ListaHotel"] = null;
            Session["ListaAbbonamenti"] = Shopping;

            ViewBag.TipoOperatore = Impostazioni.TipoOperatore();
            return Json(Url.Action("ShoppingCart", "ShoppingCart"));
        }

        //JSONRESULTS
        public JsonResult GetDiscount(string DiscountCode, string Day)
        {
            if (Day == null)
            {
                Day = Impostazioni.DataOggi();
            }

            List<DisPrice> Prices = Repository.GetDiscountPrices(DiscountCode, Day);

            return Json(Prices, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetData(string Day, string FamilyPack)
        {
            ShoppingList Shopping = new ShoppingList();


            //if different day clear session
            if (Session["Lista"] != null)
            {
                Shopping = (ShoppingList)Session["Lista"];

                if (Shopping.Day == null && (string.IsNullOrEmpty(FamilyPack) == false))
                {
                    Shopping.Day = Impostazioni.DataOggi();
                }

                if (Shopping.Day != Day)
                {
                    //Session["Lista"] = null;
                    //Shopping = null;
                }
            }

            int numdata = Impostazioni.ConvertiDataNumero(Day);

            DayProducts DayProd = new DayProducts();

            DayProd = Repository.GetData(numdata, FamilyPack);

            return Json(DayProd, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetMenuPrice(Acquisto Info)
        {

            Product prod = Repository.GetAddOnPrice(Info.Codice, Info.Giorno);

            //exclude product in the package if is present itself
            List<string> Bundle = TrovaBundle();

            foreach (string el in Bundle)
            {
                if (Info.Codice == el)
                {
                    prod.Price = null;
                }
            }

            return Json(prod, JsonRequestBehavior.AllowGet);
        }

        private List<string> TrovaBundle()
        {
            List<string> Bundles = new List<string>();
            string IdProd = "";

            if (Session["Lista"] != null)
            {
                ShoppingList Shopping = new ShoppingList();
                Shopping = (ShoppingList)Session["Lista"];

                IdProd = Shopping.Tickets[0].Id;
            }

            if (IdProd != "")
            {
                Bundles = Repository.GetBundle(IdProd);
            }

            return Bundles;
        }


        [HttpPost]
        public JsonResult GetGroupData(string Day)
        {
            ShoppingList Shopping = new ShoppingList();

            //if different day clear session
            if (Session["Lista"] != null)
            {
                Shopping = (ShoppingList)Session["Lista"];

                if (Shopping.Day == null)
                {
                    Shopping.Day = Impostazioni.DataOggi();
                }

            }

            int NumData = Impostazioni.ConvertiDataNumero(Day);

            DayProducts DayProd = Repository.GetData(NumData, "");

            string Prod = Repository.GetProdIdGroup();

            //overwrite
            DayProd.Tickets = Prod.Split().ToList();

            return Json(DayProd, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetHotelData(string DayArrive, string DayDeparture, List<Room> Rooms, string City = "")
        {
            int Arrive = Impostazioni.ConvertiDataNumero(DayArrive);
            int Departure = Impostazioni.ConvertiDataNumero(DayDeparture);

            List<HotelAv> Hotels = Repository.GetHotelData(Arrive, Departure, Rooms, City);


            //extra night available:
            DateTime Dayn = Impostazioni.ConvertiDataDateFormatTwo(DayDeparture);
            string Daten = Impostazioni.ConvertiDBaData(Dayn);
            int DayAfter = Impostazioni.ConvertiDataNumero(Daten);


            List<string> NextHotels = Hotels.Select(x => x.Hotel.Id).ToList();

            //products under calendar
            string DispNavetta = Repository.GetProductsCalendar(Repository.GetProductByProductKey("NAVETTA"), DayArrive);


            int Prod = 0;
            string ExtraNight1 = Repository.GetExtraNight(1, DayArrive, ref Prod);

            bool ExtraOpen = Repository.CheckParkDayOpen(Daten);


            return Json(new { Hotels, DispNavetta, ExtraNight1, NextHotels, ExtraOpen }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDynamicPrices(string Anno, string Mese)
        {

            List<DynamicPrice> DinPrezzi = new List<DynamicPrice>();
            DinPrezzi = Repository.ReadDynamicPrices(Anno, Mese);

            return Json(DinPrezzi, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPrice(Acquisto Info)
        {
            Product prod = Repository.GetTicketPrice(Info);

            return Json(prod, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPositionAlltoment(string ProductId, string Day)
        {
            int pos = Repository.GetPositionAlltoment(ProductId, Day);

            return Json(pos, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult GetTicketFixed(string Codice)
        {
            Product prod = Repository.GetTicketFixed(Codice, Impostazioni.Conexion);

            return Json(prod, JsonRequestBehavior.AllowGet);
        }

       
        public JsonResult GetActivityPrice(Acquisto Info)
        {
            Product prod = Repository.GetAddOnPrice(Info.Codice, Info.Giorno);

            return Json(prod, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAnimalPrice(Acquisto Info)
        {
            Product prod = Repository.GetAddOnPrice(Info.Codice, Info.Giorno);

            return Json(prod, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetSubAnimalTimes(string IdAnimalFather, string Quant)
        {
            List<Product> Prod = Repository.GetSubAnimalTimes(IdAnimalFather);

            if (Prod == null)
            {
                return Json("");
            }

            return Json(Prod, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetSubscriptionData()
        {

            List<Subscription> Subscriptions = Repository.GetSubscriptions();

            return Json(Subscriptions);
        }

        public JsonResult CreaCalendario(int Anno, int Mese)
        {

            if (Repository.CheckLastDate(Anno, Mese) == 0)
            {
                //back
                Repository.BacktoPreviousMonth(ref Anno, ref Mese);
            };

            ShoppingState LstView = TrovaShopping();

            Session["ListaPrezzi"] = Repository.ReadDynamicPrices(Anno.ToString(), Mese.ToString());
            
            CalendarState Calend = Repository.ConvertCal();

            return Json(Calend);
        }


        public JsonResult CreaCalendarioGruppi(string Anno, string Mese)
        {
            int aa = Convert.ToInt32(Anno);
            int mm = Convert.ToInt32(Mese);

            if (Repository.CheckLastDate(aa, mm) == 0)
            {
                //back
                Repository.BacktoPreviousMonth(ref aa, ref mm);
            };


            List<DynamicPrice> ListaGruppiPrezzi = new DynamicPrice().ReadDbDynamicPrices(aa, mm);
            ListaGruppiPrezzi = Repository.FindPeriod(ListaGruppiPrezzi, ref Anno, ref Mese, false);


            Session["ListaGruppiPrezzi"] = ListaGruppiPrezzi;

            string IdGroup = Repository.GetProdIdGroup();
            CalendarState Calend = Repository.ConvertCalGroup(IdGroup);

            return Json(Calend);
        }




        public JsonResult CreaCalendarioHotel(int Anno, int Mese)
        {
            //no more dynamic!
            List<DynamicPrice> Elenco = new DynamicPrice().ReadDbDynamicPrices(Anno, Mese);
            List<DynamicProductPrice> ListaHotelPrezzi = Repository.CopyIntoHotelPrices(Elenco);
            Session["ListaHotelPrezzi"] = ListaHotelPrezzi;

            CalendarProductState Calend = Repository.ConvertHotelCal(Anno, Mese);

            return Json(Calend);
        }


        public JsonResult CreaCalendarioFamily(int Anno, int Mese)
        {
            string Fam = Session["FamilyPack"].ToString();

            string FixedDate = "";
            bool Promotion = false;
            CalendarProductState Calend = Repository.ConvertFamilyCal(Anno, Mese, Fam, ref FixedDate, ref Promotion , true);

            return Json(Calend);
        }


        public JsonResult TestBank()
        {
            //before the bank
            string UrlB = Impostazioni.BsEndPoint;
            if (WsGenerici.TestConnection(UrlB,true) == false){
                Log.Write("Test connessione banca fallito");
                return Json("KO");
            }

            //and then tmaster
            string UrlT = Impostazioni.TmEndPoint;
            if (WsGenerici.TestConnection(UrlT,false) == false) {
                Log.Write("Test connessione Tmaster fallito");
                return Json("KO");
            }

            return Json("OK");
        }
    }
}