﻿using Core;
using Core.Db;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace Aquafelix.Controllers
{
    public class AcquistoId
    {
        public string Acquisto_Id { get; set; }
        public string Acquisto_Cognome { get; set; }
    }

    public class AreaRiservataController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            string color = "red";
            ViewBag.ParkOpen = Repository.GetParkOpen(ref color);
            ViewBag.ParkColor = color;
        }


        // GET: AreaRiservata
        public ActionResult Logout()
        {
            //ripulisce
            Impostazioni.DeleteAllCookies();

            Session.Remove("logged");
            Session.Remove("UserType");
            Session.Remove("UserId");
            Session.Remove("AcquistoId");
            Session.Remove("BankShopId");
            Session.Remove("Svincolato");
            Session.Remove("AssTel");
            Session.Remove("VerificaCliente");

            Session.Remove("Lista");
            Session.Remove("ListaHotel");
            Session.Remove("ListaAbbonamenti");
            Session.Remove("FamilyPack");
            Session.Remove("FromShopping");
            
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }

        //verifica acquisto
        public ActionResult Verifica()
        {
            Session["VerificaCliente"] = "true";
            return View();
        }


        //login
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetHotelAlltoment(string Hotel, int Mese)
        {

            string PriceId = Repository.GetHotelHighestPrice(Hotel, Mese);
            List<HotelAllotment> Lista = Repository.GetHotelAllotment(Hotel, Mese, PriceId);

            return Json(Lista);
        }


     

        [HttpPost]
        public PartialViewResult GetHotelAllotmentReservations(string Hotel, string Mese)
        {
            int IdPark = Impostazioni.DefaultCompany;
            List<ReservationsHotelOp> Reservations = Repository.SearchReservationsByHotel(Hotel, Mese, IdPark);

            return PartialView("~/Views/AreaRiservata/Shared/_Reservations.cshtml", Reservations);
        }


        public ActionResult UpdateHotelAllotment(string Hotel, string Mese, string Giorno, string RoomType, string Quantita)
        {
            string Msg = "";
            Msg = Repository.UpdateAllotmentsByHotel(Hotel, Mese, Giorno, RoomType, Quantita);

            return Json(Msg);
        }

        public ActionResult UpdateHotelAllotmentRoom(string Hotel, string Mese, string Giorno, string RoomType, string Quantita)
        {
            string Msg = "";
            Msg = Repository.UpdateAllotmentsByHotelRoom(Hotel, Mese, Giorno, RoomType, Quantita);

            return Json(Msg);
        }

        public ActionResult Sales()
        {
            //hotel sales for the hotel operators logged
            ViewBag.TipoOperatore = Impostazioni.TipoOperatore();
            ViewBag.IdOperatore = Impostazioni.IdOperatore();

            if (ViewBag.IdOperatore == 0)
            {
                //session timeout
                return Redirect("Logout");
            }

            string Hotel = Repository.GetHotelUser(ViewBag.IdOperatore);

            if (string.IsNullOrEmpty(Hotel))
            {
                Log.Write("Errore area riservata Hotel operatore: " + ViewBag.IdOperatore + "non ha un hotel associato");
            }
            else
            {
                ViewBag.HotelId = Hotel;
                string Product_idHotel = Repository.GetProductHotelUser(ViewBag.IdOperatore);

                HotelTm Hot = Repository.GetHotel(Product_idHotel);
                ViewBag.HotelInfo = Hot.Description;
                ViewBag.HotelAdd = Hot.Address + " " + Hot.PostalCode + " " + Hot.City + " " + Hot.Province;
            }

            return View();
        }



        public ActionResult Hotel()
        {
            //hotel allotment for the hotel operators logged
            ViewBag.TipoOperatore = Impostazioni.TipoOperatore();
            ViewBag.IdOperatore = Impostazioni.IdOperatore();

            if (ViewBag.IdOperatore == 0)
            {
                //session timeout
                return Redirect("Logout");
            }


            string Hotel = Repository.GetHotelUser(ViewBag.IdOperatore);

            if (string.IsNullOrEmpty(Hotel))
            {
                Log.Write("Errore area riservata Hotel operatore: " + ViewBag.IdOperatore + "non ha un hotel associato");
            }
            else
            {
                ViewBag.HotelId = Hotel;
                string Product_idHotel = Repository.GetProductHotelUser(ViewBag.IdOperatore);

                HotelTm Hot = Repository.GetHotel(Product_idHotel);
                ViewBag.HotelInfo = Hot.Description;
                ViewBag.HotelAdd = Hot.Address + " " + Hot.PostalCode + " " + Hot.City + " " + Hot.Province;
            }

            return View();
        }

        //acquisto operatore (assistenza telefonica)
        public ActionResult AcquistoOp()
        {
            ViewBag.TipoOperatore = Impostazioni.TipoOperatore();
            ViewBag.IdOperatore = Impostazioni.IdOperatore();
            ViewBag.Scelta = 1;
            Session["Svincolato"] = null;
            return View();
        }

        //acquisto svincolato
        public ActionResult AcquistoSv()
        {
            ViewBag.TipoOperatore = Impostazioni.TipoOperatore();
            ViewBag.IdOperatore = Impostazioni.IdOperatore();
            ViewBag.Scelta = 2;
            Session["Svincolato"] = "true";
            Session["AssTel"] = null;
            return View("AcquistoOp");
        }

        public ActionResult AcquistoOpSession(string Acquisto_Id)
        {
            Log.Write("AreaRiservataController AcquistoOpSession " + Acquisto_Id + " ClearShopSessions");
            Impostazioni.ClearShopSessions();
            Session["ReservationId"] = Acquisto_Id;
            Session["BankShopId"] = Acquisto_Id;

            string Prot = Repository.GetReservationItemType(Acquisto_Id);
            int type = Impostazioni.DecodeItemtype(Prot);

            DetectFamily(Acquisto_Id);
            Impostazioni.GetAcquistoOp(Acquisto_Id);

            return Json(type);
        }

        public ActionResult AcquistoOpStart(AcquistoId AcquistoId)
        {
            string Svincolato = "";

            string VerificaCliente = "";

            if (Session["VerificaCliente"] != null)
            {
                VerificaCliente = Session["VerificaCliente"].ToString();
            }


            if (Session["Svincolato"] != null)
            {
                Svincolato = Session["Svincolato"].ToString();
            }
            else
            {
                Session["AssTel"] = AcquistoId.Acquisto_Id;
            }

            string Err = "";

            string State = Repository.GetReservationState(AcquistoId.Acquisto_Id);

            if (State == "")
            {
                Err = "Id Acquisto non trovato";
                return Json(Err);
            }
            else
            {
                //verifica cognome             
                string Cognome = Repository.GetPurchaseUser(AcquistoId.Acquisto_Id);

                if (Cognome.ToUpper() != AcquistoId.Acquisto_Cognome.ToUpper())
                {
                    Err = "Il cognome non corrisponde all' Id acquisto";
                    return Json(Err);
                }

                if (VerificaCliente == "true")
                {
                    DetectFamily(AcquistoId.Acquisto_Id);
                    return Json("");
                }


                if (Svincolato != "true")
                {
                    //verifica condizione acquisto
                    if (Impostazioni.StopOperator == "on")
                    {
                        if (State != "PR")
                        {
                            Err = "L'acquisto non è più da completare";
                            return Json(Err);
                        }

                    }

                }
                else
                {
                    //verifica condizione acquisto
                    if (State != "OK")
                    {
                        Err = "L'acquisto non è completato";
                        return Json(Err);
                    }
                }

            }


            //Session["AssTel"] = AcquistoId.Acquisto_Id;

            return Json("");
        }

        private void DetectFamily(string Acquisto_Id)
        {

            //detect if is a family pack
            Repository Rep = new Repository();

            string fam = Rep.GetFamilyPurchase(Acquisto_Id);
            Session["FamilyPack"] = null;

            if (fam != "")
            {
                Session["FamilyPack"] = fam;
            }

        }

        public ActionResult LoginUser(Login login)
        {
            if (ModelState.IsValid)
            {
                Users UserProfile = Users.Instance().Loguearse(login.Login_Email, login.Login_Password);
                if (UserProfile != null)
                {
                    FormsAuthenticationTicket formAuthTicket = null;

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string userData = serializer.Serialize(UserProfile);

                    formAuthTicket = new FormsAuthenticationTicket(1, UserProfile.User_Email, DateTime.Now, DateTime.Now.AddMinutes(30), false, userData);
                    string encformAuthTicket = FormsAuthentication.Encrypt(formAuthTicket);
                    HttpCookie formAuthCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encformAuthTicket);
                    Response.Cookies.Add(formAuthCookie);

                    Impostazioni.ClearShopSessions();
                    Session["logged"] = true;
                    Session["UserType"] = UserProfile.User_Type;
                    Session["UserId"] = UserProfile.User_Id;

                    Impostazioni.DeleteAllCookies();

                    Log.Write("AreaRiservatacontroller LoginUser ClearShopSessions");
                    


                }
                else
                {
                    Impostazioni.DeleteAllCookies();
                    Log.Write("AreaRiservatacontroller Login  Fallito");
                    Impostazioni.ClearShopSessions();

                    string Err = "Email o password incorretta";
                    ModelState.AddModelError("error_login", Err);
                    return Json(Err);
                }
            }

            return Json("");
        }

    }
}