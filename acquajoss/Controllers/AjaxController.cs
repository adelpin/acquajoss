﻿using Core;
using System;
using System.Configuration;
using System.Web.Mvc;

namespace AcquaJoss.Controllers
{
    public class AjaxController : Controller
    {
        // GET: Ajax
        [HttpPost]
        public ActionResult NewsLetter(Newsletters data)
        {
            data.Newsletter_CountryCode = "IT";
            data.Newsletter_Status = true;
            data.Newsletter_CreationDate = DateTime.Now;
            data.Newsletter_IdSite = Convert.ToInt32(ConfigurationManager.AppSettings["IdWebSite"]);
            var newsletter = Newsletters.Instance().InsertOrUpdate(data);
            return Json(newsletter);
        }
    }
}