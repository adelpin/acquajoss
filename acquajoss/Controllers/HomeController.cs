﻿using Core;
using Core.Db;
using Core.Utilities;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AcquaJoss.Controllers
{

    public class HomeController : Controller
    {

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            GetKeywords();
            string color = "red";
            ViewBag.ParkOpen = Repository.GetParkOpen(ref color);
            ViewBag.ParkColor = color;
        }

        public async Task<List<Reviews>> Parsing(string website)
        {
            try
            {
                List<Reviews> reviews = new List<Reviews>();
                HttpClient http = new HttpClient();
                var response = await http.GetByteArrayAsync(website);
                String source = Encoding.GetEncoding("utf-8").GetString(response, 0, response.Length - 1);
                source = WebUtility.HtmlDecode(source);
                HtmlDocument resultat = new HtmlDocument();
                resultat.LoadHtml(source);

                List<HtmlNode>
                  toftitle = resultat.DocumentNode.Descendants().Where
                  (x => (x.Name == "div" && x.Attributes["class"] != null && x.Attributes["class"].Value.Contains("review-container"))).ToList();

                foreach (var tag1 in toftitle)
                {
                    HtmlNode ratin = tag1.Descendants().Where(x => (x.Name == "div" && x.Attributes["class"] != null && x.Attributes["class"].Value.Contains("rating reviewItemInline"))).ToList().FirstOrDefault();
                    HtmlNode quote = tag1.Descendants().Where(x => (x.Name == "div" && x.Attributes["class"] != null && x.Attributes["class"].Value.Contains("quote"))).ToList().FirstOrDefault();
                    HtmlNode text = tag1.Descendants().Where(x => (x.Name == "p" && x.Attributes["class"] != null && x.Attributes["class"].Value.Contains("partial_entry"))).ToList().FirstOrDefault();
                    //HtmlNode y = text.Descendants().Where(x => x.Name == "span" && x.Attributes["class"] != null && x.Attributes["class"].Value.Contains("taLnk ulBlueLinks")).ToList().FirstOrDefault();
                    //y.Remove();

                    reviews.Add(new Reviews() { Rates = ratin.InnerHtml, Tittle = Regex.Replace(quote.InnerHtml, "<.*?>", String.Empty), Text = text.InnerHtml });
                }
                return await Task.FromResult(reviews);

            }
            catch (Exception e)
            {

                Console.Write("Network Problem!" + e.Message);
            }
            return null;
        }

        public ActionResult Index()
        {
            Impostazioni.SiteSettings = Repository.GetSiteSettings();
            Impostazioni.PrezzoAssicurazione = (double)Repository.ConvertiPrezzo(Repository.GetAssicData());
            Impostazioni.ManageDateSettings();

            ViewBag.DataOggi = Impostazioni.DataOggi();


            List<Activities> specialist = new List<Activities>();
            HomeModel model = new HomeModel();
      
            DateTime date = Core.Impostazioni.DataOggiDt();
            model.ListSliders = GetSectionSlider.Where(x => x.Slider_Status == true && x.Slider_DateStart <= date && (x.Slider_DateEnd >= date || x.Slider_DateEnd == null)).ToList();

            model.ListActivities = Activities.Instance().GetActivities().Where(x => x.Activity_Status == true).OrderBy(a => a.Activity_Index).ToList();


            while (model.ListActivities.Count < 6)
            {
                var limitb = 6 - model.ListActivities.Count;
              
                if (limitb > model.ListActivities.Count)
                {
                     specialist = model.ListActivities.Take(model.ListActivities.Count).ToList();
                    model.ListActivities.AddRange(specialist);
                }
                else
                {
                     specialist = model.ListActivities.Take(limitb).ToList();
                    model.ListActivities.AddRange(specialist);
                }   
            }

            model.ListPromotions = Promotions.Instance().GetPromotions();
           
            model.AqHome = Repository.GetAquaHome(1);

            ViewBag.SliderClass = "BannerSlider";

            ViewBag.TipoOperatore = Impostazioni.TipoOperatore();
            ViewBag.IdOperatore = Impostazioni.IdOperatore();
            return View(model);

        }


        [Route("oauthredirect")]
        public ActionResult oauthredirect(string code)
        {
            //zoho campaign
            Log.Write("zohocampaign: " + code);
            return RedirectToAction("Index", "home");

        }


        [Route("Ilparco")]
        public ActionResult Ilparco()
        {
            ActivitiesModel model = new ActivitiesModel();
            model.ListSliders = GetSectionSlider;


            if (SectionSelected != null)
            {
                ViewBag.SectionId = SectionSelected.Section_Id;
            }
           

            ViewBag.SliderClass = "banners-slider";

            return View(model);
        }

        [Route("ilparco/{url}")]
        public ActionResult Activity(string url)
        {
            ActivitiesModel model = new ActivitiesModel(url);

            if (model.activity == null || model.activity.Activity_Status == false)
            {
                return RedirectToAction("Index", "home");
            }

            return View(model);
        }

        public ActionResult Confirmation()
        {
            if (!string.IsNullOrEmpty(Impostazioni.FakePurchase))
            {
                Session["ReservationId"] = Impostazioni.FakePurchase;
            }

            if (Session["ReservationId"] == null)
            {
                return RedirectToAction("Index", "home");
            }

            //delete this session NOW to avoid issues
            //loguser uses the session for update products or customer that made two purchases
            string ReservationId = Session["ReservationId"].ToString();

            Session["DownloadId"] = ReservationId;
            Session["ReservationId"] = null;

            //Zoho crm:        
            Repository.InsertZoHoCrmSale(ReservationId,true);

            try
            {
                CreateGoogleAnalytics(ReservationId);
            }
            catch (Exception ex)
            {
                Log.Write("errore CreateGoogleAnalytics " + ReservationId + " " + ex.Message);
            }


            ViewBag.ReservationId = ReservationId;
            ViewBag.TicketsEmitted = Repository.TicketsEmitted(Convert.ToInt32(ReservationId));

            //delete all transactions
            Impostazioni.DeleteAllCookies();
            Log.Write("Confirmation id: " + ReservationId + "ClearShopSessions");
            Impostazioni.ClearShopSessions();

            int LogUser = Impostazioni.TipoOperatore();


            //se acquisto op. va azzerato
            Session["ReservationId"] = null;
            Session["PurchaseDate"] = null;
            Session["BankShopId"] = null;

            ViewBag.TipoOperatore = LogUser;

            return View();

        }

        private void CreateGoogleAnalytics(string ReservationId)
        {
            string jsonCode = "";
            string Total = Repository.CreateGoogleAnalytics(ReservationId, ref jsonCode);

            ViewBag.TotalReservacion = Total;
            ViewBag.jsoncodigo = jsonCode;
            ViewBag.tag = "tagConfirma";

            Log.Write("Google analytics acquisto: " + ReservationId + " " + jsonCode);

            Log.WriteFbq("fbq write Totale: " + Total + " Dettaglio: " + jsonCode);
        }

      

        private Shops Acquisizione(out string tipo)
        {
            //acquistion
            tipo = "0";

            Shops Shop = new Shops();

            ShoppingList Shopping = (ShoppingList)Session["Lista"];
            ShoppingState LstView = Impostazioni.ConvertToState(Shopping);

            ShoppingListHotel ShoppingHotel = (ShoppingListHotel)Session["ListaHotel"];
            ShoppingStateHotel LstViewHotel = Impostazioni.ConvertToStateHotel(ShoppingHotel);

            ShoppingListSubscription ShoppingSubscriptions = (ShoppingListSubscription)Session["ListaAbbonamenti"];
            ShoppingStateSubscriptions LstViewSubscriptions = Impostazioni.ConvertToStateSubscriptions(ShoppingSubscriptions);

            Shop.ShoppingState = LstView;
            Shop.ShoppingStateHotel = LstViewHotel;
            Shop.ShoppingStateSubscriptions = LstViewSubscriptions;

            string Day = Shop.ShoppingState.DayNow;
            string DayArrive = Shop.ShoppingStateHotel.DayArrive;
            string SubscriptionTotPrice = Shop.ShoppingStateSubscriptions.TotPrice;


            if (!string.IsNullOrEmpty(Day))
            {
                tipo = "1";
            }

            //hotel
            if (!string.IsNullOrEmpty(DayArrive))
            {
                tipo = "2";
            }

            //subscriptions            
            if (!string.IsNullOrEmpty(SubscriptionTotPrice))
            {

                if (SubscriptionTotPrice != "0")
                {
                    tipo = "3";
                }
            }

            return Shop;
        }


        [HttpGet]
        public ActionResult DownloadFile()
        {
            if (Session["DownloadId"] == null)
            {
                return RedirectToAction("index", "home");
            }

            //string Barcode = "";
            //Barcode = Session["Barcode"].ToString();

            string ReservationId = Session["DownloadId"].ToString();
            string Barcode = TMasterClass.CreaBarcode(ReservationId);

            TmTransazioni Tm = Repository.GetTmasterOperations(ReservationId);

            //for product under calendar:
            Session["BackOfficeDay"] = Repository.GetAdmissionDate(ReservationId);

            Shops Shop = Impostazioni.GetAcquistoOp(ReservationId);

            Session["BackOfficeDay"] = null;

            InfoBuyer Buyer = new InfoBuyer();
            int TipoAcquisto = 0;


            if (Shop.ShoppingState.DayNow != "")
            {
                TipoAcquisto = 1;

                Buyer = Shop.ShoppingState.Buyer;
                Tm.Totale = Shop.ShoppingState.Price;
            }

            if (Shop.ShoppingStateHotel.DayArrive != "")
            {
                TipoAcquisto = 2;
                Buyer = Shop.ShoppingStateHotel.Buyer;
                Tm.Totale = Shop.ShoppingStateHotel.Price;
            }

            if (Shop.ShoppingStateSubscriptions.TotPrice != "0")
            {
                TipoAcquisto = 3;
                Buyer = Shop.ShoppingStateSubscriptions.Buyer;
                Tm.Totale = Shop.ShoppingStateSubscriptions.TotPrice;
            }

            string Nominativo = Buyer.Name + " " + Buyer.Surname;

            Purchase Acquisto = new Purchase();
            Acquisto.Purchase_Type_Id = TipoAcquisto;
            Acquisto.Purchase_Id = ReservationId;
            string downloadFileName = Impostazioni.NomeAllegatoMail(Acquisto);

            var stream = new MemoryStream();

            //string AssicData = Repository.GetAssicData();
            string AssicData = "0";
            stream = TMasterClass.CreaAllegato(Tm, TipoAcquisto, Nominativo, ReservationId, AssicData, Barcode);

            if (stream == null)
            {
                Log.Write("Allegato vuoto");
                return null;
            }
         
            return File(stream, "application/pdf", downloadFileName);
        }


        public ActionResult NewsLetter()
        {
            HomeModel model = new HomeModel();
            model.ListSliders = GetSectionSlider;
            return View("~/Views/Home/newsletter.cshtml", model);
        }


        public ActionResult Scuole()
        {
            HomeModel model = new HomeModel();
            model.ListSliders = GetSectionSlider;
            return View("~/Views/Home/scuole.cshtml", model);
        }


        public ActionResult Infanzia()
        {
            HomeModel model = new HomeModel();
            model.ListSliders = GetSectionSlider;
            return View("~/Views/Home/Infanzia.cshtml", model);
        }
        public ActionResult Primaria()
        {
            HomeModel model = new HomeModel();
            model.ListSliders = GetSectionSlider;
            return View("~/Views/Home/Primaria.cshtml", model);
        }

        public ActionResult SecondariaI()
        {
            HomeModel model = new HomeModel();
            model.ListSliders = GetSectionSlider;
            return View("~/Views/Home/SecondariaI.cshtml", model);
        }

        public ActionResult SecondariaII()
        {
            HomeModel model = new HomeModel();
            model.ListSliders = GetSectionSlider;
            return View("~/Views/Home/SecondariaII.cshtml", model);
        }

        public ActionResult Gruppi()
        {
            HomeModel model = new HomeModel();
            model.ListSliders = GetSectionSlider;
            return View("~/Views/Home/gruppi.cshtml", model);
        }

        public ActionResult pachettoDiFamiglia()
        {
            PromotionsModel model = new PromotionsModel();
            model.Promotions = Promotions.Instance().GetPromotions();

            return View("~/Views/Home/pachetto-di-famiglia.cshtml", model);
        }

        public ActionResult Promozioni()
        {
            PromotionsModel model = new PromotionsModel();
            model.Promotions = Promotions.Instance().GetPromotions();

            //Repository Rep = new Repository();
            //List<Product> PromFamily = Rep.GetPromotionsFamily();
            //List<Product> PromEventi = Rep.GetPromotionEvents();

            //ViewBag.PromFamily = PromFamily;
            //ViewBag.PromEventi = PromEventi;

            return View("~/Views/Home/promozioni.cshtml", model);
        }


        public ActionResult Calendari()
        {
            string StartDate = Impostazioni.StartInfoCalendaDate;

            if (String.IsNullOrEmpty(StartDate))
            {
                StartDate = "01/01/2018";
            }

            int StartYear = Convert.ToInt32(StartDate.Substring(6, 4));
            int StartMonth = Convert.ToInt32(StartDate.Substring(3, 2));
            ViewBag.StartYear = StartYear;
            ViewBag.StartMonth = StartMonth;

            List<ParkOperational> ParkOperationals = ParkOperational.Instance().GetParkOperationalList().Where(PO => PO.ParkOperational_Visible == true && PO.ParkOperational_Status == true).ToList();
            ViewBag.ParkOperationals = ParkOperationals;

            //if more than one parkoperational active only the first
            var elem = ParkOperationals[0];
            int ParkOperational_Id = elem.ParkOperational_Id;

            List<ParkCalendar> ParkCalendars = new List<ParkCalendar>();
            List<ParkCalendar> ParkCalendars_elem = ParkCalendar.Instance().GetParkCalendar(ParkOperational_Id);
            ParkCalendars.AddRange(ParkCalendars_elem);

            //starting date:
            DateTime dd = Impostazioni.ConvertiDataDateFormatTwo(StartDate);
            ParkCalendars = ParkCalendars.Where(x => x.ParkCalendar_Date >= dd).ToList();
            ViewBag.ParkCalendars = ParkCalendars;

            List<ParkSchedules> schedulesList = ParkSchedules.Instance().GetParkSchedules();
            schedulesList.Add(new ParkSchedules { ParkSchedule_Description = "Parco Chiuso", ParkSchedule_Color = "#ddd", ParkSchedule_Status = true });
            ViewBag.schedulesList = schedulesList;

            var queyYears = from year in ParkCalendars group year by year.ParkCalendar_Date.Year into g orderby g.Key select g;
            List<string> Years = (from year in queyYears where year != null select year.Key.ToString()).ToList();
            ViewBag.Years = Years;

            List<Activities> activities = Activities.Instance().GetActivities().FindAll(ac => ac.ActivitiesCalendar.Count > 0);
            ViewBag.activities = activities;

            List<ActivitySchedule> schedules = ActivitySchedule.Instance().GetActivitySchedules();
            ViewBag.schedules = schedules;

            return View("~/Views/Home/calendari.cshtml");
        }

        public ActionResult Eventi()
        {

            EventsModel model = new EventsModel();
            model.ListSliders = GetSectionSlider;
            return View(model);
        }

      
        public ActionResult EventoInterna(string url)
        {
            EventsModel model = new EventsModel(url);
            if (model.Event == null) {
                return RedirectToAction("Eventi");
            }

            return View(model);
        }

        //Redirect urls eventis passati 
        [Route("eventi/evento/{id}")]
        public ActionResult EventoRedirec(int id)
        {
            return RedirectToAction("Index");
        }
        //Redirect urls blog 
        [Route("blog/post/{id}/{url}")]
        public ActionResult BlogRedirec(int id, string url)
        {
            return RedirectToAction("Index");
        }


        public ActionResult EventiPassati()
        {
            EventsModel model = new EventsModel();
            model.ListSliders = GetSectionSlider;
            return View(model);
        }

        public ActionResult InformazioniGenerali()
        {

            return View("~/Views/Home/informazioni-generali.cshtml");
        }

        public ActionResult domandeFrequenti()
        {
            FaqsModel model = new FaqsModel();
            return View("~/Views/Home/domandeFrequenti.cshtml", model);
        }

        public ActionResult Lavora()
        {

            return View();
        }

        public ActionResult ComeArrivare()
        {
            HomeModel model = new HomeModel();
            model.ListSliders = GetSectionSlider;
            return View("~/Views/Home/come-arrivare.cshtml", model);
        }

        public ActionResult CentroEstivo()
        {
            HomeModel model = new HomeModel();
            model.ListSliders = GetSectionSlider;
            return View("~/Views/Home/centro-estivo.cshtml", model);
        }

        public ActionResult ChiSiamo()
        {

            return View("~/Views/Home/chi-siamo.cshtml");
        }

        public ActionResult Eduscienza()
        {
            HomeModel model = new HomeModel();
            model.ListSliders = GetSectionSlider;
            return View("~/Views/Home/eduscienza.cshtml", model);
        }

        public ActionResult News() {
            NewsModel model = new NewsModel();
            model.ListSliders = GetSectionSlider;
            return View(model);
        }

        public ActionResult NewsInterna(string url)
        {
            NewsModel model = new NewsModel(url);
            if (model.News == null)
            {
                return RedirectToAction("News");
            }

            return View(model);
        }

        public ActionResult EventiVip()
        {
            return View("~/Views/Home/eventi-vip.cshtml");
        }
        public ActionResult Compleanni()
        {
            HomeModel model = new HomeModel();
            model.ListSliders = GetSectionSlider;
            return View("~/Views/Home/compleanni.cshtml", model);
        }

        public ActionResult Convenzioni()
        {
            int id = 1;
            ConvenzioniModel model = new ConvenzioniModel();
            model.ListSliders = GetSectionSlider;
            model.Conven = Convenzion.Instance().GetConvenzioni(id);
            return View("~/Views/Home/convenzioni.cshtml", model);
        }
        public ActionResult InformativaCookie()
        {
            return View("~/Views/Home/informativa-cookie.cshtml");
        }

        public ActionResult Partner()
        {
            PartnersModel model = new PartnersModel();

            return View("~/Views/Home/Partner.cshtml", model);
        }

        public ActionResult Contatti()
        {
            ViewBag.Section = "contactti";
            return View();
        }
        public ActionResult Servizi()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SendRequest(FormContact data)
        {
         
            bool success = (FormContact.Instance().InsertOrUpdate(data) != 0);
            if (success)
            {
                string lang = "IT";

                ContactModel model = new ContactModel()
                {
                    Language = lang,
                    Data = data
                };

                string Send = "acquajoss@gmail.com";
                string Title = "Contatti AcquaJoss";
                string Body = RenderRazorViewToString("~/Views/Email/Confirm.cshtml", model);

                MailService.EnviarCorreo(data.FormContact_Email, Send, Title, Body);
                MailService.EnviarCorreo(Send, Send, Title, Body);

                TempData["Title"] = "Grazie";
                TempData["Msn"] = "Ti contatteremo il prima possibile!";
            }
            return RedirectToAction("Contatti");
        }

        public string RenderRazorViewToString(string viewName, object model = null)
        {
            try
            {
                ViewData.Model = model;
                using (var sw = new StringWriter())
                {
                    var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                    var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                    viewResult.View.Render(viewContext, sw);
                    viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                    return sw.GetStringBuilder().ToString();
                }
            }
            catch (Exception ex)
            {
                Log.Write("RenderRazorViewToString: " + ex.Message);
                return null;
            }
        }

        public Seo GetKeywords()
        {
            string url = Request.Url.AbsolutePath;
            if (url == "/")
            {
                url = Request.Url.Host;
                if (!GlobalSettings.EnProduccion) url = GlobalSettings.UrlSite;
            }

            Seo keywords = Seo.Instance().GetSeoUrl(url);


            if (keywords != null)
            {
                ViewBag.Title = keywords.Seo_MetaTitle;
                ViewBag.Description = keywords.Seo_MetaDescription;
                ViewBag.Keywords = keywords.Seo_MetaKeywords;
            }
            return keywords;
        }

        public Sections SectionSelected
        {
            get
            {
                string url = Request.Url.AbsolutePath;
                if (string.IsNullOrEmpty(url) || url.Equals("/"))
                {
                    url = GlobalSettings.UrlSite; //for home
                }

                return Sections.Instance().GetSection(url);
            }
        }


        public List<Sliders> GetSectionSlider
        {

            get
            {

                if (SectionSelected != null)
                {
                    List<Sliders> Sl = Sliders.Instance().GetSliders();
                    Sl = Sl.Where(s => s.Slider_IdSection.Equals(SectionSelected.Section_Id)).OrderBy(x => x.Slider_Order).ToList();
                    return Sl;
                }
                return null;


            }
        }
    }
}