function resize_altezze(){var e=jQuery(window).height();jQuery(".100vh").css({height:e+"px",position:"relative"}),jQuery(".50vh").css({height:e/2+"px",position:"relative"})}jQuery(document).ready(function(e){var n=e("#um-menu-trigger"),o=e("#um-menu-close"),s=e(".um-full-container"),i=e("header"),a=e(".um-frame");n.on("click",function(o){o.preventDefault(),n.addClass("is-clicked"),i.addClass("um-header-open"),a.addClass("um-frame-open"),s.addClass("um-menu-open").one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",function(){e("body").addClass("overflow-hidden")}),e("#um-lateral-nav").addClass("lateral-menu-is-open"),e("html").hasClass("no-csstransitions")&&e("body").addClass("overflow-hidden")}),o.on("click",function(o){o.preventDefault(),n.removeClass("is-clicked"),i.removeClass("um-header-open"),a.removeClass("um-frame-open"),s.removeClass("um-menu-open").one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",function(){e("body").removeClass("overflow-hidden")}),e("#um-lateral-nav").removeClass("lateral-menu-is-open"),e("html").hasClass("no-csstransitions")&&e("body").removeClass("overflow-hidden")})}),jQuery(window).load(function(){jQuery(window).width()<992&&jQuery(".slideDownNextBox").slideUp()}),jQuery(document).ready(function(){if(resize_altezze(),jQuery(window).width()>992)var e=!0;else var e=!1;jQuery(window).resize(function(){jQuery(window).width()>992?1==e?resize_altezze():(e=!0,jQuery(".slideDownNextBox").slideDown(),jQuery(".slideDownNextBox").addClass("open"),jQuery(".slideDownNext").addClass("open")):1==e&&(e=!1,jQuery(".slideDownNextBox").slideUp(),jQuery(".slideDownNextBox").removeClass("open"),jQuery(".slideDownNext").removeClass("open"))})});


jQuery(document).ready(function() {
	jQuery('.mappa iframe').addClass('scrolloff');                // set the mouse events to none when doc is ready
	jQuery('.mappa').on("mouseup",function(){          // lock it when mouse up
	    jQuery('.mappa iframe').addClass('scrolloff'); 
	    //somehow the mouseup event doesn't get call...
	});
	jQuery('.mappa').on("mousedown",function(){        // when mouse down, set the mouse events free
	    jQuery('.mappa iframe').removeClass('scrolloff');
	});
	jQuery(".mappa iframe").mouseleave(function () {              // becuase the mouse up doesn't work... 
	    jQuery('.mappa iframe').addClass('scrolloff');            // set the pointer events to none when mouse leaves the map area
	});
});

/**
 * Cookie plugin
 *
 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */

jQuery.cookie=function(name,value,options){if(typeof value!='undefined'){options=options||{};if(value===null){value='';options=$.extend({},options);options.expires=-1}var expires='';if(options.expires&&(typeof options.expires=='number'||options.expires.toUTCString)){var date;if(typeof options.expires=='number'){date=new Date();date.setTime(date.getTime()+(options.expires*24*60*60*1000))}else{date=options.expires}expires='; expires='+date.toUTCString()}var path=options.path?'; path='+(options.path):'';var domain=options.domain?'; domain='+(options.domain):'';var secure=options.secure?'; secure':'';document.cookie=[name,'=',encodeURIComponent(value),expires,path,domain,secure].join('')}else{var cookieValue=null;if(document.cookie&&document.cookie!=''){var cookies=document.cookie.split(';');for(var i=0;i<cookies.length;i++){var cookie=jQuery.trim(cookies[i]);if(cookie.substring(0,name.length+1)==(name+'=')){cookieValue=decodeURIComponent(cookie.substring(name.length+1));break}}}return cookieValue}};
