jQuery(function(){

	var ckie_flag = jQuery.cookie("Matematici-CookiePolicyManager");
	
	if(ckie_flag == null && ckie_flag != ""){
		
		var scrolled=false;
		
		jQuery('#pp_modal').slideDown(600);
		
		jQuery('a[target!="_blank"]').click(function(){
			
			jQuery.cookie("Matematici-CookiePolicyManager","true",{path:'/',expires:365});
			jQuery('#pp_modal').slideUp(600);
		});
		
		jQuery('#pp_ok').click(function(){
			
			jQuery.cookie("Matematici-CookiePolicyManager","true",{path:'/',expires:365});
			jQuery('#pp_modal').slideUp(600);
			activateStuff();
		
		});
			
		jQuery(window).scroll(function(){
		
			if(!scrolled){
			
				jQuery.cookie("Matematici-CookiePolicyManager","true",{path:'/',expires:365});
				jQuery('#pp_modal').slideUp(600);
				activateStuff();
				scrolled=true;
				
			}
			
		});
		
	}else{
		
		activateStuff();
	
	}
	
	function activateStuff(){
	
		jQuery('#myIframe').html('<iframe width="100%" height="315" src="https://www.youtube.com/embed/Gw_L3GEbqdM?rel=0" frameborder="0" allowfullscreen></iframe>');
		jQuery('#myIframeEstate').html('<iframe width="100%" height="315" src="https://www.youtube.com/embed/G6sNoEPIgIo?rel=0" frameborder="0" allowfullscreen></iframe>');
		jQuery('#myIframeDelfini').html('<iframe width="560" height="315" src="https://www.youtube.com/embed/v7K7eJ_U9PM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>');
		jQuery('#myIframeHallo').html('<iframe width="100%" height="315" src="https://www.youtube.com/embed/grj03LjDlnQ?rel=0" frameborder="0" allowfullscreen></iframe>');
		
		jQuery('#myIframeNatale').html('<iframe width="100%" height="315" src="https://www.youtube.com/embed/U6A-qFKs9h8?rel=0" frameborder="0" allowfullscreen></iframe>');
		jQuery('#myIframeNatale2').html('<iframe width="100%" height="315" src="https://www.youtube.com/embed/n1RffJb3YpI?rel=0" frameborder="0" allowfullscreen></iframe>');
		
		jQuery('#ck_map').html('<iframe src="https://www.google.com/maps/embed?pb=!1m17!1m11!1m3!1d280696.20297403145!2d12.447755082190309!3d41.748910356474056!2m2!1f0!2f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x33af6ebf968be635!2sZoomarine!5e1!3m2!1sit!2sit!4v1426074505538" width="600" height="550" frameborder="0" style="border:0"></iframe>');
		jQuery('#ck_fbox').html('<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fzoomarine&amp;width&amp;height=908&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:258px;" allowTransparency="true"></iframe>');
		jQuery('#ck_conversion').html('<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1039313177/?value=1.00&amp;currency_code=EUR&amp;label=mue9COH3vFoQmdLK7wM&amp;guid=ON&amp;script=0"/>');
		
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
		
		fbq('init','675132979264113');
		fbq('track',"PageView");
		
		setTimeout(function(){
			
			jQuery('.nl_overlay').fadeIn(600);
			jQuery('.nl_modal').addClass('enter');
		
		},8000);
		
		jQuery('#nl_close').click(function(e){
			
			e.preventDefault();
			
			jQuery('.nl_overlay').fadeOut(600);
			jQuery('.nl_modal').removeClass('enter');
			
			globalModal = true;
		
		});
		
		jQuery('.nl_modal').click(function(e){
			
			e.stopPropagation();
		
		});
			
		jQuery('.nl_overlay').click(function(e){
				
			jQuery('.nl_overlay').fadeOut(600);
			jQuery('.nl_modal').removeClass('enter');
			
			globalModal = true;
			
		});
		
		/* Exit Script */
		var exit_script      = document.createElement("script");
			exit_script.type = "text/javascript";
			exit_script.src  = "//widget.manychat.com/47551870673.js";
		jQuery("head").append(exit_script);
		
		/* Exit Intent */
		var exit_cookies = jQuery.cookie("Exit-Cookies"),
			globalModal  = false;
		
//		if(exit_cookies == null && exit_cookies != ""){
//			
//			/* Exit Modal */
//			var exit_modal = '<style>'+
//				'.exit_overlay{background:rgba(0, 53, 95, 0.8) none repeat scroll 0 0;bottom:0;height:100%;left:0;position:fixed;right:0;top:0;width:100%;z-index:100000000;display:none;}.exit_modal{background:#fff none repeat scroll 0 0;left:50%;padding:20px;position:absolute;top:64%;transform:translate(-50%, -50%);max-width:640px;color:rgb(0, 53, 95);transition:top .6s ease .4s;-webkit-transition:top .6s ease .4s;}.exit_modal.enter{top:50%;}.exit_modal h2{color:#0097d4;text-shadow:3px 3px 0px #00365d;font-size:42px;}.exit_close{z-index:100;color:#cc1042;cursor:pointer;font-size:40px;padding:10px;position:absolute;right:7px;text-align:center;top:0;transform:rotate(45deg);-webkit-transform:rotate(45deg);}.exit_close:hover{color:#00365D;text-decoration:none;}@media screen and (max-width:640px){.nl_modal{left:auto;margin:64px auto 16px;padding:10px;position:relative;top:auto;transform:translate(0px, 0px);-webkit-transform:translate(0px, 0px);width:90%;}.exit_modal.enter{top:auto;}.exit_overlay{overflow-y:scroll;}.exit_modal h2{font-size:24px;}.exit_close{right:-1%;top:-3%;}}'+
//				'</style>'+
//				'<div class="exit_overlay">'+
//					'<div class="exit_modal">'+
//						'<div class="exit_body">'+
//							'<div class="mcwidget-embed" data-widget-id="1962989"></div>'+
//						'</div><a href="#" id="exit_close" class="exit_close">+</a>'+
//					'</div>'+
//				'</div>';
//				
//			jQuery('body').append(exit_modal);
//			
//							
//			
//			
//			
//			jQuery('#exit_close').click(function(e){
//				
//				e.preventDefault();
//				
//				jQuery('.exit_overlay').fadeOut(600);
//				jQuery('.exit_modal').removeClass('enter');
//							
//			});
//			
//			
//			function detectIE() {
//			    var ua = window.navigator.userAgent;
//			
//			    var msie = ua.indexOf('MSIE ');
//			    if (msie > 0) {
//			        // IE 10 or older => return version number
//			        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
//			    }
//			
//			    var trident = ua.indexOf('Trident/');
//			    if (trident > 0) {
//			        // IE 11 => return version number
//			        var rv = ua.indexOf('rv:');
//			        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
//			    }
//			
//			    var edge = ua.indexOf('Edge/');
//			    if (edge > 0) {
//			       // Edge (IE 12+) => return version number
//			       return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
//			    }
//			
//			    // other browser
//			    return false;
//			}
//				 
//			function addEvent(obj, evt, fn) {
//				
//				if (obj.addEventListener) {
//					obj.addEventListener(evt, fn, false);
//				} else if (obj.attachEvent) {
//					obj.attachEvent("on" + evt, fn);
//				}
//				
//			}
//			
//			/* Exit Trigger */ 
//			addEvent(document, "mouseout", function(e) {
//			
//				e = e ? e : window.event;
//				
//				var from         = e.relatedTarget || e.toElement,	
//					exit_cookies = jQuery.cookie("Exit-Cookies");
//				
//				if(globalModal == true){
//					if(detectIE() <= 11){
//						if (exit_cookies === '1') {
//							return false;
//						} else if ((!from || from.nodeName == "HTML")) {
//							
//							jQuery.cookie('Exit-Cookies',"1", { path: '/', expires: 365 });
//							
//							jQuery('.exit_overlay').fadeIn(600);
//							jQuery('.exit_modal').addClass('enter');
//							
//						}
//					} else { 
//						if (exit_cookies === '1') {
//							return false;
//						} else if ((!from || from.nodeName == "HTML") && e.pageY < $(window).scrollTop()) {
//							
//							jQuery.cookie('Exit-Cookies',"1", { path: '/', expires: 365 });
//							
//							jQuery('.exit_overlay').fadeIn(600);
//							jQuery('.exit_modal').addClass('enter');
//							
//						}
//					}
//				} 
//				
//			});
//		
//		}
		
	}


});