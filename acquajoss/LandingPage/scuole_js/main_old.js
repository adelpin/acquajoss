jQuery(document).ready(function($){
	var $lateral_menu_trigger = $('#um-menu-trigger'),
		$inner_menu_trigger = $('#um-menu-close'),
		$content_wrapper = $('.um-full-container'),
		$navigation = $('header'),
		$frame = $('.um-frame');

	//open-close lateral menu clicking on the menu icon
	$lateral_menu_trigger.on('click', function(event){
		event.preventDefault();
		$lateral_menu_trigger.addClass('is-clicked');
		$navigation.addClass('um-header-open');
		$frame.addClass('um-frame-open');
		$content_wrapper.addClass('um-menu-open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
			// firefox transitions break when parent overflow is changed, so we need to wait for the end of the trasition to give the body an overflow hidden
			$('body').addClass('overflow-hidden');
		});
		$('#um-lateral-nav').addClass('lateral-menu-is-open');
		
		//check if transitions are not supported - i.e. in IE9
		if($('html').hasClass('no-csstransitions')) {
			$('body').addClass('overflow-hidden');
		}
	});
	
	//open-close lateral menu clicking on the menu icon
	$inner_menu_trigger.on('click', function(event){
		event.preventDefault();
		$lateral_menu_trigger.removeClass('is-clicked');
		$navigation.removeClass('um-header-open');
		$frame.removeClass('um-frame-open');
		$content_wrapper.removeClass('um-menu-open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
			// firefox transitions break when parent overflow is changed, so we need to wait for the end of the trasition to give the body an overflow hidden
			$('body').removeClass('overflow-hidden');
		});
		$('#um-lateral-nav').removeClass('lateral-menu-is-open');
		
		//check if transitions are not supported - i.e. in IE9
		if($('html').hasClass('no-csstransitions')) {
			$('body').removeClass('overflow-hidden');
		}
	});

	
});

function resize_altezze() {
	var w_altezza = jQuery(window).height();
	jQuery('.100vh').css({'height': w_altezza+'px', 'position':'relative'});
	jQuery('.50vh').css({'height': (w_altezza/2)+'px', 'position':'relative'});	
}


jQuery( window ).load(function() {
	
	if(jQuery(window).width() < 992){
		jQuery('.slideDownNextBox').slideUp()
	}
		
});

jQuery(document).ready(function(){
	// Caricato il DOM
	
	resize_altezze();
	
	if(jQuery(window).width() > 992){
		var sk_desktop = true;
	} else {
		var sk_desktop = false;
	}
	
	
	
	/* Controlli per passaggi da mobile->desktop e viceversa */
	jQuery(window).resize( function() {
		//se sei in desktop
		if(jQuery(window).width() > 992){
			if(sk_desktop == true) {
				//sei rimasto in desktop
				resize_altezze();
			}else{
				//sei passato da mobile a desktop
				sk_desktop = true;
				jQuery('.slideDownNextBox').slideDown()
				jQuery('.slideDownNextBox').addClass('open');
				jQuery('.slideDownNext').addClass('open');
			}
		}
		//se sei in mobile
		else{
			if(sk_desktop == true) {
				//sei passato da desktop a mobile
				sk_desktop = false;	
				jQuery('.slideDownNextBox').slideUp()
				jQuery('.slideDownNextBox').removeClass('open');
				jQuery('.slideDownNext').removeClass('open');
			}else{
				//sei rimasto in mobile
			}
		}
	});	

});