jQuery(function($){

	'use strict';

	var ZOO = window.ZOO || {};

	// GLOBALS
	var $gb_page	   = $('html, body');

	var w_width 	   = $(window).width(),
		w_outer_width  = $(window).outerWidth(),
		w_height 	   = $(window).height(),
		w_inner_height = window.innerHeight,
		w_mb_height    = window.innerHeight ? window.innerHeight : $(window).height(), // Mobile iOS/Android
		d_height 	   = $(document).height();

	$(window).on( 'resize', function(){
		w_width 	   = $(window).width(),
		w_outer_width  = $(window).outerWidth(),
		w_height 	   = $(window).height(),
		w_inner_height = window.innerHeight,
		w_mb_height    = window.innerHeight ? window.innerHeight : $(window).height(), // Mobile iOS/Android
		d_height 	   = $(document).height();
	});


	/*------------------------------------------------------------------------------------
   		Utils
	-------------------------------------------------------------------------------------*/
	ZOO.utils = function(){

		// Hover Touch
		$('body').bind('touchstart', function() {});

		// Multiple Hover Btns
		if( $('.entry-btns.multiple-btns').length ) {

			$('.entry-btns.multiple-btns .b-col').hover(function(){
				$('.entry-btns.multiple-btns .b-col').css( 'opacity', '0.3' );
				$(this).css( 'opacity', '' );
			}, function() {
			    $('.entry-btns.multiple-btns .b-col').css( 'opacity', '' );
			});

		}

		// Scroll to
		if( $('.h-scroll-btn').length ) {
			heroScrollInit();
		}

		function heroScrollInit() {

			$('.h-scroll-btn').on( 'click', function() {
				
				$gb_page.on('scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove', function(){
					$gb_page.stop();
				});

				$gb_page.animate({ scrollTop: $('.sk-col-h-txt').offset().top }, 1000, 'easeInOutExpo', function(){
					$gb_page.off('scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove');
				});

				return false;

			});

		}

	};


	/*------------------------------------------------------------------------------------
   		Lazy Load
	-------------------------------------------------------------------------------------*/
	ZOO.lazyLoad = function(){
		
		function logElementEvent(eventName, element) {
			//console.log(new Date().getTime(), eventName, element.getAttribute('data-src'));
		}
		function logEvent(eventName, elementsLeft) {
			//console.log(new Date().getTime(), eventName, elementsLeft + " images left");
		}

		// Background Image - css class
		var myLazyLoad1 = new LazyLoad({
			elements_selector: ".lazy-bg",
			threshold: 10,
			callback_enter: function (element) {
				//logElementEvent("ENTERED", element);
			},
			callback_load: function (element) {
				//logElementEvent("BG LOADED", element);
			},
			callback_set: function (element) {
				setTimeout( function(){
					$(element).addClass('loaded');
				}, 100);
				//logElementEvent("BG SET", element);
			},
			callback_error: function(element) {
				//logElementEvent("BG ERROR", element);
			}
		});
		myLazyLoad1.update(); /* Force?! */

		// Image <img>
		var myLazyLoad2 = new LazyLoad({
			elements_selector: "img",
			threshold: 10,
			callback_enter: function (element) {
				//logElementEvent("ENTERED", element);
			},
			callback_load: function (element) {
				//logElementEvent("IMG LOADED", element);
			},
			callback_set: function (element) {
				setTimeout( function(){
					$(element).addClass('loaded');
				}, 100);
				//logElementEvent("IMG SET", element);
			},
			callback_error: function(element) {
				//logElementEvent("IMG ERROR", element);
			}
		});
		myLazyLoad2.update(); /* Force?! */

	};


	/*------------------------------------------------------------------------------------
   		COOKIE MANAGER
	-------------------------------------------------------------------------------------*/
	ZOO.cookieManager = function(){

		var ckie_flag = $.cookie("Matematici-CookiePolicyManager");

		if ( ckie_flag == null && ckie_flag != "" ) {
			
			var scrolled = false;
		
			/* Show Cookie Banner */
			$('#pp_modal').stop(true, true).slideDown(600, 'easeInOutExpo');
		
			$('a[target!="_blank"]').click( function() {
				$.cookie( "Matematici-CookiePolicyManager", "true", { path: '/', expires: 365 });
				$('#pp_modal').stop(true, true).slideUp(600, 'easeInOutExpo');
			});
			
			$('#pp_ok').click( function() {
				$.cookie( "Matematici-CookiePolicyManager", "true", { path: '/', expires: 365 });
				$('#pp_modal').stop(true, true).slideUp(600, 'easeInOutExpo');
				activateStuff();
			});
			
			$(window).scroll( function() {
				if( !scrolled ) {
					$.cookie( "Matematici-CookiePolicyManager", "true", { path: '/', expires: 365 });
					$('#pp_modal').stop(true, true).slideUp(600, 'easeInOutExpo');
					activateStuff();
					scrolled = true;
				}
			});
		
		} else {
			
			activateStuff();

		}

		function activateStuff(){

			/* YouTube Video */
			if( $('#myIframe').length ) {
				$('#myIframe').html('<iframe width="100%" height="315" src="https://www.youtube.com/embed/Gw_L3GEbqdM?rel=0" frameborder="0" allowfullscreen></iframe>');
			}
			
			/* Google Map */
			if( $('#ck_map').length ) {
				$('#ck_map').html('<iframe src="https://www.google.com/maps/embed?pb=!1m17!1m11!1m3!1d280696.20297403145!2d12.447755082190309!3d41.748910356474056!2m2!1f0!2f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x33af6ebf968be635!2sZoomarine!5e1!3m2!1sit!2sit!4v1426074505538" width="600" height="550" frameborder="0" style="border:0"></iframe>');
			}
		
			/* Facebook Like Box */
			if( $('#ck_fbox').length ) {
				$('#ck_fbox').html('<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fzoomarine&amp;width&amp;height=908&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:258px;" allowTransparency="true"></iframe>');
			}
		
			/* Adwords Conversion */
			$('#ck_conversion').html('<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1039313177/?value=1.00&amp;currency_code=EUR&amp;label=mue9COH3vFoQmdLK7wM&amp;guid=ON&amp;script=0"/>');
		
			/* Facebook Pixel */
			!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','//connect.facebook.net/en_US/fbevents.js');
			
			fbq('init', '675132979264113');
			fbq('track', "PageView");

			/* Init Newsletter */
			setTimeout(function() {
				newsletterModal();
			}, 8000);

		}

		function newsletterModal(){

			initModal();

			function initModal(){

				// Disable Scroll
				$('html').addClass('no-scroll');

				// Animate In
				anime.remove('#sk-newsletter-modal');
				anime({
					targets : '#sk-newsletter-modal',
					duration: 300,
					opacity : 1,
					delay   : 100,
					easing  : 'linear',
					begin   : function(anim){
						$('#sk-newsletter-modal').css({ 'visibility' : 'visible' });
					},
					complete: function(anim){
						$('#sk-newsletter-modal').addClass('is-ready');

						anime.remove('.sk-modal-dialog');
						anime({
							targets : '.sk-modal-dialog',
							duration: 300,
							opacity : 1,
							delay   : 100,
							easing  : 'linear',
							begin   : function(anim){
				    			$('.sk-modal-dialog').css({ 'visibility' : 'visible' });
				    		}
						});
					}
				});

				// Kill Modal
				$('.sk-close-modal').on( 'click', function(){
				    killModal();
				});

				$(document).on( 'keyup.sk_esc_modal', function (e) {
				    if ( e.keyCode === 27 ) {
				        killModal();
				    }
				});

				$('.sk-modal-bg').on('click.sk_close_modal',function(e) {
				    killModal();
				});

			}

			function killModal() {

				// Unbinding
				$(document).off( 'keyup.sk_esc_modal' );
				$(document).off( 'click.sk_close_modal' );

				// Renabled Scroll
				$('html').removeClass('no-scroll');

				// Animate Out
				anime.remove('.sk-modal-dialog');
				anime({
					targets : '.sk-modal-dialog',
					duration: 300,
					opacity : 0,
					easing  : 'linear',
					complete: function(anim){
						$('.sk-modal-dialog').css({ 'visibility' : 'hidden' });

						anime.remove('#sk-newsletter-modal');
						anime({
							targets : '#sk-newsletter-modal',
							duration: 300,
							opacity : 0,
							delay   : 100,
							easing  : 'linear',
							complete: function(anim){
				    			$('#sk-newsletter-modal').removeClass('is-ready').css({ 'visibility' : 'hidden' });
				    		}
						});

					}
				});

			}

		}

	};

	
	/*------------------------------------------------------------------------------------
   		Init
	-------------------------------------------------------------------------------------*/
	$(document).ready(function(){

		// Utils
		ZOO.utils();
		ZOO.lazyLoad();

		// Cookie Manager
		ZOO.cookieManager();

	});

});